CREATE TABLE copies (
 book_number CHAR(10) NOT NULL,
 sequence_number CHAR(10) NOT NULL,
 type_of_copy CHAR(10),
 price DOUBLE PRECISION
);
ALTER TABLE copies ADD CONSTRAINT PK_copies PRIMARY KEY (book_number,sequence_number);
CREATE TABLE user_infos (
 user_id CHAR(10) NOT NULL,
 name CHAR(10),
 password CHAR(10),
 email CHAR(10),
 gender CHAR(10),
 activation_status CHAR(10)
);
ALTER TABLE user_infos ADD CONSTRAINT PK_user_infos PRIMARY KEY (user_id);
CREATE TABLE activation_codes (
 user_id CHAR(10) NOT NULL,
 activation_code CHAR(10)
);
ALTER TABLE activation_codes ADD CONSTRAINT PK_activation_codes PRIMARY KEY (user_id);
CREATE TABLE borrower_cards (
 card_id CHAR(10) NOT NULL,
 expired_date DATE,
 activated_date DATE,
 username CHAR(10),
 user_id CHAR(10)
);
ALTER TABLE borrower_cards ADD CONSTRAINT PK_borrower_cards PRIMARY KEY (card_id);
CREATE TABLE student_infos (
 user_id CHAR(10) NOT NULL,
 student_id CHAR(10)
);
ALTER TABLE student_infos ADD CONSTRAINT PK_student_infos PRIMARY KEY (user_id);
CREATE TABLE wanted_copies (
 card_id CHAR(10) NOT NULL,
 book_number CHAR(10) NOT NULL,
 sequence_number CHAR(10) NOT NULL
);
ALTER TABLE wanted_copies ADD CONSTRAINT PK_wanted_copies PRIMARY KEY (card_id,book_number,sequence_number);
CREATE TABLE borrowed_copies (
 card_id CHAR(10) NOT NULL,
 book_number CHAR(10) NOT NULL,
 sequence_number CHAR(10) NOT NULL
);
ALTER TABLE borrowed_copies ADD CONSTRAINT PK_borrowed_copies PRIMARY KEY (card_id,book_number,sequence_number);
ALTER TABLE activation_codes ADD CONSTRAINT FK_activation_codes_0 FOREIGN KEY (user_id) REFERENCES user_info (user_id);
ALTER TABLE borrower_cards ADD CONSTRAINT FK_borrower_cards_0 FOREIGN KEY (user_id) REFERENCES user_info (user_id);
ALTER TABLE student_infos ADD CONSTRAINT FK_student_infos_0 FOREIGN KEY (user_id) REFERENCES user_info (user_id);
ALTER TABLE wanted_copies ADD CONSTRAINT FK_wanted_copies_0 FOREIGN KEY (card_id) REFERENCES borrower_cards (card_id);
ALTER TABLE wanted_copies ADD CONSTRAINT FK_wanted_copies_1 FOREIGN KEY (book_number,sequence_number) REFERENCES copies (book_number,sequence_number);
ALTER TABLE borrowed_copies ADD CONSTRAINT FK_borrowed_copies_0 FOREIGN KEY (card_id) REFERENCES borrower_cards (cardId);
ALTER TABLE borrowed_copies ADD CONSTRAINT FK_borrowed_copies_1 FOREIGN KEY (book_number,sequence_number) REFERENCES copies (book_number,sequence_number);


