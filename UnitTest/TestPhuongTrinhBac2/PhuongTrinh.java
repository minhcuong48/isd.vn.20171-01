/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuongtrinhbachai;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class PhuongTrinh {
    
    public static final BigDecimal TWO = new BigDecimal(2L);
    public static final BigDecimal FOUR = new BigDecimal("4");
    public static final BigDecimal VO_NGHIEM = BigDecimal.ZERO;
    public static final BigDecimal VO_SO_NGHIEM = BigDecimal.ONE;
    public static final BigDecimal CO_NGHIEM_KEP = new BigDecimal("2");
    public static final BigDecimal CO_HAI_NGHIEM_DON = new BigDecimal("3");
    
    public static BigDecimal canBac2(BigDecimal x, MathContext mc) {
	BigDecimal g = x.divide(TWO, mc);
	boolean done = false;
	final int maxIterations = mc.getPrecision() + 1;		
	for (int i = 0; !done && i < maxIterations; i++) {
		// r = (x/g + g) / 2
		BigDecimal r = x.divide(g, mc);
		r = r.add(g);
		r = r.divide(TWO, mc);
		done = r.equals(g);
		g = r;
	}
	return g;
}
    
    private final BigDecimal a;
    private final BigDecimal b;
    private final BigDecimal c;

    public PhuongTrinh(BigDecimal a, BigDecimal b, BigDecimal c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public boolean kiemTraVoSoNghiem() {
        return a.compareTo(BigDecimal.ZERO) == 0
                && b.compareTo(BigDecimal.ZERO) == 0
                && c.compareTo(BigDecimal.ZERO) == 0;
    }
    
    public BigDecimal delta() {
        return b.multiply(b).subtract(a.multiply(c).multiply(FOUR));
    }
    
    public boolean kiemTraVoNghiem() {
        return delta().compareTo(BigDecimal.ZERO) < 0;
    }
    
    public boolean kiemTraNghiemKep() {
        return delta().compareTo(BigDecimal.ZERO) == 0;
    }
    
    public boolean kiemTraHaiNghiemDon() {
        return delta().compareTo(BigDecimal.ZERO) > 0;
    }

    public BigDecimal nghiem1() {
        BigDecimal tuSo = BigDecimal.ZERO.subtract(b).subtract(PhuongTrinh.canBac2(delta(), MathContext.DECIMAL64));
        BigDecimal mauSo = a.multiply(TWO);
        return tuSo.divide(mauSo, MathContext.DECIMAL64);
    }
    
    public BigDecimal nghiem2() {
        BigDecimal tuSo = BigDecimal.ZERO.subtract(b).add(PhuongTrinh.canBac2(delta(), MathContext.DECIMAL64));
        BigDecimal mauSo = a.multiply(TWO);
        return tuSo.divide(mauSo, MathContext.DECIMAL64);
    }

    public BigDecimal[] giai() {
        BigDecimal[] ketQua = new BigDecimal[3];
        if(kiemTraVoNghiem()) {
            ketQua[0] = ketQua[1] = ketQua[2] = VO_NGHIEM;
        }
        else if(kiemTraVoSoNghiem()) {
            ketQua[0] = VO_SO_NGHIEM;
            ketQua[1] = ketQua[2] = BigDecimal.ZERO;
        }
        else if(kiemTraNghiemKep()) {
            ketQua[0] = CO_NGHIEM_KEP;
            ketQua[1] = nghiem1();
            ketQua[2] = nghiem2();
        }
        else if(kiemTraHaiNghiemDon()) {
            ketQua[0] = CO_HAI_NGHIEM_DON;
            ketQua[1] = nghiem1();
            ketQua[2] = nghiem2();
        }
        return ketQua;
    }
    
    public void hienKetQua() {
        BigDecimal[] ketQua = giai();
        System.out.format("%s, %s, %s", ketQua[0].toString(), ketQua[1].toString(), ketQua[2].toString());
    }
}
