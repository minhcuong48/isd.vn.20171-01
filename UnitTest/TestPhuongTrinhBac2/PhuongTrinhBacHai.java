/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phuongtrinhbachai;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class PhuongTrinhBacHai {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BigDecimal zero = new BigDecimal("0");
        
        PhuongTrinh pt;
        pt = new PhuongTrinh(new BigDecimal("-3"), new BigDecimal("12"), new BigDecimal("36"));
        System.out.println(pt.kiemTraVoSoNghiem());
        
        System.out.println(pt.delta());
        System.out.println(pt.kiemTraVoNghiem());
        pt.hienKetQua();
        BigDecimal so1 = new BigDecimal("6.0");
        BigDecimal so2 = new BigDecimal("6.00");
        System.out.println(so1.compareTo(so2) == 0);
    }
    
}
