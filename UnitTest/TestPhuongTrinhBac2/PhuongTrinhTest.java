package phuongtrinhbachai;

import java.math.BigDecimal;
import java.math.MathContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class PhuongTrinhTest {

    /**
     * Test of canBac2 method, of class PhuongTrinh.
     */
    @Test
    public void testCanBac2() {
        System.out.println("canBac2");
        BigDecimal x = new BigDecimal(4.0);
        MathContext mc = MathContext.DECIMAL64;
        BigDecimal expResult = new BigDecimal(2.0);
        BigDecimal result = PhuongTrinh.canBac2(x, mc);
        assertEquals(expResult, result);
    }

    /**
     * Test of kiemTraVoSoNghiem method, of class PhuongTrinh.
     */
    @Test
    public void testKiemTraVoSoNghiem() {
        System.out.println("kiemTraVoSoNghiem");
        PhuongTrinh instance = new PhuongTrinh(new BigDecimal("0"), new BigDecimal("0"), new BigDecimal("0"));
        boolean expResult = true;
        boolean result = instance.kiemTraVoSoNghiem();
        assertEquals(expResult, result);
    }

    /**
     * Test of delta method, of class PhuongTrinh.
     */
    @Test
    public void testDelta() {
        System.out.println("delta");
        PhuongTrinh instance = new PhuongTrinh(new BigDecimal("-3"), new BigDecimal("12"), new BigDecimal("36"));
        BigDecimal expResult = new BigDecimal("576");
        BigDecimal result = instance.delta();
        assertEquals(expResult, result);
    }

    /**
     * Test of kiemTraVoNghiem method, of class PhuongTrinh.
     */
    @Test
    public void testKiemTraVoNghiem() {
        System.out.println("kiemTraVoNghiem");
        PhuongTrinh instance = new PhuongTrinh(new BigDecimal("3"), new BigDecimal("12"), new BigDecimal("36"));
        boolean expResult = true;
        boolean result = instance.kiemTraVoNghiem();
        assertEquals(expResult, result);
    }

    /**
     * Test of kiemTraNghiemKep method, of class PhuongTrinh.
     */
    @Test
    public void testKiemTraNghiemKep() {
        System.out.println("kiemTraNghiemKep");
        PhuongTrinh instance = new PhuongTrinh(new BigDecimal("4"), new BigDecimal("12"), new BigDecimal("9"));
        boolean expResult = true;
        boolean result = instance.kiemTraNghiemKep();
        assertEquals(expResult, result);
    }

    /**
     * Test of kiemTraHaiNghiemDon method, of class PhuongTrinh.
     */
    @Test
    public void testKiemTraHaiNghiemDon() {
        System.out.println("kiemTraHaiNghiemDon");
        PhuongTrinh instance = new PhuongTrinh(new BigDecimal("-3"), new BigDecimal("12"), new BigDecimal("9"));
        boolean expResult = true;
        boolean result = instance.kiemTraHaiNghiemDon();
        assertEquals(expResult, result);
    }

    /**
     * Test of nghiem1 method, of class PhuongTrinh.
     */
    @Test
    public void testNghiem1() {
        System.out.println("nghiem1");
        PhuongTrinh instance = new PhuongTrinh(new BigDecimal("-3"), new BigDecimal("12"), new BigDecimal("36"));
        BigDecimal expResult = new BigDecimal("6");
        BigDecimal result = instance.nghiem1();
        boolean finalResult = expResult.compareTo(result) == 0;
        assertTrue(finalResult);
    }

    /**
     * Test of nghiem2 method, of class PhuongTrinh.
     */
    @Test
    public void testNghiem2() {
        System.out.println("nghiem2");
        PhuongTrinh instance = new PhuongTrinh(new BigDecimal("-3"), new BigDecimal("12"), new BigDecimal("36"));
        BigDecimal expResult = new BigDecimal("-2");
        BigDecimal result = instance.nghiem2();
        boolean finalResult = expResult.compareTo(result) == 0;
        assertTrue(finalResult);
    }

}
