package library.controller;

import library.entity.Copies;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddNewBookControllerTest {
    private AddNewBookController controller;
    @BeforeEach
    void setUp() {
        controller = new AddNewBookController();
    }

    @Test
    void addNewBook() {
        assertTrue(controller.AddNewBook("SQL Language", "NXB BKHN", "Nguyen Thi Thu Trang", "12412421", Copies.TypeOfCopies.BORROWABLE, 12000.0));
    }

}