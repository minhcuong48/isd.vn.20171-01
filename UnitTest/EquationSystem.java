public class EquationSystem {

    private static final double DELTA = 0.00001;

    double[] solve(double a1, double b1, double c1, double a2, double b2, double c2) {
        double[] result = new double[2];
        double d = a1 * b2 - a2 * b1;
        double dx = c1 * b2 - c2 * b1;
        double dy = a1 * c2 - a2 * c1;
        if (equals(d, 0.0)) {
            if (equals(dx, 0.0) && equals(dy, 0.0)) {
                // vô số nghiệm
                result[0] = Double.MAX_VALUE;
                result[1] = Double.MAX_VALUE;
            }
            if (!equals(dx, 0.0) || !equals(dy, 0.0)) {
                // vô nghiệm
                result[0] = Double.NaN;
                result[1] = Double.NaN;
            }
        } else {
            result[0] = dx/d;
            result[1] = dy/d;
        }
        return result;
    }

    private boolean equals(double a, double b) {
        return (Math.abs(a - b) < DELTA);
    }
}