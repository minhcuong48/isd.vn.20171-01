/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triangle;

/**
 *
 * @author bach0
 */
public class Triangle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }

    public boolean isTriangle(double a, double b, double c) {
        double x = Double.max(Double.max(a, b), c);
        double y = Double.min(Double.max(a, b), c);
        double z = a + b + c - x - y;
        return x < y + z;
    }
}
