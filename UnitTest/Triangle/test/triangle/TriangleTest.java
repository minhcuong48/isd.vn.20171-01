/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triangle;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bach0
 */
public class TriangleTest {

    public TriangleTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of isTriangle method, of class Triangle.
     */
    @Test
    public void testIsTriangle() {
        System.out.println("isTriangle");
        Triangle t = new Triangle();
        assertEquals(true, t.isTriangle(3, 4, 5));
        assertEquals(true, t.isTriangle(4, 6, 5));
        assertEquals(true, t.isTriangle(8, 4, 6));
        assertEquals(false, t.isTriangle(1, 2, 3));
        assertEquals(false, t.isTriangle(4, 2, 2));
        assertEquals(false, t.isTriangle(7, 4, 3));
        assertEquals(false, t.isTriangle(-1, 1, 3));
        assertEquals(false, t.isTriangle(1, -2, 3));
        assertEquals(false, t.isTriangle(1, 2, -3));
        assertEquals(false, t.isTriangle(1, 2, 4));
        assertEquals(false, t.isTriangle(6, 2, 3));
        assertEquals(false, t.isTriangle(1, 7, 3));
        assertEquals(false, t.isTriangle(-3, -4, -5));
        assertEquals(false, t.isTriangle(0, 0, 0));
    }

}
