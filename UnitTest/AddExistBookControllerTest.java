package library.controller;

import library.entity.Copies;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddExistBookControllerTest {

    private AddExistBookController controller;
    @BeforeEach
    void setUp() {
        controller = new AddExistBookController();
    }

    @Test
    void addExistBook() {
        assertFalse(controller.AddExistBook("001232", 4, Copies.TypeOfCopies.REFERENCE, 12000.0));
        assertTrue(controller.AddExistBook("000001", 1, Copies.TypeOfCopies.BORROWABLE, 120000.0));
    }

}