import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EquationSystemTest {

    private EquationSystem classUnderTest;
    private static final double DELTA = 0.00001;

    @Before
    public void setUp() throws Exception {
        classUnderTest = new EquationSystem();
    }

    @Test
    public void solve() throws Exception {
        double result[] = {3.0, -2.0};
        assertArrayEquals(result, classUnderTest.solve(4.0, 3.0, 6.0, 2.0, 1.0, 4.0), DELTA);

        result[0] = 1.0; result[1] = 1.0;
        assertArrayEquals(result, classUnderTest.solve(1.0, 1.0, 2.0, 1.0, 0.0, 1.0), DELTA);

        // vô số nghiệm
        result[0] = Double.MAX_VALUE; result[1] = Double.MAX_VALUE;
        assertArrayEquals(result, classUnderTest.solve(1.0, -3.0, 3.0, 3.0, -9.0, 9.0), DELTA);

        // vô nghiệm
        result[0] = Double.NaN; result[1] = Double.NaN;
        assertArrayEquals(result, classUnderTest.solve(1.0, -3.0, 3.0, 3.0, -9.0, 5.0), DELTA);
    }

}
