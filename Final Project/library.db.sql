CREATE TABLE IF NOT EXISTS `wanted_copies` (
	`id`	INTEGER,
	`book_number`	VARCHAR ( 6 ),
	`sequence_number`	INTEGER,
	`borrowed_date`	DATE,
	FOREIGN KEY(`book_number`,`sequence_number`) REFERENCES `copies`(`book_number`,`sequence_number`),
	FOREIGN KEY(`id`) REFERENCES `borrower_card`(`id`),
	PRIMARY KEY(`id`,`book_number`,`sequence_number`)
);
CREATE TABLE IF NOT EXISTS `user` (
	`username`	CHAR ( 128 ) NOT NULL,
	`password`	VARCHAR ( 128 ) NOT NULL,
	`full_name`	VARCHAR ( 128 ) NOT NULL,
	`email`	VARCHAR ( 254 ) NOT NULL,
	`contact`	VARCHAR ( 256 ) NOT NULL,
	`gender`	VARCHAR ( 20 ) NOT NULL,
	`status`	CHAR ( 20 ) NOT NULL,
	`activate_token`	CHAR ( 32 ),
	`type`	CHAR ( 20 ) NOT NULL,
	PRIMARY KEY(`username`)
);
CREATE TABLE IF NOT EXISTS `student` (
	`username`	CHAR ( 128 ) NOT NULL,
	`student_id`	CHAR ( 128 ) NOT NULL,
	`period_start`	DATE NOT NULL,
	`period_end`	DATE NOT NULL,
	PRIMARY KEY(`username`),
	FOREIGN KEY(`username`) REFERENCES `user`
);
CREATE TABLE IF NOT EXISTS `copies` (
	`book_number`	CHAR ( 6 ) NOT NULL,
	`sequence_number`	INTEGER,
	`type_of_copies`	VARCHAR ( 16 ) NOT NULL,
	`price`	DOUBLE,
	`status`	VARCHAR ( 16 ) NOT NULL,
	FOREIGN KEY(`book_number`) REFERENCES `book`,
	PRIMARY KEY(`book_number`,`sequence_number`)
);
CREATE TABLE IF NOT EXISTS `borrower_card` (
	`id`	INTEGER NOT NULL,
	`username`	CHAR ( 128 ) NOT NULL,
	`issued_date`	DATE NOT NULL,
	`expired_date`	DATE NOT NULL,
	FOREIGN KEY(`username`) REFERENCES `user`,
	PRIMARY KEY(`id`)
);
CREATE TABLE IF NOT EXISTS `borrowed_copies` (
	`id`	INTEGER,
	`book_number`	CHAR ( 6 ),
	`sequence_number`	INTEGER,
	`lent_date`	DATE,
	`expected_return_date`	DATE,
	PRIMARY KEY(`id`,`book_number`,`sequence_number`),
	FOREIGN KEY(`book_number`,`sequence_number`) REFERENCES `copies`(`book_number`,`sequence_number`),
	FOREIGN KEY(`id`) REFERENCES `borrower_card`(`id`)
);
CREATE TABLE IF NOT EXISTS `book` (
	`book_number`	CHAR ( 6 ) NOT NULL,
	`title`	VARCHAR ( 256 ) NOT NULL,
	`publisher`	VARCHAR ( 256 ) NOT NULL,
	`authors`	VARCHAR ( 128 ) NOT NULL,
	`isbn`	VARCHAR ( 32 ) NOT NULL,
	PRIMARY KEY(`book_number`)
);
