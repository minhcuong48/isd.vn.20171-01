/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.helper;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.Month;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import library.entity.WantedCopies;
import static library.helper.DatabaseHelper.getConnection;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class BorrowBooksHelper {

    private static BorrowBooksHelper defaultInstance = null;

    public static BorrowBooksHelper getDefault() {
        if (defaultInstance == null) {
            defaultInstance = new BorrowBooksHelper();
        }

        return defaultInstance;
    }

    // private constructor
    private BorrowBooksHelper() {
    }

    // execute sql quickly
    public void executeSql(String sql) {
        Connection conn = getConnection();
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            conn.commit();
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    // for test
    public void initDataBeforeTestRegisterToBorrowBooks() {
        Connection conn = DatabaseHelper.getConnection();
        // create user
        String sql = "INSERT INTO user(username, password, email, full_name, contact, gender, status, type) VALUES ('gmc','123456','gmc@gmail.com','GMC','GMC','MALE','ACTIVE','BORROWER');";
        executeSql(sql);

        // create card for gmc
        sql = "INSERT INTO borrower_card(id, username, issued_date, expired_date) VALUES (?, ?, ?, ?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 8484);
            stmt.setString(2, "gmc");
            stmt.setDate(3, Date.valueOf(LocalDate.of(2015, Month.AUGUST, 4)));
            stmt.setDate(4, Date.valueOf(LocalDate.of(2019, Month.AUGUST, 4)));
            stmt.execute();
            conn.commit();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        // create one book
        sql = "INSERT INTO book(book_number, title, publisher, authors, isbn) VALUES ('IT8484','Test Book 8484','xxx','xxx','xxx');";
        executeSql(sql);

        // create one copy
        sql = "INSERT INTO copies(book_number, sequence_number, type_of_copies, price, status) VALUES("
                + "'" + "IT8484" + "',"
                + "'" + "1" + "',"
                + "'" + "BORROWABLE" + "',"
                + "'" + "20000" + "',"
                + "'" + "AVAILABLE" + "')";
        executeSql(sql);
    }

    public void initDataBeforeTestConfirmBorrowBooks() {
        initDataBeforeTestRegisterToBorrowBooks();
        // create test wanted copies
        ObservableList<WantedCopies> list = FXCollections.observableArrayList();
        WantedCopies wantedCopy = new WantedCopies();
        wantedCopy.setCardId(8484);
        wantedCopy.setBookNumber("IT8484");
        wantedCopy.setSequenceNumber(1);
        wantedCopy.setBorrowedDate(LocalDate.now());
        list.add(wantedCopy);
        WantedCopies.add(list);
    }
}
