/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.controller;

import javafx.collections.ObservableList;
import library.entity.BorrowedCopies;
import library.entity.ChoosedCopies;
import library.entity.Copies;
import library.entity.WantedCopies;
import library.helper.BorrowBooksHelper;
import library.helper.Constant;
import library.helper.DatabaseHelper;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class RegisterToBorrowBooksControllerTest {

    public RegisterToBorrowBooksControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        // init database before test
        DatabaseHelper.clearDatabase();
        DatabaseHelper.createTablesIfNotExists();
        BorrowBooksHelper.getDefault().initDataBeforeTestRegisterToBorrowBooks();
    }

    @AfterClass
    public static void tearDownClass() {
        DatabaseHelper.clearDatabase();
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {

    }

    /**
     * Test of start method, of class RegisterToBorrowBooksController.
     */
    @Test
    public void testStart() {
        System.out.println("start");
        RegisterToBorrowBooksController instance = new RegisterToBorrowBooksController(8484);
        assertNotEquals(instance, null);
    }

    /**
     * Test of getChoosedCopies method, of class
     * RegisterToBorrowBooksController.
     */
    @Test
    public void testGetChoosedCopies() {
        System.out.println("getChoosedCopies");
        RegisterToBorrowBooksController instance = new RegisterToBorrowBooksController(8484);
        int result = instance.getChoosedCopies().size();
        assertTrue(result > 0);
    }

    /**
     * Test of getAvailableChoosedCopies method, of class
     * RegisterToBorrowBooksController.
     */
    @Test
    public void testGetAvailableChoosedCopies() {
        System.out.println("getAvailableChoosedCopies");
        RegisterToBorrowBooksController instance = new RegisterToBorrowBooksController(8484);
        boolean result = instance.getAvailableChoosedCopies().size() > 0;
        assertTrue(result);
    }

    /**
     * Test of isNumBorrowedCopiesValid method, of class
     * RegisterToBorrowBooksController.
     */
    @Test
    public void testIsNumBorrowedCopiesValid() {
        System.out.println("isNumBorrowedCopiesValid");
        ObservableList<ChoosedCopies> wantedItems = ChoosedCopies.all(Copies.Status.AVAILABLE);
        int numWantedCopies = WantedCopies.all(8484).size();
        int numBorrowedCopies = BorrowedCopies.countBorrowedCopies(8484);
        boolean expResult = false;
        if (wantedItems.size() + numWantedCopies + numBorrowedCopies <= Constant.MAX_NUM_WANTED_COPIES) {
            expResult = true;
        }

        RegisterToBorrowBooksController instance = new RegisterToBorrowBooksController(8484);

        boolean result = instance.isNumBorrowedCopiesValid(wantedItems);
        assertEquals(expResult, result);
    }

    /**
     * Test of loadBorrowerCardInfo method, of class
     * RegisterToBorrowBooksController.
     */
    @Test
    public void testLoadBorrowerCardInfo() {
        System.out.println("loadBorrowerCardInfo");
        RegisterToBorrowBooksController instance = new RegisterToBorrowBooksController(8484);
        instance.loadBorrowerCardInfo();
    }

}
