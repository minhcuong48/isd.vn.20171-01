/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.controller;

import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import library.entity.WantedCopies;
import library.helper.BorrowBooksHelper;
import library.helper.DatabaseHelper;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class ConfirmBorrowBooksControllerTest {

    public ConfirmBorrowBooksControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        DatabaseHelper.clearDatabase();
        DatabaseHelper.createTablesIfNotExists();
        BorrowBooksHelper.getDefault().initDataBeforeTestConfirmBorrowBooks();
    }

    @AfterClass
    public static void tearDownClass() {
        DatabaseHelper.clearDatabase();
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of start method, of class ConfirmBorrowBooksController.
     */
    @Test
    public void testStart() {
        System.out.println("start");
        ConfirmBorrowBooksController instance = new ConfirmBorrowBooksController();
        assertNotEquals(instance, null);
    }

    /**
     * Test of getWantedCopies method, of class ConfirmBorrowBooksController.
     */
    @Test
    public void testGetWantedCopies() {
        System.out.println("getWantedCopies");
        ConfirmBorrowBooksController instance = new ConfirmBorrowBooksController();
        instance.setBorrowerCard(8484);
        ObservableList<WantedCopies> wantedCopies = instance.getWantedCopies();
        boolean result = wantedCopies.size() > 0;
        assertTrue(result);
    }

    /**
     * Test of search method, of class ConfirmBorrowBooksController.
     */
    @Test
    public void testSetBorrowerCard() {
        System.out.println("search");
        int cardId = 0;
        ConfirmBorrowBooksController instance = new ConfirmBorrowBooksController();
        boolean result = instance.setBorrowerCard(8488);
        assertFalse(result);
        result = instance.setBorrowerCard(8484);
        assertTrue(result);
    }

    /**
     * Test of confirmBorrowBooks method, of class ConfirmBorrowBooksController.
     */
    @Test
    public void testConfirmBorrowBooks() {
        System.out.println("confirmBorrowBooks");
        ObservableList<WantedCopies> items = FXCollections.observableArrayList();
        WantedCopies wantedCopy = new WantedCopies();
        wantedCopy.setAuthors("xxx");
        wantedCopy.setBookNumber("IT8484");
        wantedCopy.setBorrowedDate(LocalDate.now());
        wantedCopy.setCardId(8484);
        wantedCopy.setIsbn("xxx");
        wantedCopy.setPrice(20000.0);
        wantedCopy.setPublisher("xxx");
        wantedCopy.setSequenceNumber(1);
        wantedCopy.setTitle("Test Book 8484");
        wantedCopy.setUsername("gmc");

        items.add(wantedCopy);

        ConfirmBorrowBooksController instance = new ConfirmBorrowBooksController();
        instance.confirmBorrowBooks(items);
    }

}
