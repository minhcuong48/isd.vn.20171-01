package library.service;

import java.sql.Connection;
import java.sql.Statement;
import library.entity.Copies;
import library.helper.DatabaseHelper;
import org.junit.*;
import static org.junit.Assert.*;

public class AddExistBookManagerTest {

    private AddExistBookManager classTest;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
        DatabaseHelper.clearDatabase();
    }

    @Before
    public void setUp() throws Exception {
        classTest = new AddExistBookManager();
        DatabaseHelper.clearDatabase();
        DatabaseHelper.createTablesIfNotExists();
        insertTestRow();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void addExistBook() throws Exception {
        assertEquals(false, classTest.addExistBook("", 1, Copies.TypeOfCopies.BORROWABLE, 120000.0));
        assertEquals(false, classTest.addExistBook("000000", 0, Copies.TypeOfCopies.BORROWABLE, 120000.0));
        assertEquals(false, classTest.addExistBook("110000", 3, Copies.TypeOfCopies.BORROWABLE, 120000.0));
        assertEquals(false, classTest.addExistBook("000000", -1, Copies.TypeOfCopies.BORROWABLE, 120000.0));
        assertEquals(false, classTest.addExistBook("000000", 2, Copies.TypeOfCopies.BORROWABLE, -40.0));
        assertEquals(true, classTest.addExistBook("000000", 2, Copies.TypeOfCopies.BORROWABLE, 120000.0));
    }

    private void insertTestRow() throws Exception {
        Connection conn = DatabaseHelper.getConnection();
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("INSERT INTO book VALUES ('000000','Title Sample','Publisher Sample','Authors Sample','ISBN Sample');");
        conn.commit();
        stmt.executeUpdate("INSERT INTO copies VALUES ('000000','1','BORROWABLE','120000.0','AVAILABLE');");
        conn.commit();
        stmt.close();
    }

}