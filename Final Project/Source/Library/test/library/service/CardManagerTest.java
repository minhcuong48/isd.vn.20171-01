/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.service;

import java.time.LocalDate;
import java.time.Month;
import library.entity.Borrower;
import library.entity.BorrowerCard;
import library.entity.Student;
import library.entity.User;
import library.helper.DatabaseHelper;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Dang Xuan Bach
 */
public class CardManagerTest {

    public CardManagerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
        DatabaseHelper.clearDatabase();
    }

    @Before
    public void setUp() {
        DatabaseHelper.clearDatabase();
        DatabaseHelper.createTablesIfNotExists();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addCard method, of class CardManager.
     */
    @Test
    public void testAddCard() {
        System.out.println("addCard");
        CardManager instance = new CardManager();

        System.out.println("* Check null input");
        assertEquals(false, instance.addCard(null));

        System.out.println("* Check missing fields input");
        BorrowerCard card = new BorrowerCard();
        assertEquals(false, instance.addCard(card));

        System.out.println("* Check valid input");
        UserManager userManager = new UserManager();
        User user = new Borrower();
        user.setUsername("test_user");
        user.setPassword("password");
        user.setStatus(User.Status.NOT_ACTIVATED);
        user.setGender("Male");
        user.setFullName("Test User");
        user.setEmail("test@example.com");
        user.setContact("1 ABC Street");
        assertEquals(true, userManager.addUser(user));
        card = new BorrowerCard();
        card.setId(1234567);
        card.setIssuedDate(LocalDate.now());
        card.setExpiredDate(LocalDate.now().plusYears(4));
        card.setUser(user);

        System.out.println("* Check valid input");
        assertEquals(true, instance.addCard(card));

        System.out.println("* Check duplicate id");
        assertEquals(false, instance.addCard(card));
        card.setId(11114567);

        System.out.println("* Check muliple card");
        assertEquals(true, instance.addCard(card));

        System.out.println("* Check student input");
        Student student = new Student();
        student.setUsername("test_student");
        student.setPassword("password");
        student.setStatus(User.Status.NOT_ACTIVATED);
        student.setGender("Male");
        student.setFullName("Test User");
        student.setEmail("test@example.com");
        student.setContact("1 ABC Street");
        student.setStudentId("123456");
        student.setStudyPeriodStart(LocalDate.now());
        student.setStudyPeriodEnd(LocalDate.now().plusYears(4));
        assertEquals(true, userManager.addUser(student));
        card = new BorrowerCard();
        card.setId(12345678);
        card.setIssuedDate(LocalDate.now());
        card.setExpiredDate(LocalDate.now().plusYears(1));
        card.setUser(student);
        assertEquals(true, instance.addCard(card));
        card.setId(12345698);

        System.out.println("* Check student overlapped card");
        assertEquals(false, instance.addCard(card));

        System.out.println("* Check student not overlapped card");
        card.setIssuedDate(LocalDate.now().plusYears(2));
        card.setExpiredDate(LocalDate.now().plusYears(3));
        assertEquals(true, instance.addCard(card));
    }

    /**
     * Test of updateCard method, of class CardManager.
     */
    @Test
    public void testUpdateCard() {
        System.out.println("updateCard");
        CardManager instance = new CardManager();
        System.out.println("* Check null input");
        assertEquals(false, instance.updateCard(null));
        System.out.println("* Check missing fields input");
        BorrowerCard card = new BorrowerCard();
        assertEquals(false, instance.updateCard(card));
        System.out.println("* Check valid input");
        UserManager userManager = new UserManager();
        Student student = new Student();
        student.setUsername("test_student");
        student.setPassword("password");
        student.setStatus(User.Status.NOT_ACTIVATED);
        student.setGender("Male");
        student.setFullName("Test User");
        student.setEmail("test@example.com");
        student.setContact("1 ABC Street");
        student.setStudentId("123456");
        student.setStudyPeriodStart(LocalDate.now());
        student.setStudyPeriodEnd(LocalDate.now().plusYears(4));
        assertEquals(true, userManager.addUser(student));
        card = new BorrowerCard();
        card.setId(12345678);
        card.setIssuedDate(LocalDate.now());
        card.setExpiredDate(LocalDate.now().plusYears(1));
        card.setUser(student);
        assertEquals(true, instance.addCard(card));
        card.setExpiredDate(LocalDate.now().plusYears(2));
        assertEquals(true, instance.updateCard(card));
        assertEquals(LocalDate.now().plusYears(2), instance.getCardById(card.getId()).getExpiredDate());
        System.out.println("* Check update expired time pass period end");
        card.setExpiredDate(LocalDate.now().plusYears(10));
        assertEquals(false, instance.updateCard(card));
    }

    /**
     * Test of getCardByUser method, of class CardManager.
     */
    @Test
    public void testGetCardByUser() {
        System.out.println("getCardByUser");
        CardManager instance = new CardManager();
        System.out.println("* Check null input");
        assertEquals(true, instance.getCardByUser(null).isEmpty());
        System.out.println("* Check non-exist user");
        User user = new Borrower();
        assertEquals(true, instance.getCardByUser(user).isEmpty());
        user.setUsername("test_user");
        assertEquals(true, instance.getCardByUser(user).isEmpty());
        System.out.println("* Check user with no card");
        UserManager userManager = new UserManager();
        Student student = new Student();
        student.setUsername("test_student");
        student.setPassword("password");
        student.setStatus(User.Status.NOT_ACTIVATED);
        student.setGender("Male");
        student.setFullName("Test User");
        student.setEmail("test@example.com");
        student.setContact("1 ABC Street");
        student.setStudentId("123456");
        student.setStudyPeriodStart(LocalDate.now());
        student.setStudyPeriodEnd(LocalDate.now().plusYears(4));
        assertEquals(true, userManager.addUser(student));
        assertEquals(true, instance.getCardByUser(student).isEmpty());

        System.out.println("* Check user with card(s)");
        BorrowerCard card = new BorrowerCard();
        card.setId(12345678);
        card.setIssuedDate(LocalDate.now());
        card.setExpiredDate(LocalDate.now().plusYears(1));
        card.setUser(student);
        assertEquals(true, instance.addCard(card));
        assertEquals(false, instance.getCardByUser(student).isEmpty());
    }

    /**
     * Test of getCardById method, of class CardManager.
     */
    @Test
    public void testGetCardById() {
        System.out.println("getCardById");
        CardManager instance = new CardManager();
        System.out.println("* Check valid input");
        UserManager userManager = new UserManager();
        Student student = new Student();
        student.setUsername("test_student");
        student.setPassword("password");
        student.setStatus(User.Status.NOT_ACTIVATED);
        student.setGender("Male");
        student.setFullName("Test User");
        student.setEmail("test@example.com");
        student.setContact("1 ABC Street");
        student.setStudentId("123456");
        student.setStudyPeriodStart(LocalDate.now());
        student.setStudyPeriodEnd(LocalDate.now().plusYears(4));
        assertEquals(true, userManager.addUser(student));
        BorrowerCard card = new BorrowerCard();
        card.setId(12345678);
        card.setIssuedDate(LocalDate.now());
        card.setExpiredDate(LocalDate.now().plusYears(1));
        card.setUser(student);
        assertEquals(true, instance.addCard(card));
        assertNotEquals(null, instance.getCardById(12345678));
    }

    /**
     * Test of getAllCards method, of class CardManager.
     */
    @Test
    public void testGetAllCards() {
        System.out.println("getAllCards");
        CardManager instance = new CardManager();
        System.out.println("* Check empty");
        assertEquals(true, instance.getAllCards().isEmpty());

        System.out.println("* Check user with no card");
        UserManager userManager = new UserManager();
        Student student = new Student();
        student.setUsername("test_student");
        student.setPassword("password");
        student.setStatus(User.Status.NOT_ACTIVATED);
        student.setGender("Male");
        student.setFullName("Test User");
        student.setEmail("test@example.com");
        student.setContact("1 ABC Street");
        student.setStudentId("123456");
        student.setStudyPeriodStart(LocalDate.now());
        student.setStudyPeriodEnd(LocalDate.now().plusYears(4));
        assertEquals(true, userManager.addUser(student));
        BorrowerCard card = new BorrowerCard();
        card.setId(12345678);
        card.setIssuedDate(LocalDate.now());
        card.setExpiredDate(LocalDate.now().plusYears(1));
        card.setUser(student);
        assertEquals(true, instance.addCard(card));
        assertEquals(1, instance.getAllCards().size());

        System.out.println("* Check after update");
        card.setExpiredDate(LocalDate.now().plusYears(2));
        assertEquals(true, instance.updateCard(card));
        assertEquals(1, instance.getAllCards().size());
    }

    /**
     * Test of getActiveCards method, of class CardManager.
     */
    @Test
    public void testGetActiveCards() {
        System.out.println("getActiveCards");
        CardManager instance = new CardManager();
        System.out.println("* Check user with no card");
        UserManager userManager = new UserManager();
        Student student = new Student();
        student.setUsername("test_student");
        student.setPassword("password");
        student.setStatus(User.Status.NOT_ACTIVATED);
        student.setGender("Male");
        student.setFullName("Test User");
        student.setEmail("test@example.com");
        student.setContact("1 ABC Street");
        student.setStudentId("123456");
        student.setStudyPeriodStart(LocalDate.of(2009, Month.AUGUST, 11));
        student.setStudyPeriodEnd(LocalDate.of(2015, Month.AUGUST, 11));
        assertEquals(true, userManager.addUser(student));
        assertEquals(true, instance.getActiveCards(student, LocalDate.MIN, LocalDate.MAX, true, true).isEmpty());
        BorrowerCard card = new BorrowerCard();
        card.setId(12345678);
        card.setIssuedDate(LocalDate.of(2010, Month.AUGUST, 11));
        card.setExpiredDate(LocalDate.of(2014, Month.AUGUST, 11));
        card.setUser(student);
        assertEquals(true, instance.addCard(card));
        System.out.println("* Check range embraces cards");
        assertEquals(false, instance.getActiveCards(student, LocalDate.MIN, LocalDate.MAX, true, true).isEmpty());
        System.out.println("* Check range inside cards");
        assertEquals(false, instance.getActiveCards(student, LocalDate.of(2012, Month.AUGUST, 11), LocalDate.of(2013, Month.AUGUST, 11), true, true).isEmpty());
        System.out.println("* Check range boundaries");
        assertEquals(true, instance.getActiveCards(student, LocalDate.MIN, LocalDate.of(2010, Month.AUGUST, 11), false, false).isEmpty());
    }

}
