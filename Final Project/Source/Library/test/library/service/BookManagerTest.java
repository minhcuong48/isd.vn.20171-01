package library.service;

import library.entity.Book;
import library.helper.DatabaseHelper;
import org.junit.*;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;

import static org.junit.Assert.*;

public class BookManagerTest {

    private BookManager classTest;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
        DatabaseHelper.clearDatabase();
    }

    @Before
    public void setUp() throws Exception {
        classTest = new BookManager();
        DatabaseHelper.clearDatabase();
        DatabaseHelper.createTablesIfNotExists();
        insertTestRow();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void checkBookExistTest() throws Exception {
        assertEquals(true, classTest.checkBookExist("000000"));
        assertEquals(false, classTest.checkBookExist(""));
        assertEquals(false, classTest.checkBookExist("000002"));
    }

    private void insertTestRow() throws Exception {
        Connection conn = DatabaseHelper.getConnection();
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("INSERT INTO book VALUES ('000000','Title Sample','Publisher Sample','Authors Sample','ISBN Sample');");
        conn.commit();
        stmt.executeUpdate("INSERT INTO copies VALUES ('000000','1','BORROWABLE','120000.0','AVAILABLE');");
        conn.commit();
        stmt.close();
    }
}