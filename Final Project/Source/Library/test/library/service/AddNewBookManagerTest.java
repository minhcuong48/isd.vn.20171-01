package library.service;

import library.entity.Copies;
import library.helper.DatabaseHelper;
import org.junit.*;
import static org.junit.Assert.*;

public class AddNewBookManagerTest {

    private AddNewBookManager classTest;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
        DatabaseHelper.clearDatabase();
    }

    @Before
    public void setUp() throws Exception {
        classTest = new AddNewBookManager();
        DatabaseHelper.clearDatabase();
        DatabaseHelper.createTablesIfNotExists();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void addNewBook() throws Exception {
        assertEquals(false, classTest.addNewBook("", "", "", "", Copies.TypeOfCopies.BORROWABLE, 0));
        assertEquals(false, classTest.addNewBook("", "", "", "", Copies.TypeOfCopies.BORROWABLE, 10000.0));
        assertEquals(false, classTest.addNewBook("Title 1", "", "", "", Copies.TypeOfCopies.BORROWABLE, 10000.0));
        assertEquals(false, classTest.addNewBook("Title 2", "Publisher", "Authors", "ISBN", Copies.TypeOfCopies.BORROWABLE, 0));
        assertEquals(false, classTest.addNewBook("Title 3", "Publisher", "Authors", "ISBN", Copies.TypeOfCopies.BORROWABLE, -50000.0));
        assertEquals(true, classTest.addNewBook("Title 4", "Publisher", "Authors", "ISBN", Copies.TypeOfCopies.BORROWABLE, 10000.0));
    }

}