/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.service;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import library.entity.Borrower;
import library.entity.BorrowerCard;
import library.entity.Student;
import library.entity.User;
import library.helper.DatabaseHelper;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Dang Xuan Bach
 */
public class UserManagerTest {

    public UserManagerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
        DatabaseHelper.clearDatabase();
    }

    @Before
    public void setUp() {
        DatabaseHelper.clearDatabase();
        DatabaseHelper.createTablesIfNotExists();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addUser method, of class UserManager.
     */
    @Test
    public void testAddUser() {
        System.out.println("addUser");
        User user = null;
        UserManager instance = new UserManager();
        System.out.println("* Check null pointer input");
        boolean expResult = false;
        boolean result = instance.addUser(user);
        assertEquals(expResult, result);

        System.out.println("* Check non-existed user input with empty database");
        user = new Borrower();
        user.setUsername("test_user");
        user.setPassword("password");
        user.setStatus(User.Status.NOT_ACTIVATED);
        user.setGender("Male");
        user.setFullName("Test User");
        user.setEmail("test@example.com");
        user.setContact("1 ABC Street");
        assertEquals(true, instance.addUser(user));

        System.out.println("* Check non-existed user input with filled database");
        user.setUsername("test_user2");
        assertEquals(true, instance.addUser(user));

        System.out.println("* Check existed user input");
        user.setUsername("test_user");
        assertEquals(false, instance.addUser(user));

        System.out.println("* Check invalid study period input");
        Student student = new Student();
        student.setUsername("test_student");
        student.setPassword("password2");
        student.setStatus(User.Status.NOT_ACTIVATED);
        student.setGender("Feale");
        student.setFullName("Test Student");
        student.setEmail("student@example.com");
        student.setContact("2 ABC Street");
        student.setStudentId("123456");
        student.setStudyPeriodStart(LocalDate.of(2010, Month.JANUARY, 10));
        student.setStudyPeriodEnd(LocalDate.of(2009, Month.JANUARY, 10));
        assertEquals(false, instance.addUser(student));

        System.out.println("* Check valid study period input");
        student.setStudyPeriodStart(LocalDate.of(2009, Month.JANUARY, 10));
        student.setStudyPeriodEnd(LocalDate.of(2010, Month.JANUARY, 10));
        assertEquals(true, instance.addUser(student));

        System.out.println("* Check ignored input");
        user.setUsername("test_user3");
        user.setStatus(User.Status.ACTIVE);
        assertEquals(true, instance.addUser(user));
        assertEquals(User.Status.NOT_ACTIVATED, instance.getUserByUsername("test_user3").getStatus());

        System.out.println("* Check missing field input");
        user.setUsername("");
        assertEquals(false, instance.addUser(user));
        user.setUsername(null);
        assertEquals(false, instance.addUser(user));
        user.setUsername("test_user3");
        user.setGender("");
        assertEquals(false, instance.addUser(user));
        user.setGender(null);
        assertEquals(false, instance.addUser(user));
    }

    /**
     * Test of updateUser method, of class UserManager.
     */
    @Test
    public void testUpdateUser() {
        System.out.println("updateUser");
        User user = null;
        UserManager instance = new UserManager();
        System.out.println("* Check null pointer input");
        boolean expResult = false;
        boolean result = instance.updateUser(user);
        assertEquals(expResult, result);

        System.out.println("* Check non-existed user input with empty database");
        user = new Borrower();
        user.setUsername("test_user");
        user.setPassword("password");
        user.setStatus(User.Status.NOT_ACTIVATED);
        user.setGender("Male");
        user.setFullName("Test User");
        user.setEmail("test@example.com");
        user.setContact("1 ABC Street");
        assertEquals(false, instance.updateUser(user));

        System.out.println("* Check non-existed user input with filled database");
        assertEquals(true, instance.addUser(user));
        user.setUsername("test_user2");
        assertEquals(false, instance.updateUser(user));

        System.out.println("* Check existed user input");
        user.setUsername("test_user");
        user.setContact("1 DEF Street");
        assertEquals(true, instance.updateUser(user));
        assertEquals("1 DEF Street", instance.getUserByUsername(user.getUsername()).getContact());

        System.out.println("* Check invalid study period input");
        Student student = new Student();
        student.setUsername("test_student");
        student.setPassword("password2");
        student.setStatus(User.Status.NOT_ACTIVATED);
        student.setGender("Feale");
        student.setFullName("Test Student");
        student.setEmail("student@example.com");
        student.setContact("2 ABC Street");
        student.setStudentId("123456");
        student.setStudyPeriodStart(LocalDate.of(2009, Month.JANUARY, 10));
        student.setStudyPeriodEnd(LocalDate.of(2010, Month.JANUARY, 10));
        assertEquals(true, instance.addUser(student));
        student = (Student) instance.getUserByUsername(student.getUsername());
        student.setStudyPeriodStart(LocalDate.of(2010, Month.JANUARY, 10));
        student.setStudyPeriodEnd(LocalDate.of(2009, Month.JANUARY, 10));
        assertEquals(false, instance.updateUser(student));

        System.out.println("* Check valid study period input");
        student = (Student) instance.getUserByUsername(student.getUsername());
        student.setStudyPeriodStart(LocalDate.of(2010, Month.JANUARY, 10));
        student.setStudyPeriodEnd(LocalDate.of(2011, Month.JANUARY, 10));
        assertEquals(true, instance.updateUser(student));
        student = (Student) instance.getUserByUsername(student.getUsername());
        assertEquals(LocalDate.of(2011, Month.JANUARY, 10), student.getStudyPeriodEnd());
        assertEquals(LocalDate.of(2010, Month.JANUARY, 10), student.getStudyPeriodStart());

        System.out.println("* Check ignored input");
        user = instance.getUserByUsername("test_user");
        user.setStatus(User.Status.ACTIVE);
        assertEquals(true, instance.updateUser(user));
        assertEquals(User.Status.NOT_ACTIVATED, instance.getUserByUsername("test_user").getStatus());

        System.out.println("* Check missing field input");
        user.setUsername("");
        assertEquals(false, instance.updateUser(user));
        user.setUsername(null);
        assertEquals(false, instance.updateUser(user));
        user.setUsername("test_user");
        user.setGender("");
        String saved = instance.getUserByUsername("test_user").getGender();
        assertEquals(true, instance.updateUser(user));
        assertEquals(saved, instance.getUserByUsername("test_user").getGender());
        user.setGender(null);
        assertEquals(true, instance.updateUser(user));
        assertEquals(saved, instance.getUserByUsername("test_user").getGender());
    }

    /**
     * Test of activateUser method, of class UserManager.
     */
    @Test
    public void testActivateUser() {
        System.out.println("activateUser");
        System.out.println("* Check empty input");
        String activateToken = "";
        UserManager instance = new UserManager();
        User expResult = null;
        User result = instance.activateUser(activateToken);
        assertEquals(expResult, result);
        System.out.println("* Check invalid token input");
        User user = new Borrower();
        user.setUsername("test_user");
        user.setPassword("password");
        user.setStatus(User.Status.NOT_ACTIVATED);
        user.setGender("Male");
        user.setFullName("Test User");
        user.setEmail("test@example.com");
        user.setContact("1 ABC Street");
        assertEquals(true, instance.addUser(user));
        CardManager cardManager = new CardManager();
        BorrowerCard card = new BorrowerCard();
        card.setUser(user);
        card.setIssuedDate(LocalDate.now());
        card.setExpiredDate(LocalDate.now().plusYears(4));
        card.setId(123456789);
        cardManager.addCard(card);
        String token = instance.generateActivateToken("test_user");
        assertNotEquals(null, token);
        user = instance.activateUser("invalid_token");
        assertEquals(null, user);

        System.out.println("* Check valid token input");
        user = instance.activateUser(token);
        assertNotEquals(null, user);
        assertEquals(User.Status.ACTIVE, instance.getUserByUsername("test_user").getStatus());
    }

    /**
     * Test of getUserByUsername method, of class UserManager.
     */
    @Test
    public void testGetUserByUsername() {
        System.out.println("getUserByUsername");

        System.out.println("* Check empty input");
        String username = "";
        UserManager instance = new UserManager();
        User expResult = null;
        User result = instance.getUserByUsername(username);
        assertEquals(expResult, result);

        System.out.println("* Check exist user result");
        User user = new Borrower();
        user.setUsername("test_user");
        user.setPassword("password");
        user.setStatus(User.Status.NOT_ACTIVATED);
        user.setGender("Male");
        user.setFullName("Test User");
        user.setEmail("test@example.com");
        user.setContact("1 ABC Street");
        assertEquals(true, instance.addUser(user));
        assertNotEquals(null, instance.getUserByUsername("test_user"));

        System.out.println("* Check non-exist user result");
        assertEquals(null, instance.getUserByUsername("test_user2"));

    }

    /**
     * Test of getAllUsers method, of class UserManager.
     */
    @Test
    public void testGetAllUsers() {
        System.out.println("getAllUsers");
        UserManager instance = new UserManager();
        System.out.println("* Check empty result");
        boolean expResult = true;
        List<User> result = instance.getAllUsers();
        assertEquals(expResult, result.isEmpty());

        System.out.println("* Check result after add");
        User user = new Borrower();
        user.setUsername("test_user");
        user.setPassword("password");
        user.setStatus(User.Status.NOT_ACTIVATED);
        user.setGender("Male");
        user.setFullName("Test User");
        user.setEmail("test@example.com");
        user.setContact("1 ABC Street");
        assertEquals(true, instance.addUser(user));
        assertEquals(1, instance.getAllUsers().size());

        System.out.println("* Check result after update");
        user.setFullName("Test User 2");
        assertEquals(true, instance.updateUser(user));
        assertEquals(1, instance.getAllUsers().size());
    }

    /**
     * Test of generateActivateToken method, of class UserManager.
     */
    @Test
    public void testGenerateActivateToken() {
        System.out.println("generateActivateToken");
        UserManager instance = new UserManager();

        System.out.println("* Check empty input");
        String token = instance.generateActivateToken("");
        assertEquals(null, token);

        System.out.println("* Check null input");
        token = instance.generateActivateToken(null);
        assertEquals(null, token);

        System.out.println("* Check non-exist user");
        token = instance.generateActivateToken("test_user");
        assertEquals(null, token);

        System.out.println("* Check exist user");
        User user = new Borrower();
        user.setUsername("test_user");
        user.setPassword("password");
        user.setStatus(User.Status.NOT_ACTIVATED);
        user.setGender("Male");
        user.setFullName("Test User");
        user.setEmail("test@example.com");
        user.setContact("1 ABC Street");
        assertEquals(true, instance.addUser(user));

        System.out.println("* Check user with no card");
        token = instance.generateActivateToken("test_user");
        assertEquals(null, token);

        System.out.println("* Check user with card");
        CardManager cardManager = new CardManager();
        BorrowerCard card = new BorrowerCard();
        card.setUser(user);
        card.setIssuedDate(LocalDate.now());
        card.setExpiredDate(LocalDate.now().plusYears(4));
        card.setId(123456789);
        cardManager.addCard(card);
        token = instance.generateActivateToken("test_user");
        assertNotEquals(null, token);
    }
}
