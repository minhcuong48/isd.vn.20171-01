/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

import javafx.application.Application;
import javafx.stage.Stage;
import library.controller.ConfirmBorrowBooksController;
import library.service.AuthService;

/**
 * This is main file to test business for CuongGM
 * @author GiapMinhCuong-20140539
 */
public class CuongGm extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        // init database
//        BorrowBooksHelper.getDefault().createTables();
//        BorrowBooksHelper.getDefault().createTableData();
        
        // register to borrow books
//        AuthService.getInstance().authenticate("gminhcuong1", "123456");
//        RegisterToBorrowBooksController registerController = new RegisterToBorrowBooksController(1);
//        registerController.start();
        
//        // confirm borrow books
        AuthService.getInstance().authenticate("gminhcuong3", "123456");
        ConfirmBorrowBooksController confirmController = new ConfirmBorrowBooksController();
        confirmController.start();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
   
}
