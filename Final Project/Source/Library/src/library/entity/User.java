/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.entity;

/**
 * The model represent an user
 *
 * @author Dang Xuan Bach
 */
abstract public class User {

    /**
     * The type of user
     */
    public enum Type {

        /**
         * User is a borrower
         */
        BORROWER,
        /**
         * User is a student
         */
        STUDENT,
        /**
         * User is a librarian
         */
        LIBRARIAN,
        /**
         * User is an administrator
         */
        ADMIN
    }

    /**
     * The status of the user
     */
    public enum Status {
        /**
         * The user is active, the account has been activated
         */
        ACTIVE,
        /**
         * The user has retired, the account is deactivated
         */
        RETIRED,
        /**
         * User has just registered and the account is not activated
         */
        NOT_ACTIVATED,
    }
    private String username;
    private String password;
    private String fullName;
    private String email;
    private String gender;
    private String contact;
    private Status status;

    /**
     * Get status of user
     *
     * @return the status of user
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Set status of user
     *
     * @param status the status of user
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * Get username of user
     *
     * @return the username of user
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set username of user
     *
     * @param username the username of user
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get (ecrypted) password of user
     *
     * @return the (ecrypted) password of user
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set (ecrypted) password of user
     *
     * @param password the (ecrypted) password of user
     */
    public void setPassword(String password) {
        // TODO(bachdx): Maybe encrypt this, meh
        this.password = password;
    }

    /**
     * Get full name of user
     *
     * @return the full name of user
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Set full name of user
     *
     * @param fullName the full name of user
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * Get email of user
     *
     * @return the email of user
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set email of user
     *
     * @param email the email of user
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get gender of user
     *
     * @return the gender of user
     */
    public String getGender() {
        return gender;
    }

    /**
     * Set gender of user
     *
     * @param gender the gender of user
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * Get contact of user
     *
     * @return the contact of user
     */
    public String getContact() {
        return contact;
    }

    /**
     * Set contact of user
     *
     * @param contact the contact of user
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * Get the type of user
     *
     * @return the type of user
     */
    abstract public Type getType();
}
