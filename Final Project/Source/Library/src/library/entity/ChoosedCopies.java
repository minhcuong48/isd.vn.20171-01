/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.entity;

import javafx.collections.ObservableList;
import library.service.ChoosedCopiesService;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class ChoosedCopies {
    
    /**
     * Get all choosed copiess
     * @return choosed copies
     */
    public static ObservableList<ChoosedCopies> all() {
        return ChoosedCopiesService.getDefault().all();
    }
    
    /**
     * Get all status-specific choosed copies
     * @param status
     * @return choosed copies
     */
    public static ObservableList<ChoosedCopies> all(Copies.Status status) {
        return ChoosedCopiesService.getDefault().all(status);
    }
    
    private String bookNumber;
    private int sequenceNumber;
    private String typeOfCopies;
    private double price;
    private String status;
    private String title;
    private String publisher;
    private String authors;
    private String isbn;

    public String getBookNumber() {
        return bookNumber;
    }

    public void setBookNumber(String bookNumber) {
        this.bookNumber = bookNumber;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getTypeOfCopies() {
        return typeOfCopies;
    }

    public void setTypeOfCopies(String typeOfCopies) {
        this.typeOfCopies = typeOfCopies;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
    
    public boolean isAvailable() {
        Copies.Status currentStatus = Copies.Status.valueOf(getStatus());
        return currentStatus == Copies.Status.AVAILABLE;
    }

}
