package library.entity;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import library.service.CopiesService;

public class Copies {
    
    /**
     * Find a copy by book number and sequence number
     * @param bookNumber book number
     * @param sequenceNumber sequence number
     * @return a copy
     */
    public static Copies find(String bookNumber, int sequenceNumber) {
        return CopiesService.getDefault().find(bookNumber, sequenceNumber);
    }
    
    /**
     * Get lists of wanted copies
     * @param wantedCopies
     * @return list of wanted copies
     */
    public static ObservableList<Copies> listOfWantedCopies(ObservableList<WantedCopies> wantedCopies) {
        ObservableList<Copies> list = FXCollections.observableArrayList();
        for(int i=0; i<wantedCopies.size(); i++) {
            WantedCopies wantedCopy = wantedCopies.get(i);
            Copies copy = Copies.find(wantedCopy.getBookNumber(), wantedCopy.getSequenceNumber());
            if (copy != null) {
                list.add(copy);
            }  
        }
        
        return list;
    }

    /**
     * Convert from wanted copies to copies
     * @param wantedCopies
     * @return 
     */
    public static ObservableList<Copies> listOfBorrowedCopies(ObservableList<BorrowedCopies> wantedCopies) {
        ObservableList<Copies> list = FXCollections.observableArrayList();
        for(int i=0; i<wantedCopies.size(); i++) {
            BorrowedCopies borrowedCopies = wantedCopies.get(i);
            Copies copy = Copies.find(borrowedCopies.getBookNumber(), borrowedCopies.getSequenceNumber());
            if (copy != null) {
                list.add(copy);
            }
        }

        return list;
    }
    
    /**
     * Update status for copies
     * @param copies list of copies
     * @param status status
     */
    public static void updateStatus(ObservableList<Copies> copies, Copies.Status status) {
        CopiesService.getDefault().updateStatus(copies, status);
    }

    public enum TypeOfCopies {
        REFERENCE,
        BORROWABLE
    }

    public enum Status {
        AVAILABLE,
        REFERENCED,
        BORROWED,   // not lend physically to borrowers
        LENT        // lend physically to borrowers
    }
    private String bookNumber;
    private int sequenceNumber;
    private TypeOfCopies typeOfCopies;
    private double price;
    private Status status;

    @Override
    public String toString() {
        return bookNumber + "-" + sequenceNumber + "-";
    }

    public String getBookNumber() {
        return bookNumber;
    }

    public void setBookNumber(String bookNumber) {
        this.bookNumber = bookNumber;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public TypeOfCopies getTypeOfCopies() {
        return typeOfCopies;
    }

    public void setTypeOfCopies(TypeOfCopies typeOfCopies) {
        this.typeOfCopies = typeOfCopies;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    
    public void updateStatus(Status status) {
        CopiesService.getDefault().updateStatus(this, status);
    }
}
