/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.entity;

import java.time.LocalDate;

/**
 * The model represet a student
 *
 * @author Dang Xuan Bach
 */
public class Student extends User {

    private String studentId;
    private LocalDate studyPeriodStart;
    private LocalDate studyPeriodEnd;

    /**
     * Get student id of student
     *
     * @return the student id of student
     */
    public String getStudentId() {
        return studentId;
    }

    /**
     * Set student id of student
     *
     * @param studentId the student if of student
     */
    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    /**
     * Get study period start time
     *
     * @return the study period start time
     */
    public LocalDate getStudyPeriodStart() {
        return studyPeriodStart;
    }

    /**
     * Set study period start time
     *
     * @param studyPeriodStart the study period start time
     */
    public void setStudyPeriodStart(LocalDate studyPeriodStart) {
        this.studyPeriodStart = studyPeriodStart;
    }

    /**
     * Get study period end time
     *
     * @return the study period end time
     */
    public LocalDate getStudyPeriodEnd() {
        return studyPeriodEnd;
    }

    /**
     * Set study period end time
     *
     * @param studyPeriodEnd the study period end time
     */
    public void setStudyPeriodEnd(LocalDate studyPeriodEnd) {
        this.studyPeriodEnd = studyPeriodEnd;
    }

    /**
     * Get the type of student
     *
     * @return STUDENT
     */
    @Override
    public Type getType() {
        return Type.STUDENT;
    }

}
