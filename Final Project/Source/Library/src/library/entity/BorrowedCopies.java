/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.entity;

import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import library.service.BorrowedCopiesService;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class BorrowedCopies {
    
    public static int countBorrowedCopies(int cardId) {
        return BorrowedCopiesService.getDefault().countBorrowedCopies(cardId);
    }
    
    public static ObservableList<BorrowedCopies> listOf(ObservableList<WantedCopies> wantedItems) {
        ObservableList<BorrowedCopies> borrowedItems = FXCollections.observableArrayList();
        for (int i=0; i<wantedItems.size(); i++) {
            BorrowedCopies borrowedCopy = new BorrowedCopies();
            
            WantedCopies wantedCopy = wantedItems.get(i);
            borrowedCopy.setCardId(wantedCopy.getCardId());
            borrowedCopy.setBookNumber(wantedCopy.getBookNumber());
            borrowedCopy.setSequenceNumber(wantedCopy.getSequenceNumber());
            borrowedCopy.setLentDate(LocalDate.now());
            borrowedCopy.setExpectedReturnDate(LocalDate.now().plusWeeks(2));
            
            borrowedItems.add(borrowedCopy);
        }
        return borrowedItems;
    }
    
    public static void add(ObservableList<BorrowedCopies> items) {
        BorrowedCopiesService.getDefault().add(items);
    }

    public static ObservableList<BorrowedCopies> all(int cardId) {
        return BorrowedCopiesService.getDefault().all(cardId);
    }

    public static void delete(ObservableList<BorrowedCopies> items) {
        BorrowedCopiesService.getDefault().delete(items);
    }
    
    int cardId;
    String bookNumber;
    int sequenceNumber;
    LocalDate lentDate;
    LocalDate expectedReturnDate;

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getBookNumber() {
        return bookNumber;
    }

    public void setBookNumber(String bookNumber) {
        this.bookNumber = bookNumber;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public LocalDate getLentDate() {
        return lentDate;
    }

    public void setLentDate(LocalDate lentDate) {
        this.lentDate = lentDate;
    }

    public LocalDate getExpectedReturnDate() {
        return expectedReturnDate;
    }

    public void setExpectedReturnDate(LocalDate expectedReturnDate) {
        this.expectedReturnDate = expectedReturnDate;
    }
    
}
