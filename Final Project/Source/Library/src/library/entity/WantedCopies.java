/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.entity;

import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import library.service.WantedCopiesService;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class WantedCopies {

    /**
     * Convert from choosed copies to wanted copies
     * @param choosedCopies choosed copies
     * @param card Borrower card
     * @return list of wanted copies
     */
    public static ObservableList<WantedCopies> listOf(ObservableList<ChoosedCopies> choosedCopies, BorrowerCard card) {
        ObservableList<WantedCopies> list = FXCollections.observableArrayList();

        for (int i = 0; i < choosedCopies.size(); i++) {
            ChoosedCopies choosedItem = choosedCopies.get(i);
            WantedCopies wantedCopy = new WantedCopies();
            wantedCopy.setBookNumber(choosedItem.getBookNumber());
            wantedCopy.setSequenceNumber(choosedItem.getSequenceNumber());
            wantedCopy.setCardId(card.getId());
            wantedCopy.setUsername(card.getUser().getUsername());
            wantedCopy.setBorrowedDate(LocalDate.now());
            list.add(wantedCopy);
        }

        return list;
    }

    public static boolean add(ObservableList<WantedCopies> wantedCopies) {
        return WantedCopiesService.getDefault().add(wantedCopies);
    }

    public static int countBorrowedCopiesOfUser(int cardId) {
        return WantedCopiesService.getDefault().countBorrowedCopiesOfUser(cardId);
    }

    public static ObservableList<WantedCopies> all(int cardId) {
        return WantedCopiesService.getDefault().all(cardId);
    }

    public static void delete(ObservableList<WantedCopies> items) {
        WantedCopiesService.getDefault().delete(items);
    }

    private int cardId;
    private String username;
    private String bookNumber;
    private int sequenceNumber;
    LocalDate borrowedDate;

    private String title;
    private Double price;
    private String publisher;
    private String authors;
    private String isbn;

    public String getBookNumber() {
        return bookNumber;
    }

    public void setBookNumber(String bookNumber) {
        this.bookNumber = bookNumber;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public LocalDate getBorrowedDate() {
        return borrowedDate;
    }

    public void setBorrowedDate(LocalDate borrowedDate) {
        this.borrowedDate = borrowedDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

}
