/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.entity;

import java.time.LocalDate;
import java.util.List;
import library.service.CardManager;
import library.service.UserManager;

/**
 *
 * @author Gian Minh Cuong
 */
public class BorrowerCard {

    public static BorrowerCard find(int cardId) {
        CardManager cardManager = new CardManager();
        return cardManager.getCardById(cardId);
    }
    
    public static List<BorrowerCard> find(String username) {
        CardManager cardManager = new CardManager();
        UserManager userManager = new UserManager();
        return cardManager.getCardByUser(userManager.getUserByUsername(username));
    }
    
    private int id;
    private LocalDate issuedDate;
    private LocalDate expiredDate;
    private String username;
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public LocalDate getIssuedDate() {
        return issuedDate;
    }
    
    public void setIssuedDate(LocalDate issuedDate) {
        this.issuedDate = issuedDate;
    }
    
    public LocalDate getExpiredDate() {
        return expiredDate;
    }
    
    public void setExpiredDate(LocalDate expiredDate) {
        this.expiredDate = expiredDate;
    }
    
    public User getUser() {
        UserManager um = new UserManager();
        return um.getUserByUsername(username);
    }
    
    public void setUser(User user) {
        this.username = user.getUsername();
    }
    
}
