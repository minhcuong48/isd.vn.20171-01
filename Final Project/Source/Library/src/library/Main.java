package library;

import javafx.application.Application;
import javafx.stage.Stage;
import library.controller.*;
import library.helper.DatabaseHelper;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {

//        DatabaseHelper.clearDatabase();
        DatabaseHelper.getConnection();
        DatabaseHelper.createTablesIfNotExists();

//        AddNewBookController controller = new AddNewBookController(primaryStage);
//        controller.show();
//        AddExistBookController controller = new AddExistBookController(primaryStage);
//        controller.show();
//        DeleteBookController controller = new DeleteBookController(primaryStage);
//        controller.show();
//        SearchBookController controller = new SearchBookController(primaryStage);
//        controller.show();
//        ViewBookController controller = new ViewBookController(primaryStage);
//        controller.show();
        UpdateBookController controller = new UpdateBookController(primaryStage);
        controller.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
