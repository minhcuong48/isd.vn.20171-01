/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.controller;

import javafx.collections.ObservableList;
import library.entity.BorrowedCopies;
import library.entity.BorrowerCard;
import library.entity.Copies;
import library.entity.User;
import library.helper.Constant;
import library.service.AuthService;
import library.view.ReturnBookView;

public class ReturnBookController extends BaseController {

    private BorrowerCard borrowerCard = null;

    @Override
    public void start() {
        // check is auth
        if (!isAuth()) {
            showErrorMessage(Constant.MESSAFE_ERROR_MUST_AUTH_FIRST);
            return;
        }

        // check permission
        User.Type userType = AuthService.getInstance().getIdentity().getType();
        if (userType != User.Type.ADMIN && userType != User.Type.LIBRARIAN) {
            showErrorMessage(Constant.MESSAGE_ERROR_NOT_PERMISSION);
            return;
        }

        view = new ReturnBookView(this);
        view.show();
    }

    public boolean setBorrowerCard(int cardId) {
        borrowerCard = BorrowerCard.find(cardId);
        if(borrowerCard == null) {
            return false;
        }
        return true;
    }

    public void search(int cardId) {
        setBorrowerCard(cardId);
        if(borrowerCard == null) {
            showErrorMessage(Constant.MESSAGE_ERROR_CARD_NOT_FOUND);
            return;
        }
        getView().setUserInfo(borrowerCard);
        getView().setBorrowedCopies(BorrowedCopies.all(borrowerCard.getId()));
    }

    public void returnBorrowedBooks(ObservableList<BorrowedCopies> items) {
        ObservableList<Copies> copies = Copies.listOfBorrowedCopies(items);
        Copies.updateStatus(copies, Copies.Status.AVAILABLE);

        BorrowedCopies.delete(items);
    }

    public BorrowerCard getBorrowerCard() {
        return borrowerCard;
    }

    private ReturnBookView getView() {
        return (ReturnBookView) view;
    }

}
