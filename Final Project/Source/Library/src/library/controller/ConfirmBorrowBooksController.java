/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.controller;

import javafx.collections.ObservableList;
import library.entity.BorrowedCopies;
import library.entity.BorrowerCard;
import library.entity.Copies;
import library.entity.User;
import library.entity.WantedCopies;
import library.helper.Constant;
import library.service.AuthService;
import library.view.ConfirmBorrowBooksView;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class ConfirmBorrowBooksController extends BaseController {

    private BorrowerCard borrowerCard = null;

    @Override
    public void start() {
        // check is auth
        if (!isAuth()) {
            showErrorMessage(Constant.MESSAFE_ERROR_MUST_AUTH_FIRST);
            return;
        }

        // check permission
        User.Type userType = AuthService.getInstance().getIdentity().getType();
        if (userType != User.Type.ADMIN && userType != User.Type.LIBRARIAN) {
            showErrorMessage(Constant.MESSAGE_ERROR_NOT_PERMISSION);
            return;
        }

        view = new ConfirmBorrowBooksView(this);
        view.show();
    }

    /**
     * Get wanted copies of a card
     * @return list wanted copies belongs to one card
     */
    public ObservableList<WantedCopies> getWantedCopies() {
        return WantedCopies.all(borrowerCard.getId());
    }

    /**
     * Set card id for this controller
     * @param cardId Borrower Card ID
     * @return success or fail
     */
    public boolean setBorrowerCard(int cardId) {
        borrowerCard = BorrowerCard.find(cardId);
        if(borrowerCard == null) {
            return false;
        }
        return true;
    }

    /**
     * Search list of wanted copies belongs to a card
     * @param cardId Borrower card ID
     */
    public void search(int cardId) {
        setBorrowerCard(cardId);
        if(borrowerCard == null) {
            showErrorMessage(Constant.MESSAGE_ERROR_CARD_NOT_FOUND);
            return;
        }
        getView().setUserInfo(borrowerCard);
        getView().setBorrowedCopies(WantedCopies.all(borrowerCard.getId()));
    }

    /**
     * Confirm to borrow copies
     * @param items list registed copies
     */
    public void confirmBorrowBooks(ObservableList<WantedCopies> items) {
        ObservableList<BorrowedCopies> borrowedItems = BorrowedCopies.listOf(items);
        BorrowedCopies.add(borrowedItems);

        ObservableList<Copies> copies = Copies.listOfWantedCopies(items);
        Copies.updateStatus(copies, Copies.Status.LENT);

        WantedCopies.delete(items);
    }

    /**
     * Get borrower card of this controller
     * @return Borrower card
     */
    public BorrowerCard getBorrowerCard() {
        return borrowerCard;
    }

    /**
     * Get view that is controlled by this controller
     * @return 
     */
    private ConfirmBorrowBooksView getView() {
        return (ConfirmBorrowBooksView) view;
    }

}
