/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javafx.stage.Stage;
import library.entity.BorrowerCard;
import library.entity.Student;
import library.entity.User;
import library.service.CardManager;
import library.service.UserManager;
import library.view.IssueCardView;
import library.view.IssueCardView.CardInfo;

/**
 *
 * @author Dang Xuan Bach
 */
public class IssueCardController {

    private final IssueCardView view;

    public IssueCardController(Stage parent, User user) {
        view = new IssueCardView(this, parent, user.getUsername());
    }

    public void show() {
        view.show();
    }

    public void issueCard(String username, String cardId, LocalDate expiredDate) throws IllegalArgumentException {
        if (cardId.isEmpty()) {
            throw new IllegalArgumentException("Card ID must not be empty");
        }
        UserManager userManager = new UserManager();
        User user = userManager.getUserByUsername(username);
        CardManager cardManager = new CardManager();
        if (cardManager.getCardById(Integer.parseUnsignedInt(cardId)) != null) {
            throw new IllegalArgumentException("Card already has a user");
        }
        if (user.getType() == User.Type.STUDENT) {
            Student student = (Student) user;
            if (!cardManager.getActiveCards(student, LocalDate.now(), expiredDate).isEmpty()) {
                throw new IllegalArgumentException("Student can only have one card at a time");
            }
            if (student.getStudyPeriodEnd().isBefore(expiredDate)) {
                throw new IllegalArgumentException("The expiration date is after period end");
            }
        }
        BorrowerCard card = new BorrowerCard();
        card.setId(Integer.parseUnsignedInt(cardId));
        card.setIssuedDate(LocalDate.now());
        card.setExpiredDate(expiredDate);
        card.setUser(user);
        cardManager.addCard(card);
    }

    public List<CardInfo> getAllCards(String username) {
        UserManager userManager = new UserManager();
        User user = userManager.getUserByUsername(username);
        CardManager cardManager = new CardManager();
        List<CardInfo> cardInfos = new ArrayList<CardInfo>();
        List<BorrowerCard> card = cardManager.getCardByUser(user);
        for (BorrowerCard c : card) {
            cardInfos.add(new CardInfo(c.getId(), c.getIssuedDate(), c.getExpiredDate()));
        }
        return cardInfos;
    }
}
