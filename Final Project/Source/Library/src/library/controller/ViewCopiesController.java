package library.controller;

import java.util.List;
import javafx.stage.Stage;
import library.entity.Copies;
import library.service.CopiesManager;
import library.view.ViewCopiesView;

public class ViewCopiesController {

    private final ViewCopiesView view;

    public ViewCopiesController(Stage parent, String bookNumber) {
        view = new ViewCopiesView(this, parent, bookNumber);
    }

    public void show() {
        view.show();
    }

    public List<Copies> getCopiesList(String bookNumber) {
        CopiesManager manager = new CopiesManager();
        return manager.getCopiesList(bookNumber);
    }
}
