/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.controller;

import javafx.stage.Stage;
import library.service.UserManager;
import library.view.ActivateAccountView;

/**
 *
 * @author Dang Xuan Bach
 */
public class ActivateAccountController {

    private final ActivateAccountView view;

    public ActivateAccountController(Stage parent) {
        view = new ActivateAccountView(this, parent);
    }

    public void show() {
        view.show();
    }

    public void activate(String activationCode) {
        if (activationCode == null || activationCode.isEmpty()) {
            throw new IllegalArgumentException("Activation code is empty");
        }
        UserManager manager = new UserManager();
        if (manager.activateUser(activationCode) == null) {
            throw new IllegalArgumentException("Invalid activation code");
        }
    }
}
