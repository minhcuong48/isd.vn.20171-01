/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.controller;

import javafx.stage.Stage;
import library.entity.User;
import library.service.AuthService;
import library.view.LoginView;

/**
 *
 * @author Dang Xuan Bach
 */
public class LoginController {

    private final LoginView view;

    public LoginController(Stage parent) {
        view = new LoginView(this, parent);
    }

    public void show() {
        view.show();
    }

    public void login(String username, String password) {
        AuthService auth = AuthService.getInstance();
        User user = auth.authenticate(username, password);
        if (user == null) {
            throw new IllegalArgumentException("Username and password does not match any account");
        }
    }

    public void showSignup() {
        SignupController controller = new SignupController(view.getStage());
        controller.show();
    }

    public void showActivate() {
        ActivateAccountController controller = new ActivateAccountController(view.getStage());
        controller.show();
    }

    public void showMenu() {
        if (AuthService.getInstance().getIdentity() != null) {
            view.getStage().close();
            MenuController controller = new MenuController(null, AuthService.getInstance().getIdentity().getType().name());
            controller.show();
        }
    }
}
