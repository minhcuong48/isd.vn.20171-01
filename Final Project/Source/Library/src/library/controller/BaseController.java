/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.controller;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import library.service.AuthService;
import library.view.BaseView;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public abstract class BaseController {
    
    protected BaseView view = null;
    
    /**
     * Start controller
     */
    public abstract void start();
    
//    public void showMessageDialog(String title, String message) {
//        MessageDialogView dialog = new MessageDialogView(this);
//        dialog.setInfo(title, message);
//        dialog.setParentView(view);
//        dialog.show();
//    }
    
    /**
     * Show error dialog
     * @param message content text
     */
    public void showErrorMessage(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR, message, ButtonType.OK);
        alert.show();
    }
    
    /**
     * Show success message
     * @param message content text
     */
    public void showSuccessMessage(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, message, ButtonType.OK);
        alert.show();
    }
    
    /**
     * Check if user has authenticated
     * @return authentication status
     */
    protected boolean isAuth() {
        return AuthService.getInstance().getIdentity() != null;
    }
}
