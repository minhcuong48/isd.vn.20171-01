package library.controller;

import javafx.stage.Stage;
import library.view.AddNewBookView;

public class AddNewBookController {

    private final AddNewBookView view;

    public AddNewBookController(Stage parent) {
        view = new AddNewBookView(this, parent);
    }

    public void show() {
        view.show();
    }

    public boolean checkValidInput(String title, String publisher, String authors, String isbn) {
        if (title.isEmpty() || publisher.isEmpty() || authors.isEmpty() || isbn.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public void showNewCopiesView(String title, String publisher, String authors, String isbn) {
        AddNewCopiesController controller = new AddNewCopiesController(view.getStage(), title, publisher, authors, isbn);
        controller.show();
    }
}
