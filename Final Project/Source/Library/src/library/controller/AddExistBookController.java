package library.controller;

import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import library.entity.Copies;
import library.service.AddExistBookManager;
import library.service.BookManager;
import library.view.AddExistBookView;

public class AddExistBookController {

    private final AddExistBookView view;

    public AddExistBookController(Stage parent) {
        view = new AddExistBookView(this, parent);
    }

    public void show() {
        view.show();
    }

    public boolean checkValidInput(String bookNumber, String numberOfCopies, ToggleGroup toggleGroup, String price) {
        if (bookNumber.isEmpty() || numberOfCopies.isEmpty() ||toggleGroup.getSelectedToggle() == null || price.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public boolean checkNumberValid(String numberOfCopies) {
        try {
            int value = Integer.parseInt(numberOfCopies);
            if (value <= 0) return false;
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public boolean checkPriceValid(String price) {
        try {
            double value = Double.parseDouble(price);
            if (value <= 0) return false;
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public boolean addExistBook(String bookNumber, int numberOfCopies, Copies.TypeOfCopies type, double price) {
        AddExistBookManager manager = new AddExistBookManager();
        return manager.addExistBook(bookNumber, numberOfCopies, type, price);
    }

    /**
     * kiểm tra sách đã tồn tại chưa
     * @param bookNumber
     * @return
     */
    public boolean checkBookExist(String bookNumber) {
        BookManager bookManager = new BookManager();
        return bookManager.checkBookExist(bookNumber);
    }
}
