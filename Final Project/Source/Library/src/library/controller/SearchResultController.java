package library.controller;

import java.util.List;
import javafx.stage.Stage;
import library.entity.Book;
import library.service.BookManager;
import library.view.SearchResultView;

public class SearchResultController {
    SearchResultView view;

    public SearchResultController(Stage parent, String bookNumber, String title, String publisher, String authors, String isbn) {
        view = new SearchResultView(this, parent, bookNumber, title, publisher, authors, isbn);
    }

    public void show() {
        view.show();
    }

    public List<Book> getBookList(String bookNumber, String title, String publisher, String authors, String isbn) {
        BookManager manager = new BookManager();
        return manager.getBookList(bookNumber, title, publisher, authors, isbn);
    }
}
