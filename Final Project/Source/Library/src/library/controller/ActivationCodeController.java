/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.controller;

import javafx.stage.Stage;
import library.entity.User;
import library.service.UserManager;
import library.view.ActivationCodeView;

/**
 *
 * @author Dang Xuan Bach
 */
public class ActivationCodeController {

    private final ActivationCodeView view;
    private final User user;

    public ActivationCodeController(Stage parent, User user) {
        this.user = user;
        UserManager userManager = new UserManager();
        String code = userManager.generateActivateToken(user.getUsername());
        view = new ActivationCodeView(this, parent, code);
    }

    public void show() {
        view.show();
    }
}
