/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.controller;

import javafx.stage.Stage;
import library.entity.Borrower;
import library.entity.Librarian;
import library.entity.Student;
import library.service.UserManager;
import library.view.SignupView;
import library.view.SignupView.SignupInfo;

/**
 *
 * @author Dang Xuan Bach
 */
public class SignupController {

    SignupView view;

    public SignupController(Stage parent) {
        view = new SignupView(this, parent);
    }

    public void show() {
        view.show();
    }

    public void signup(String type, SignupInfo info) {
        UserManager manager = new UserManager();
        if (type.equals("Student")) {
            Student user = new Student();
            user.setUsername(info.username);
            user.setPassword(info.password);
            user.setFullName(info.fullName);
            user.setEmail(info.email);
            user.setGender(info.gender);
            user.setContact(info.contact);
            user.setStudentId(info.userId);
            user.setStudyPeriodStart(info.periodStart);
            user.setStudyPeriodEnd(info.periodEnd);
            if (!manager.addUser(user)) {
                throw new IllegalArgumentException("Invalid account info");
            }
        } else if (type.equals("Borrower")) {
            Borrower user = new Borrower();
            user.setUsername(info.username);
            user.setPassword(info.password);
            user.setFullName(info.fullName);
            user.setEmail(info.email);
            user.setGender(info.gender);
            user.setContact(info.contact);
            if (!manager.addUser(user)) {
                throw new IllegalArgumentException("Invalid account info");
            }
        } else if (type.equals("Librarian")) {
            Librarian user = new Librarian();
            user.setUsername(info.username);
            user.setPassword(info.password);
            user.setFullName(info.fullName);
            user.setEmail(info.email);
            user.setGender(info.gender);
            user.setContact(info.contact);
            if (!manager.addUser(user)) {
                throw new IllegalArgumentException("Invalid account info");
            }
        } else {
            throw new IllegalArgumentException("Invalid account type");
        }
    }
}
