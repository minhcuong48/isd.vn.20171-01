package library.controller;

import javafx.stage.Stage;
import library.view.SearchBookView;

public class SearchBookController {
    private final SearchBookView view;

    public SearchBookController(Stage parent) {
        view = new SearchBookView(this, parent);
    }

    public void show() {
        view.show();
    }

    public void showBookResultView(String bookNumber, String title, String publisher, String authors, String isbn) {
        SearchResultController controller = new SearchResultController(view.getStage(), bookNumber, title, publisher, authors, isbn);
        controller.show();
    }
}
