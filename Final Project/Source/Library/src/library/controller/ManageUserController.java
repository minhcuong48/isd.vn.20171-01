 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javafx.stage.Stage;
import library.entity.User;
import library.service.CardManager;
import library.service.UserManager;
import library.view.ManageUserView;
import library.view.ManageUserView.Result;

/**
 *
 * @author Dang Xuan Bach
 */
public class ManageUserController {

    private final ManageUserView view;

    public ManageUserController(Stage parent) {
        view = new ManageUserView(this, parent);
    }

    public List<Result> getAllUsers() {
        List<Result> results = new ArrayList<>();
        for (User user : new UserManager().getAllUsers()) {
            results.add(new Result(user.getUsername(), user.getEmail(), user.getFullName(), user.getGender(), user.getContact(), user.getType().toString(), user.getStatus().toString()));
        }
        return results;
    }

    public void show() {
        view.show();
    }

    public void activateUser(String username) {
        UserManager userManager = new UserManager();
        User user = userManager.getUserByUsername(username);
        CardManager cardManager = new CardManager();
        if (cardManager.getActiveCards(user, LocalDate.now()).isEmpty()) {
            IssueCardController issueCardController = new IssueCardController(view.getStage(), user);
            issueCardController.show();
        }
        if (!cardManager.getActiveCards(user, LocalDate.now()).isEmpty()) {
            ActivationCodeController activationCodeController = new ActivationCodeController(view.getStage(), user);
            activationCodeController.show();
        }

    }

    public void issueCard(String username) {
        UserManager userManager = new UserManager();
        User user = userManager.getUserByUsername(username);
        IssueCardController issueCardController = new IssueCardController(view.getStage(), user);
        issueCardController.show();
    }
}
