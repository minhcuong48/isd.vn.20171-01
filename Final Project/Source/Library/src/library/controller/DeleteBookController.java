package library.controller;

import javafx.stage.Stage;
import library.service.BookManager;
import library.service.CopiesManager;
import library.service.DeleteBookManager;
import library.service.DeleteCopiesManager;
import library.view.DeleteBookView;

public class DeleteBookController {
    private final DeleteBookView view;

    public DeleteBookController(Stage parent) {
        view = new DeleteBookView(this, parent);
    }

    public void show() {
        view.show();
    }

    public boolean checkValidInput(String bookNumber) {
        if (bookNumber.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public boolean checkSequenceValid(String sequenceNumber) {
        try {
            int value = Integer.parseInt(sequenceNumber);
            if (value <= 0) return false;
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public boolean checkBookExist(String bookNumber) {
        BookManager manager = new BookManager();
        return manager.checkBookExist(bookNumber);
    }

    public boolean deleteBook(String bookNumber) {
        DeleteBookManager manager = new DeleteBookManager();
        return manager.deleteBook(bookNumber);
    }

    public boolean deleteCopies(String bookNumber, int sequenceNumber) {
        DeleteCopiesManager manager = new DeleteCopiesManager();
        return manager.deleteCopies(bookNumber, sequenceNumber);
    }

    public boolean checkCopiesExist(String bookNumber, int sequenceNumber) {
        CopiesManager manager = new CopiesManager();
        return manager.checkCopiesExist(bookNumber, sequenceNumber);
    }
}
