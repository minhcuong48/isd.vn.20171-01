/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.controller;

import java.time.LocalDate;
import javafx.collections.ObservableList;
import library.entity.BorrowedCopies;
import library.entity.BorrowerCard;
import library.entity.ChoosedCopies;
import library.entity.Copies;
import library.entity.User;
import library.entity.WantedCopies;
import library.helper.Constant;
import library.service.AuthService;
import library.view.RegisterToBorrowBooksView;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class RegisterToBorrowBooksController extends BaseController {
    
    private final int cardId;
    BorrowerCard borrowerCard = null;

    /**
     * Constructor
     *
     * @param cardId BorrowerCard id for current user
     */
    public RegisterToBorrowBooksController(int cardId) {
        this.cardId = cardId;
    }

    /**
     * Start Registering To Borrow Module
     */
    @Override
    public void start() {
        // check is auth
        if (!isAuth()) {
            showErrorMessage(Constant.MESSAFE_ERROR_MUST_AUTH_FIRST);
            return;
        }

        // check permission
        User.Type userType = AuthService.getInstance().getIdentity().getType();
        if (userType != User.Type.BORROWER && userType != User.Type.STUDENT) {
            showErrorMessage(Constant.MESSAGE_ERROR_NOT_PERMISSION);
            return;
        }
        
        loadBorrowerCardInfo();

        // check if card is expired
        if (borrowerCard.getExpiredDate().isBefore(LocalDate.now())) {
            showErrorMessage(Constant.MESSAGE_ERROR_CARD_EXPIRED);
            return;
        }
        
        view = new RegisterToBorrowBooksView(this);
        view.setTitle("Register To Borrow Books");
        view.show();
    }

    // public methods
    
    /**
     * Get list of copies in system
     * @return list of copies
     */
    public ObservableList<ChoosedCopies> getChoosedCopies() {
        return ChoosedCopies.all();
    }
    
    /**
     * Get list of available copies in system
     * @return list of available copies
     */
    public ObservableList<ChoosedCopies> getAvailableChoosedCopies() {
        return ChoosedCopies.all(Copies.Status.AVAILABLE);
    }
    
    /**
     * Check if number of borrowed copies is valid
     * @param wantedItems list of wanted copies
     * @return valid or invalid
     */
    public boolean isNumBorrowedCopiesValid(ObservableList<ChoosedCopies> wantedItems) {
        return wantedItems.size()
                + WantedCopies.countBorrowedCopiesOfUser(cardId)
                + BorrowedCopies.countBorrowedCopies(cardId)
                <= Constant.MAX_NUM_WANTED_COPIES;
    }
    
    /**
     * Register to borrow copies
     * @param wantedItems list of registing copies
     */
    public void registerToBorrowBooks(ObservableList<ChoosedCopies> wantedItems) {
        if (wantedItems.isEmpty()) {
            showErrorMessage(Constant.MESSAGE_ERROR_EMPTY_CHOOSED_COPIES);
            return;
        }

        // if count > 5
        if (!isNumBorrowedCopiesValid(wantedItems)) {
            showErrorMessage(Constant.MESSAGE_ERROR_NUM_COPIES_INVALID);
            return;
        }
        
        if (!isCopiesAvailable(wantedItems)) {
            showErrorMessage(Constant.MESSAGE_ERROR_COPIES_NOT_AVAILABLE);
            return;
        }
        
        ObservableList<WantedCopies> items = WantedCopies.listOf(wantedItems, borrowerCard);
        if (WantedCopies.add(items)) {
            ObservableList<Copies> copiesItems = Copies.listOfWantedCopies(items);
            Copies.updateStatus(copiesItems, Copies.Status.BORROWED);
            showSuccessMessage(Constant.MESSAGE_SUCCESS_REGISTER_TO_BORROW_BOOKS);
        } else {
            showErrorMessage(Constant.MESSAGE_ERROR_DUPLICATE_WANTED_COPIES);
        }
    }
    
    /**
     * Load Borrower Card's information
     */
    public void loadBorrowerCardInfo() {
        borrowerCard = BorrowerCard.find(cardId);
    }

    // private methods
    
    /**
     * Check if wanted copies if available
     * @param wantedItems wanted copies
     * @return available or not available
     */
    private boolean isCopiesAvailable(ObservableList<ChoosedCopies> wantedItems) {
        for (int i = 0; i < wantedItems.size(); i++) {
            if (!wantedItems.get(i).isAvailable()) {
                return false;
            }
        }
        return true;
    }
}
