package library.controller;

import javafx.stage.Stage;
import library.service.UpdateBookManager;
import library.view.UpdateBookInfoView;

public class UpdateBookInfoController {

    private final UpdateBookInfoView view;

    public UpdateBookInfoController(Stage parent, String bookNumber, String title, String publisher, String authors, String isbn) {
        view = new UpdateBookInfoView(this, parent, bookNumber, title, publisher, authors, isbn);
    }

    public void show() {
        view.show();
    }

    public boolean checkValidInput(String title, String publisher, String authors, String isbn) {
        if (title.isEmpty() || publisher.isEmpty() || authors.isEmpty() || isbn.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public boolean updateBook(String bookNumber, String title, String publisher, String authors, String isbn) {
        UpdateBookManager manager = new UpdateBookManager();
        return manager.updateBook(bookNumber, title, publisher, authors, isbn);
    }
}
