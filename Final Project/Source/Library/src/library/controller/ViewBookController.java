package library.controller;

import javafx.stage.Stage;
import library.entity.Book;
import library.service.BookManager;
import library.view.ViewBookView;

public class ViewBookController {

    private final ViewBookView view;

    public ViewBookController(Stage parent) {
        view = new ViewBookView(this, parent);
    }

    public void show() {
        view.show();
    }

    public boolean checkValidInput(String bookNumber) {
        if (bookNumber.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public Book viewBook(String bookNumber) {
        BookManager manager = new BookManager();
        return manager.viewBook(bookNumber);
    }

    /**
     * kiểm tra sách đã tồn tại chưa
     * @param bookNumber
     * @return
     */
    public boolean checkBookExist(String bookNumber) {
        BookManager bookManager = new BookManager();
        return bookManager.checkBookExist(bookNumber);
    }

    public void showCopiesListView(String bookNumber) {
        ViewCopiesController controller = new ViewCopiesController(view.getStage(), bookNumber);
        controller.show();
    }
}
