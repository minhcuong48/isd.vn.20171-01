package library.controller;

import javafx.stage.Stage;
import library.entity.Book;
import library.service.BookManager;
import library.view.UpdateBookView;

public class UpdateBookController {

    private final UpdateBookView view;

    public UpdateBookController(Stage parent) {
        view = new UpdateBookView(this, parent);
    }

    public void show() {
        view.show();
    }

    public boolean checkValidInput(String bookNumber) {
        if (bookNumber.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public Book viewBook(String bookNumber) {
        BookManager manager = new BookManager();
        return manager.viewBook(bookNumber);
    }

    /**
     * kiểm tra sách đã tồn tại chưa
     * @param bookNumber
     * @return
     */
    public boolean checkBookExist(String bookNumber) {
        BookManager bookManager = new BookManager();
        return bookManager.checkBookExist(bookNumber);
    }

    public void showUpdateBookInfoView(String bookNumber, String title, String publisher, String authors, String isbn) {
        UpdateBookInfoController controller = new UpdateBookInfoController(view.getStage(), bookNumber, title, publisher, authors, isbn);
        controller.show();
    }
}
