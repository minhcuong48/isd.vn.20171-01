/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.controller;

import java.util.ArrayList;
import java.util.List;
import javafx.stage.Stage;
import library.entity.BorrowerCard;
import library.entity.User;
import library.service.CardManager;
import library.service.UserManager;
import library.view.ChooseCardToRegisterBookView;
import library.view.ChooseCardToRegisterBookView.CardInfo;

/**
 *
 * @author Dang Xuan Bach
 */
public class ChooseCardToRegisterBookController {

    ChooseCardToRegisterBookView view;

    public ChooseCardToRegisterBookController(Stage parent, String username) {
        view = new ChooseCardToRegisterBookView(this, parent, username);
    }

    public void show() {
        view.show();
    }

    public void registerBookToBorrow(int cardId) {
        RegisterToBorrowBooksController controller = new RegisterToBorrowBooksController(cardId);
        controller.start();
    }

    public List<CardInfo> getAllCards(String username) {
        UserManager userManager = new UserManager();
        User user = userManager.getUserByUsername(username);
        CardManager cardManager = new CardManager();
        List<CardInfo> cardInfos = new ArrayList<CardInfo>();
        List<BorrowerCard> card = cardManager.getCardByUser(user);
        for (BorrowerCard c : card) {
            cardInfos.add(new CardInfo(c.getId(), c.getIssuedDate(), c.getExpiredDate()));
        }
        return cardInfos;
    }
}
