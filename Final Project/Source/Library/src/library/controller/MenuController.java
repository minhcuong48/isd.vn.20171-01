/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.controller;

import javafx.stage.Stage;
import library.service.AuthService;
import library.view.MenuView;

/**
 *
 * @author Dang Xuan Bach
 */
public class MenuController {

    private final MenuView view;

    public MenuController(Stage parent, String type) {
        view = new MenuView(this, parent, type);
    }

    public void show() {
        view.show();
    }

    public void manageUser() {
        ManageUserController controller = new ManageUserController(null);
        controller.show();
    }

    public void confirmBorrow() {
        ConfirmBorrowBooksController controller = new ConfirmBorrowBooksController();
        controller.start();
    }

    public void returnBook() {
        ReturnBookController controller = new ReturnBookController();
        controller.start();
    }

    public void addNewBook() {
        AddNewBookController controller = new AddNewBookController(null);
        controller.show();
    }

    public void addExistBook() {
        AddExistBookController controller = new AddExistBookController(null);
        controller.show();
    }

    public void viewBook() {
        ViewBookController controller = new ViewBookController(null);
        controller.show();
    }

    public void deleteBook() {
        DeleteBookController controller = new DeleteBookController(null);
        controller.show();
    }

    public void searchBook() {
        SearchBookController controller = new SearchBookController(null);
        controller.show();
    }

    public void registerBorrow() {
        ChooseCardToRegisterBookController controller = new ChooseCardToRegisterBookController(null, AuthService.getInstance().getIdentity().getUsername());
        controller.show();
    }

    public void logout() {
        AuthService.getInstance().clearIdentity();
        LoginController controller = new LoginController(null);
        controller.show();
    }
}
