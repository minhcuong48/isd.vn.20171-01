package library.controller;

import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import library.entity.Copies;
import library.service.AddNewBookManager;
import library.view.AddNewCopiesView;

public class AddNewCopiesController {

    AddNewCopiesView view;

    public AddNewCopiesController(Stage parent, String title, String publisher, String authors, String isbn) {
        view = new AddNewCopiesView(this, parent, title, publisher, authors, isbn);
    }

    public void show() {
        view.show();
    }

    public boolean checkValidInput(ToggleGroup toggleGroup, String price) {
        if (toggleGroup.getSelectedToggle() == null || price.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public boolean checkPriceValid(String price) {
        try {
            double value = Double.parseDouble(price);
            if (value <= 0) return false;
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public boolean addNewBook(String title, String publisher, String authors, String isbn, Copies.TypeOfCopies type, double price) {
        AddNewBookManager manager = new AddNewBookManager();
        return manager.addNewBook(title, publisher, authors, isbn, type, price);
    }
}
