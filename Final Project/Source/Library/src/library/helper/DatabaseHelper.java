/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.helper;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dang Xuan Bach
 */
public class DatabaseHelper {

    private static final String DATABASE_NAME = "jdbc:sqlite:library.db";

    static private Connection conn;

    public static Connection getConnection() {
        // Should not use singleton here, seriously, really, really bad practice
        if (conn == null) {
            try {
                conn = DriverManager.getConnection(DATABASE_NAME);
                conn.setAutoCommit(false);
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return conn;
    }

    public static void createTablesIfNotExists() {
        createUserTable();
        createStudentTable();
        createCardTable();
        createBookTable();
        createCopiesTable();
        createBorrowedCopiesTable();
        createWantedCopiesTable();

    }

    private static void createUserTable() {
        Connection conn = getConnection();
        try {
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS user("
                    + "username CHAR(128) PRIMARY KEY NOT NULL,"
                    + "password VARCHAR(128) NOT NULL,"
                    + "full_name VARCHAR(128) NOT NULL,"
                    + "email VARCHAR(254) NOT NULL,"
                    + "contact VARCHAR(256) NOT NULL,"
                    + "gender VARCHAR(20) NOT NULL,"
                    + "status CHAR(20) NOT NULL,"
                    + "activate_token CHAR(32),"
                    + "type CHAR(20) NOT NULL);");
            conn.commit();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void createStudentTable() {
        Connection conn = getConnection();
        try {
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS student("
                    + "username CHAR(128) PRIMARY KEY NOT NULL REFERENCES user,"
                    + "student_id CHAR(128) NOT NULL,"
                    + "period_start DATE NOT NULL,"
                    + "period_end DATE NOT NULL);");
            conn.commit();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void createCardTable() {
        Connection conn = getConnection();
        try {
            Statement stmt = conn.createStatement();
            conn.commit();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS borrower_card("
                    + "id INTEGER PRIMARY KEY NOT NULL,"
                    + "username CHAR(128) NOT NULL REFERENCES user,"
                    + "issued_date DATE NOT NULL,"
                    + "expired_date DATE NOT NULL);");
            conn.commit();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void createBookTable() {
        Connection conn = getConnection();
        try {
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS book("
                    + "book_number CHAR(6) PRIMARY KEY NOT NULL,"
                    + "title VARCHAR(256) NOT NULL,"
                    + "publisher VARCHAR(256) NOT NULL,"
                    + "authors VARCHAR(128) NOT NULL,"
                    + "isbn VARCHAR(32) NOT NULL);");
            conn.commit();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void createCopiesTable() {
        Connection conn = getConnection();
        try {
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS copies("
                    + "book_number CHAR(6) NOT NULL,"
                    + "sequence_number INTEGER,"
                    + "type_of_copies VARCHAR(16) NOT NULL,"
                    + "price DOUBLE,"
                    + "status VARCHAR(16) NOT NULL,"
                    + "PRIMARY KEY(book_number, sequence_number),"
                    + "FOREIGN KEY(book_number) REFERENCES book);");
            conn.commit();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void createWantedCopiesTable() {
        Connection conn = getConnection();
        try {
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS wanted_copies("
                    + "id INTEGER,"
                    + "book_number VARCHAR(6),"
                    + "sequence_number INTEGER,"
                    + "borrowed_date DATE,"
                    + "FOREIGN KEY (id) REFERENCES borrower_card(id)"
                    + "FOREIGN KEY (book_number, sequence_number) REFERENCES copies(book_number, sequence_number)"
                    + "PRIMARY KEY (id, book_number, sequence_number)"
                    + ");");
            conn.commit();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void createBorrowedCopiesTable() {
        Connection conn = getConnection();
        try {
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS borrowed_copies("
                    + "id INTEGER,"
                    + "book_number CHAR(6),"
                    + "sequence_number INTEGER,"
                    + "lent_date DATE,"
                    + "expected_return_date DATE,"
                    + "FOREIGN KEY (id) REFERENCES borrower_card(id)"
                    + "FOREIGN KEY (book_number, sequence_number) REFERENCES copies(book_number, sequence_number)"
                    + "PRIMARY KEY (id, book_number, sequence_number)"
                    + ");");
            conn.commit();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void clearDatabase() {
        try {
            getConnection().close();
            conn = null;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        String fileName = DATABASE_NAME.substring(DATABASE_NAME.lastIndexOf(':') + 1);
        File file = new File(fileName);
        file.delete();
    }
}
