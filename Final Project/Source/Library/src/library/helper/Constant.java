/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.helper;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class Constant {
    public static final String TITLE_ERROR = "Error";
    public static final String TITLE_SUCCESS = "Success";
    
    public static final String MESSAGE_SUCCESS_REGISTER_TO_BORROW_BOOKS = "Registering is successful!";
    
    public static final String MESSAGE_ERROR_CARD_EXPIRED = "Your card is expired!";
    public static final String MESSAGE_ERROR_EMPTY_CHOOSED_COPIES = "You have not choosen any copies!";
    public static final String MESSAGE_ERROR_NUM_COPIES_INVALID = "Number of copies can't be more than 5!";
    public static String MESSAGE_ERROR_COPIES_NOT_AVAILABLE = "Copies are not available!";
    public static String MESSAGE_ERROR_DUPLICATE_WANTED_COPIES = "Maybe you have registered one of these copies!";
    public static String MESSAGE_ERROR_EMPTY_SEARCH_FIELD = "Search field is empty!";
    public static String MESSAGE_ERROR_CARD_NOT_FOUND = "Borrower Card is not found!";
    public static String MESSAGE_ERROR_NOT_PERMISSION = "You dont have permission to access this function!";
    public static String MESSAFE_ERROR_MUST_AUTH_FIRST = "You have to authenticate first!";
    
    public static String MESSAGE_SUCCESS_CONFIRMED_BORROW_BOOKS = "Confirmed Borrow Books!";
    public static String MESSAGE_SUCCESS_RETURN_BOOKS = "Books has been returned.";
    
    public static String MESSAGE_UNEXPIRED = "Unexpired";
    public static String MESSAGE_EXPIRED = "Expired";
    
    public static final int MAX_NUM_WANTED_COPIES = 5;
    
    public static final int DIALOG_FONT_SIZE = 15;  
}
