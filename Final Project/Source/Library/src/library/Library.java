/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

import javafx.application.Application;
import javafx.stage.Stage;
import library.controller.LoginController;
import library.helper.DatabaseHelper;

/**
 *
 * @author Dang Xuan Bach
 */
public class Library extends Application {

    @Override
    public void start(Stage primaryStage) {
        //DatabaseHelper.clearDatabase();
        DatabaseHelper.getConnection();
        DatabaseHelper.createTablesIfNotExists();
//        UserManager manager = new UserManager();
//        for (int i = 0; i < 15; i++) {
//            Student student = new Student();
//            student.setUsername("student" + String.valueOf(i));
//            student.setPassword("student" + String.valueOf(i));
//            student.setContact(UUID.randomUUID().toString());
//            student.setEmail("student" + String.valueOf(i) + "@example.com");
//            student.setFullName("Student " + String.valueOf(i));
//            student.setGender(Math.random() < 0.5 ? "Male" : "Female");
//            student.setStudentId(String.valueOf(20140000 + i));
//            LocalDate start = LocalDate.now();
//            start.minusDays((long) (Math.random() * (365 * 2)));
//            student.setStudyPeriodStart(start);
//            student.setStudyPeriodEnd(start.plusDays((long) (Math.random() * (365 * 2))));
//            manager.addUser(student);
//            Borrower borrower = new Borrower();
//            borrower.setUsername("borrower" + String.valueOf(i));
//            borrower.setPassword("borrower" + String.valueOf(i));
//            borrower.setContact(UUID.randomUUID().toString());
//            borrower.setEmail("borrower" + String.valueOf(i) + "@example.com");
//            borrower.setFullName("Borrower " + String.valueOf(i));
//            borrower.setGender(Math.random() < 0.5 ? "Male" : "Female");
//            manager.addUser(borrower);
//            Librarian librarian = new Librarian();
//            librarian.setUsername("librarian" + String.valueOf(i));
//            librarian.setPassword("librarian" + String.valueOf(i));
//            librarian.setContact(UUID.randomUUID().toString());
//            librarian.setEmail("librarian" + String.valueOf(i) + "@example.com");
//            librarian.setFullName("Librarian " + String.valueOf(i));
//            librarian.setGender(Math.random() < 0.5 ? "Male" : "Female");
//            manager.addUser(librarian);
//        }
        LoginController lc = new LoginController(primaryStage);
        lc.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
