/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.view;

import java.util.function.Predicate;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import library.controller.ManageUserController;

/**
 *
 * @author Dang Xuan Bach
 */
public class ManageUserView {

    private ObservableList<Result> data;
    private FilteredList<Result> filteredData;
    private final int ROWS_PER_PAGE = 10;
    private final ManageUserController controller;
    private final Stage stage;

    public Stage getStage() {
        return stage;
    }

    public ManageUserView(ManageUserController controller, Stage parent) {
        this.controller = controller;
        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.WINDOW_MODAL);
    }

    public void show() {

        // Create filter side panel
        VBox filterVBox = new VBox();
        Label filterLabel = new Label("Filters");
        filterLabel.setFont(new Font(32));
        filterLabel.setLabelFor(filterVBox);
        filterVBox.setPadding(new Insets(10));
        filterVBox.setMaxWidth(360);
        filterVBox.setMinWidth(360);
        filterVBox.setSpacing(10);

        // Create username filter
        Label usernameLabel = new Label("Username");
        TextField usernameFilter = new TextField();
        usernameLabel.setLabelFor(usernameFilter);

        // Create email filter
        Label emailLabel = new Label("Email");
        TextField emailFilter = new TextField();
        emailLabel.setLabelFor(emailFilter);

        // Create full name filter
        Label fullNameLabel = new Label("Full Name");
        TextField fullNameFilter = new TextField();
        fullNameLabel.setLabelFor(fullNameFilter);

        // Create gender filter
        Label genderLabel = new Label("Gender");
        CheckBox maleCheckBox = new CheckBox("Male");
        maleCheckBox.setSelected(true);
        CheckBox femaleCheckBox = new CheckBox("Female");
        femaleCheckBox.setSelected(true);
        // Create filter button
        Button filterButton = new Button("Apply Filter");
        filterButton.setMaxWidth(Double.MAX_VALUE);
        Button clearButton = new Button("Clear Filter");
        clearButton.setMaxWidth(Double.MAX_VALUE);
        HBox filterButtonBox = new HBox(10, filterButton, clearButton);
        HBox.setHgrow(filterButton, Priority.ALWAYS);
        HBox.setHgrow(clearButton, Priority.ALWAYS);
        filterButtonBox.setMaxWidth(Double.MAX_VALUE);

        filterVBox.getChildren().addAll(filterLabel, usernameLabel,
                usernameFilter, emailLabel, emailFilter, fullNameLabel,
                fullNameFilter, genderLabel, maleCheckBox, femaleCheckBox,
                filterButtonBox);

        // Create result label
        VBox resultVBox = new VBox();
        resultVBox.setPadding(new Insets(10));
        resultVBox.setSpacing(10);
        Label resultLabel = new Label("Results");
        resultLabel.setFont(new Font(32));

        data = FXCollections.observableArrayList();
        // Create result pagination
        Pagination resultPagination = new Pagination();
        VBox.setVgrow(resultPagination, Priority.ALWAYS);
        resultPagination.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                if (filteredData.isEmpty()) {
                    return new Label("No user found");
                }
                int displace = filteredData.size() % ROWS_PER_PAGE;
                int lastIndex = filteredData.size() / ROWS_PER_PAGE;
                // Create result table
                TableView resultTable = new TableView();
                TableColumn usernameCol = new TableColumn("Username");
                usernameCol.setCellValueFactory(new PropertyValueFactory("username"));
                TableColumn emailCol = new TableColumn("Email");
                emailCol.setCellValueFactory(new PropertyValueFactory("email"));
                TableColumn fullNameCol = new TableColumn("Full Name");
                fullNameCol.setCellValueFactory(new PropertyValueFactory("fullName"));
                TableColumn genderCol = new TableColumn("Gender");
                genderCol.setCellValueFactory(new PropertyValueFactory("gender"));
                TableColumn contactCol = new TableColumn("Contact");
                contactCol.setCellValueFactory(new PropertyValueFactory("contact"));
                TableColumn typeCol = new TableColumn("Type");
                typeCol.setCellValueFactory(new PropertyValueFactory("type"));
                TableColumn activateCol = new TableColumn("Activate");
                Callback<TableColumn<Result, String>, TableCell<Result, String>> cellFactory = new Callback<TableColumn<Result, String>, TableCell<Result, String>>() {
                    @Override
                    public TableCell call(final TableColumn<Result, String> param) {
                        final TableCell<Result, String> cell = new TableCell<Result, String>() {
                            final Button btn = new Button("Activate");

                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    Result result = getTableView().getItems().get(getIndex());
                                    if (!result.getStatus().equals("NOT_ACTIVATED")) {
                                        setGraphic(null);
                                        setText(null);
                                        return;
                                    }
                                    btn.setOnAction(event -> {
                                        controller.activateUser(result.getUsername());
                                    });
                                    setGraphic(btn);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };
                activateCol.setCellFactory(cellFactory);
                TableColumn issueCol = new TableColumn("Issue card");
                Callback<TableColumn<Result, String>, TableCell<Result, String>> issueCellFactory = new Callback<TableColumn<Result, String>, TableCell<Result, String>>() {
                    @Override
                    public TableCell call(final TableColumn<Result, String> param) {
                        final TableCell<Result, String> cell = new TableCell<Result, String>() {
                            final Button btn = new Button("Issue card");

                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    Result result = getTableView().getItems().get(getIndex());
                                    btn.setOnAction(event -> {
                                        controller.issueCard(result.getUsername());
                                    });
                                    setGraphic(btn);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };
                issueCol.setCellFactory(issueCellFactory);
                resultTable.getColumns().addAll(usernameCol, emailCol, fullNameCol, genderCol,
                        contactCol, typeCol, activateCol, issueCol);
                if (lastIndex == pageIndex) {
                    resultTable.setItems(FXCollections.observableList(filteredData.subList(pageIndex * ROWS_PER_PAGE, pageIndex * ROWS_PER_PAGE + displace)));
                } else {
                    resultTable.setItems(FXCollections.observableList(filteredData.subList(pageIndex * ROWS_PER_PAGE, pageIndex * ROWS_PER_PAGE + ROWS_PER_PAGE)));
                }
                return resultTable;
            }
        });
        filterButton.setOnAction(event -> {
            Predicate<Result> usernamePredicate = result -> {
                if (usernameFilter.getText() != null && !usernameFilter.getText().isEmpty()) {
                    if (result.getUsername().toLowerCase().contains(usernameFilter.getText().toLowerCase())) {
                        return true;
                    }
                    return false;
                }
                return true;
            };
            Predicate<Result> emailPredicate = result -> {
                if (emailFilter.getText() != null && !emailFilter.getText().isEmpty()) {
                    if (result.getEmail().toLowerCase().contains(emailFilter.getText().toLowerCase())) {
                        return true;
                    }
                    return false;
                }
                return true;
            };
            Predicate<Result> fullNamePredicate = result -> {
                if (fullNameFilter.getText() != null && !fullNameFilter.getText().isEmpty()) {
                    if (result.getFullName().toLowerCase().contains(fullNameFilter.getText().toLowerCase())) {
                        return true;
                    }
                    return false;
                }
                return true;
            };
            Predicate<Result> genderPredicate = result -> {
                if (maleCheckBox.isSelected() && result.getGender().equals("Male")) {
                    return true;
                }
                if (femaleCheckBox.isSelected() && result.getGender().equals("Female")) {
                    return true;
                }
                return false;
            };
            filteredData.setPredicate(usernamePredicate.and(emailPredicate).and(fullNamePredicate).and(genderPredicate));
            resultPagination.setPageCount((filteredData.size() + ROWS_PER_PAGE - 1) / ROWS_PER_PAGE);
        });
        clearButton.setOnAction(event -> {
            filteredData.setPredicate(result -> true);
            resultPagination.setPageCount(0);
            resultPagination.setPageCount((filteredData.size() + ROWS_PER_PAGE - 1) / ROWS_PER_PAGE);
        });
        resultVBox.getChildren().addAll(resultLabel, resultPagination);
        data.addAll(controller.getAllUsers());
        filteredData = new FilteredList(data, result -> true);
        resultPagination.setPageCount((filteredData.size() + ROWS_PER_PAGE - 1) / ROWS_PER_PAGE);
        SplitPane root = new SplitPane(filterVBox, resultVBox);
        Scene scene = new Scene(root, 1280, 800);

        stage.setTitle("Manage User");
        stage.setScene(scene);
        stage.show();
    }

    public static class Result {

        private final StringProperty username;
        private final StringProperty email;
        private final StringProperty fullName;
        private final StringProperty gender;
        private final StringProperty contact;
        private final StringProperty type;
        private final StringProperty status;

        public String getUsername() {
            return username.get();
        }

        public String getEmail() {
            return email.get();
        }

        public String getFullName() {
            return fullName.get();
        }

        public String getGender() {
            return gender.get();
        }

        public String getContact() {
            return contact.get();
        }

        public String getType() {
            return type.get();
        }

        public String getStatus() {
            return status.get();
        }

        public void setUsername(String username) {
            this.username.set(username);
        }

        public void setEmail(String email) {
            this.email.set(email);
        }

        public void setFullName(String fullName) {
            this.fullName.set(fullName);
        }

        public void setGender(String gender) {
            this.gender.set(gender);
        }

        public void setContact(String contact) {
            this.contact.set(contact);
        }

        public void setType(String type) {
            this.type.set(type);
        }

        public void setStatus(String status) {
            this.status.set(status);
        }

        public Result(String username, String email, String fullName, String gender, String contact, String type, String status) {
            this.username = new SimpleStringProperty(username);
            this.email = new SimpleStringProperty(email);
            this.fullName = new SimpleStringProperty(fullName);
            this.gender = new SimpleStringProperty(gender);
            this.contact = new SimpleStringProperty(contact);
            this.type = new SimpleStringProperty(type);
            this.status = new SimpleStringProperty(status);
        }
    }
}
