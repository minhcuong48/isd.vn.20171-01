/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import library.controller.BaseController;
import library.controller.RegisterToBorrowBooksController;
import library.entity.ChoosedCopies;
import library.helper.Constant;


/**
 *
 * @author GiapMinhCuong-20140539
 */
public class RegisterToBorrowBooksView extends BaseView {
    
    // instance variables
    
    private final VBox mainContainer = new VBox();
    private final HBox buttonsContainer = new HBox();
    
    private final TableView<ChoosedCopies> choosedTable = new TableView<>();
    private final TableView<ChoosedCopies> wantedTable = new TableView<>();
    
    private final Button selectButton = new Button("Select");
    private final Button deleteButton = new Button("Delete");
    private final Button registerButton = new Button("Register To Borrow");
    
    private final Label choosedTableLabel = new Label("List of Copies");
    private final Label wantedTableLabel = new Label("List of wanted Copies");

    private ObservableList<ChoosedCopies> wantedItems = FXCollections.observableArrayList();
    
    private ObservableList<ChoosedCopies> choosedItems = null;
    
    // constructors
    
    /**
     * Constructor
     * @param controller Controller that control this view 
     */
    public RegisterToBorrowBooksView(BaseController controller) {
        super(controller);
    }

    // implement abstract methods
    
    @Override
    public Parent initRoot() {
        
        // buttons container
        buttonsContainer.getChildren().addAll(selectButton, 
                deleteButton,
                registerButton);
        
        // main container
        mainContainer.getChildren().addAll(choosedTableLabel,
                choosedTable,
                buttonsContainer,
                wantedTableLabel,
                wantedTable);

        // setup columns
        setupChoosedTableColumns();
        setupWantedTableColumns();
        
        loadAvailableChoosedCopiesTable();
        loadWantedCopiesTable();
        
        setupLayout();
        setupEvents();
        
        return mainContainer;
    }
    
    // private methods
    
    // setup UI
    
    /**
     * Setup events
     */
    private void setupEvents() {
        selectButton.setOnAction((ActionEvent event) -> {
            choooseACopy();
        });
        
        deleteButton.setOnAction((ActionEvent event) -> {
            deleteACopy();
        });
        
        registerButton.setOnAction((ActionEvent event) -> {
            registerToBorrowBooks();
        });
    }
    
    /**
     * Setup UI
     */
    private void setupLayout() {
        // modify UI
        // setup auto increare size based on stage's size
        HBox.setHgrow(choosedTable, Priority.ALWAYS);
        VBox.setVgrow(wantedTable, Priority.ALWAYS);
        
        // setup spacing between components
        mainContainer.setPadding(new Insets(8.0, 8.0, 8.0, 8.0));
        mainContainer.setSpacing(8.0);
        
        buttonsContainer.setSpacing(8.0);
        
        // setup size
        choosedTable.setMinWidth(800);
        choosedTable.setMinHeight(300);
        
        wantedTable.setMinWidth(400);
        wantedTable.setMinHeight(200);
        
        selectButton.setMinSize(100, 50);
        deleteButton.setMinSize(100, 50);
        registerButton.setMinSize(100, 50);
        
        // set noedit on tables
        choosedTable.setEditable(false);
        wantedTable.setEditable(false);
        
        // set fonts
        Font font = Font.font(Font.getDefault().getFamily(), FontWeight.BOLD, Constant.DIALOG_FONT_SIZE);
        choosedTableLabel.setFont(font);
        wantedTableLabel.setFont(font);    
    }
    
    /**
     * Setup choosed table columns
     */
    private void setupChoosedTableColumns() {
        TableColumn<ChoosedCopies, String> bookNumberColumn = new TableColumn<>("Book Number");
        bookNumberColumn.setPrefWidth(150);
        bookNumberColumn.setCellValueFactory(new PropertyValueFactory<>("bookNumber"));
        
        TableColumn<ChoosedCopies, String> sequenceNumberColumn = new TableColumn<>("Sequence Number");
        sequenceNumberColumn.setPrefWidth(150);
        sequenceNumberColumn.setCellValueFactory(new PropertyValueFactory<>("sequenceNumber"));
        
        TableColumn<ChoosedCopies, String> titleColumn = new TableColumn<>("Title");
        titleColumn.setPrefWidth(200);
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        
        TableColumn<ChoosedCopies, Double> priceColumn = new TableColumn<>("Price (VND)");
        priceColumn.setPrefWidth(150);
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        
        TableColumn<ChoosedCopies, String> publisherColumn = new TableColumn<>("Publisher");
        publisherColumn.setPrefWidth(150);
        publisherColumn.setCellValueFactory(new PropertyValueFactory<>("publisher"));
        
        TableColumn<ChoosedCopies, String> authorsColumn = new TableColumn<>("Authors");
        authorsColumn.setPrefWidth(150);
        authorsColumn.setCellValueFactory(new PropertyValueFactory<>("authors"));
        
        TableColumn<ChoosedCopies, String> typeOfCopiesColumn = new TableColumn<>("Type Of Copy");
        typeOfCopiesColumn.setPrefWidth(150);
        typeOfCopiesColumn.setCellValueFactory(new PropertyValueFactory<>("typeOfCopies"));
        
        TableColumn<ChoosedCopies, String> statusColumn = new TableColumn<>("Status");
        statusColumn.setPrefWidth(150);
        statusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        
        TableColumn<ChoosedCopies, String> isbnColumn = new TableColumn<>("ISBN");
        isbnColumn.setPrefWidth(100);
        isbnColumn.setCellValueFactory(new PropertyValueFactory<>("isbn"));
        
        choosedTable.getColumns().addAll(bookNumberColumn,
                sequenceNumberColumn,
                titleColumn,
                priceColumn,
                publisherColumn,
                authorsColumn,
                typeOfCopiesColumn,
                statusColumn,
                isbnColumn);
    }
    
    /**
     * Setup wanted table columns
     */
    private void setupWantedTableColumns() {
        TableColumn<ChoosedCopies, String> bookNumberColumn = new TableColumn<>("Book Number");
        bookNumberColumn.setPrefWidth(150);
        bookNumberColumn.setCellValueFactory(new PropertyValueFactory<>("bookNumber"));
        
        TableColumn<ChoosedCopies, String> sequenceNumberColumn = new TableColumn<>("Sequence Number");
        sequenceNumberColumn.setPrefWidth(150);
        sequenceNumberColumn.setCellValueFactory(new PropertyValueFactory<>("sequenceNumber"));
        
        TableColumn<ChoosedCopies, String> titleColumn = new TableColumn<>("Title");
        titleColumn.setPrefWidth(200);
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        
        TableColumn<ChoosedCopies, Double> priceColumn = new TableColumn<>("Price (VND)");
        priceColumn.setPrefWidth(150);
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        
        TableColumn<ChoosedCopies, String> publisherColumn = new TableColumn<>("Publisher");
        publisherColumn.setPrefWidth(150);
        publisherColumn.setCellValueFactory(new PropertyValueFactory<>("publisher"));
        
        TableColumn<ChoosedCopies, String> authorsColumn = new TableColumn<>("Authors");
        authorsColumn.setPrefWidth(150);
        authorsColumn.setCellValueFactory(new PropertyValueFactory<>("authors"));
        
        TableColumn<ChoosedCopies, String> typeOfCopiesColumn = new TableColumn<>("Type Of Copy");
        typeOfCopiesColumn.setPrefWidth(150);
        typeOfCopiesColumn.setCellValueFactory(new PropertyValueFactory<>("typeOfCopies"));
        
        TableColumn<ChoosedCopies, String> statusColumn = new TableColumn<>("Status");
        statusColumn.setPrefWidth(150);
        statusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        
        TableColumn<ChoosedCopies, String> isbnColumn = new TableColumn<>("ISBN");
        isbnColumn.setPrefWidth(100);
        isbnColumn.setCellValueFactory(new PropertyValueFactory<>("isbn"));
        
        wantedTable.getColumns().addAll(bookNumberColumn,
                sequenceNumberColumn,
                titleColumn,
                priceColumn,
                publisherColumn,
                authorsColumn,
                typeOfCopiesColumn,
                statusColumn,
                isbnColumn);
    }

    /**
     * Load available copies to table
     */
    private void loadAvailableChoosedCopiesTable() {
        choosedItems = getController().getAvailableChoosedCopies();
        choosedTable.setItems(choosedItems);
    }
    
    /**
     * Load wanted copies to table
     */
    private void loadWantedCopiesTable() {
        wantedItems = FXCollections.observableArrayList();
        wantedTable.setItems(wantedItems);
    }
    
    /**
     * Refresh tables
     */
    private void refresh() {
        loadAvailableChoosedCopiesTable();
        loadWantedCopiesTable();
    }
    
    // utils
    
    /**
     * Get controller that control this view
     * @return RegisterToBorrowBooksController
     */
    private RegisterToBorrowBooksController getController() {
        return (RegisterToBorrowBooksController)controller;
    }
    
    /**
     * Check if any item is choosed in table
     * @param item wanted copies
     * @return 
     */
    private boolean isDuplicateWantedCopy(ChoosedCopies item) {
        for(int i=0; i<wantedItems.size(); i++) {
            ChoosedCopies tmpItem = wantedItems.get(i);
            if(tmpItem.getBookNumber().equals(item.getBookNumber())
                    && tmpItem.getSequenceNumber() == item.getSequenceNumber()) {
                return true;
            }
        }
        return false;
    }
    
    // actions respond events
    
    /**
     * Add a copy to list of wanted items
     */
    private void choooseACopy() {
        ChoosedCopies selectedItem = choosedTable.getSelectionModel().getSelectedItem();
        if(selectedItem != null && !isDuplicateWantedCopy(selectedItem)) {
            wantedItems.add(selectedItem);
        }
    }
    
    /**
     * Delete a copy from list of wanted items
     */
    private void deleteACopy() {
        ChoosedCopies selectedItem = wantedTable.getSelectionModel().getSelectedItem();
        if(selectedItem != null) {
            wantedItems.remove(selectedItem);
        }
    }
    
    /**
     * Register to borrow wanted items
     */
    private void registerToBorrowBooks() {
        getController().registerToBorrowBooks(wantedItems);
        refresh();
    }
}
