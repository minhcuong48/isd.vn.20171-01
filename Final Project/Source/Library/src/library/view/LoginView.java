/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import library.controller.LoginController;

/**
 *
 * @author Dang Xuan Bach
 */
public class LoginView {

    private final LoginController controller;
    private final Stage stage;

    public Stage getStage() {
        return stage;
    }

    public LoginView(LoginController controller, Stage parent) {
        this.controller = controller;
        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.WINDOW_MODAL);
    }

    public void show() {
        VBox vBox = new VBox();
        vBox.setPadding(new Insets(10));
        vBox.setPrefWidth(320);
        vBox.setMaxWidth(Double.MAX_VALUE);
        vBox.setMaxHeight(Double.MAX_VALUE);
        Label loginLabel = new Label("Login");
        loginLabel.setFont(new Font(32));
        loginLabel.setLabelFor(vBox);
        Label usernameLabel = new Label("Username");
        TextField usernameInput = new TextField();
        usernameLabel.setLabelFor(usernameInput);
        Label passwordLabel = new Label("Password");
        TextField passwordInput = new TextField();
        passwordLabel.setLabelFor(passwordInput);
        passwordLabel.setPadding(new Insets(10, 0, 0, 0));
        Button loginButton = new Button("Login");
        loginButton.setOnAction(event -> {
            try {
                controller.login(usernameInput.getText(), passwordInput.getText());
                controller.showMenu();
            } catch (IllegalArgumentException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR, ex.getMessage());
                alert.showAndWait();
            }
        });
        loginButton.setMaxWidth(Double.MAX_VALUE);
        Hyperlink signupLink = new Hyperlink("Sign up");
        signupLink.setOnAction(event -> {
            controller.showSignup();
        });
        Hyperlink activateLink = new Hyperlink("Activate account");
        activateLink.setOnAction(event -> {
            controller.showActivate();
        });
        HBox innerBox = new HBox();
        innerBox.setMaxWidth(Double.MAX_VALUE);
        innerBox.setAlignment(Pos.CENTER);
        innerBox.setSpacing(10);
        innerBox.getChildren().addAll(signupLink, activateLink);
        Region r = new Region();
        r.setPadding(new Insets(5));
        vBox.getChildren().addAll(loginLabel,usernameLabel, usernameInput, passwordLabel, passwordInput, r, loginButton, innerBox);
        loginButton.setAlignment(Pos.CENTER);
        vBox.setAlignment(Pos.CENTER_LEFT);
        Scene scene = new Scene(vBox);
        stage.setTitle("Login");
        stage.setScene(scene);
        stage.show();
    }
}
