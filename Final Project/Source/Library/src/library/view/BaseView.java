/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.view;

import com.sun.glass.ui.Size;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import library.controller.BaseController;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public abstract class BaseView {
    
    // instance variables
    
    protected final BaseController controller;
    
    protected String title = "";
    
    protected Stage stage = null;
    protected Scene scene = null;
    protected Parent root = null;
    
    protected BaseView parentView = null;
    protected Stage parentStage = null;
    
    // constructors
    
    public BaseView(BaseController controller) {
        this.controller = controller;
    }

    // abstract methods
    
    /**
     * Constructor
     * @return Root of view
     */
    public abstract Parent initRoot();
    
    // getters and setters
    
    /**
     * Get stage
     * @return 
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * Get parent view
     * @return parent view of this view
     */
    public BaseView getParentView() {
        return parentView;
    }

    /**
     * Set parent view
     * @param parentView parent of this view
     */
    public void setParentView(BaseView parentView) {
        this.parentView = parentView;
        if(parentView != null) {
            parentStage = parentView.getStage();
        }
    }
    
    /**
     * Set title for view
     * @param title title
     */
    public void setTitle(String title) {
         if(title != null) {
            this.title = title;
         }
         if(stage != null) {
             stage.setTitle(title);
         }
    }
    
    /**
     * Set parent stage for current stage. This is related to modal mode
     * @param parentStage 
     */
    public void setParentStage(Stage parentStage) {
        this.parentStage = parentStage;
    }

    // public methods
    
    /**
     * Show view to user
     */
    public void show() {
        // setup stage
        stage = new Stage(getStageStyle());
        stage.initModality(getModality());
        stage.setTitle(title);
        root = initRoot();
        if (getSizeWindow() == null) {
            scene = new Scene(root);
        } else {
            scene = new Scene(root, getSizeWindow().width, getSizeWindow().height);
        }
        stage.setScene(scene);
        setParentStage();
        stage.show();
    }

    // protected methods
    
    /**
     * Get modality
     * @return current modality
     */
    protected Modality getModality() {
        return Modality.NONE;
    }
    
    /**
     * Get Stage Style
     * @return stage style
     */
    protected StageStyle getStageStyle() {
        return StageStyle.DECORATED;
    }
    
    /**
     * get Size of Window
     * @return size of window
     */
    protected Size getSizeWindow() {
        return null;
    }
    
    // private methods
    
    /**
     * Set parent stage
     */
    private void setParentStage() {
//        if (stage != null && parentView != null && parentView.stage != null) {
//            stage.initOwner(parentView.getStage());
//        }
        if(stage != null && parentStage != null) {
            stage.initOwner(parentStage);
        }
    }
    
}
