package library.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import library.controller.UpdateBookController;
import library.entity.Book;

public class UpdateBookView {
    private final UpdateBookController controller;
    private final Stage stage;

    private String bookNumber;

    public Stage getStage() {
        return stage;
    }

    public UpdateBookView(UpdateBookController controller, Stage parent) {
        this.controller = controller;
        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.APPLICATION_MODAL);
    }

    public void show() {

        Text titleWindow = new Text("Update Book");
        Text bookNumberLabel = new Text("Enter book number");
        TextField bookNumberText = new TextField();
        Button buttonUpdate = new Button("Update");
        titleWindow.setStyle("-fx-font: normal bold 15px 'serif' ");
        GridPane gridPane = new GridPane();
        gridPane.setMinSize(300, 300);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);
        gridPane.add(titleWindow, 0, 0);
        gridPane.add(bookNumberLabel, 0, 1);
        gridPane.add(bookNumberText, 1, 1);
        gridPane.add(buttonUpdate, 0, 3);

        buttonUpdate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                bookNumber = bookNumberText.getText();
                if (!controller.checkValidInput(bookNumber)) {
                    displayError("Book number must not be empty.");
                } else if (!controller.checkBookExist(bookNumber)) {
                    displayError("Book " + bookNumber + " not exist.");
                } else {
                    Book result = controller.viewBook(bookNumber);
                    if (result.getBookNumber().isEmpty() || result.getTitle().isEmpty()) {
                        displayError("Can't read book information.");
                    } else {
                        controller.showUpdateBookInfoView(result.getBookNumber(), result.getTitle(), result.getPublisher(), result.getAuthors(), result.getIsbn());
                    }
                }

            }
        });

        Scene scene = new Scene(gridPane);
        stage.setTitle("Update Book");
        stage.setScene(scene);
        stage.show();
    }

    private void displayError(String message) {
        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
        errorAlert.setHeaderText("Error");
        errorAlert.setContentText(message);
        errorAlert.showAndWait();
    }

    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.initOwner(owner);
        alert.showAndWait();
    }
}
