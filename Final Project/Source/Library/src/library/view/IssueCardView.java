/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.view;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import library.controller.IssueCardController;

/**
 *
 * @author Dang Xuan Bach
 */
public class IssueCardView {

    private final IssueCardController controller;
    private final Stage stage;
    private final String username;

    public Stage getStage() {
        return stage;
    }

    public IssueCardView(IssueCardController controller, Stage parent, String username) {
        this.controller = controller;
        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.APPLICATION_MODAL);
        this.username = username;
    }

    public void show() {
        TableView cardTable = new TableView();
        TableColumn idCol = new TableColumn("ID");
        TableColumn issuedCol = new TableColumn("Issued Date");
        TableColumn expiredCol = new TableColumn("Expired Date");
        idCol.setCellValueFactory(new PropertyValueFactory("id"));
        issuedCol.setCellValueFactory(new PropertyValueFactory("issuedDate"));
        expiredCol.setCellValueFactory(new PropertyValueFactory("expiredDate"));
        cardTable.getColumns().addAll(idCol, issuedCol, expiredCol);
        cardTable.setItems(FXCollections.observableArrayList(controller.getAllCards(username)));

        VBox vBox = new VBox();
        Label issueCardLabel = new Label("Issue Borrower Card");
        issueCardLabel.setFont(new Font(32));
        issueCardLabel.setLabelFor(vBox);
        vBox.setSpacing(10);
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(5);
        Label usernameLabel = new Label("Username");
        Label cardIdLabel = new Label("Card Number");
        Label expirationDateLabel = new Label("Expiration Date");
        TextField usernameInput = new TextField(username);
        if (!username.isEmpty()) {
            usernameInput.setEditable(false);
        }
        usernameInput.setMaxWidth(Double.MAX_VALUE);
        TextField cardIdInput = new TextField();
        cardIdInput.setMaxWidth(Double.MAX_VALUE);
        cardIdInput.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                    String newValue) {
                if (!newValue.matches("\\d*")) {
                    cardIdInput.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        DatePicker expirationDatePicker = new DatePicker(LocalDate.now());
        expirationDatePicker.setMaxWidth(Double.MAX_VALUE);
        expirationDatePicker.setConverter(new StringConverter<LocalDate>() {
            private final DateTimeFormatter defaultFormatter = DateTimeFormatter.ofPattern("dd/MM/uuuu");

            @Override
            public String toString(LocalDate object) {
                if (object != null) {
                    return object.format(defaultFormatter);
                }
                return LocalDate.now().format(defaultFormatter);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = LocalDate.parse(string, defaultFormatter);
                if (date.isBefore(LocalDate.now())) {
                    return LocalDate.now();
                }
                return date;
            }
        });
        final Callback<DatePicker, DateCell> dayCellFactory;

        dayCellFactory = (final DatePicker datePicker) -> new DateCell() {
            @Override
            public void updateItem(LocalDate item, boolean empty) {
                super.updateItem(item, empty);
                if (item.isBefore(LocalDate.now())) {
                    setDisable(true);
                }
            }
        };
        expirationDatePicker.setDayCellFactory(dayCellFactory);
        usernameLabel.setLabelFor(usernameInput);
        cardIdLabel.setLabelFor(cardIdInput);
        expirationDateLabel.setLabelFor(expirationDatePicker);
        grid.addRow(0, usernameLabel, usernameInput);
        grid.addRow(1, cardIdLabel, cardIdInput);
        grid.addRow(2, expirationDateLabel, expirationDatePicker);
        ColumnConstraints cc = new ColumnConstraints();
        cc.setHgrow(Priority.NEVER);
        grid.getColumnConstraints().add(cc);
        cc = new ColumnConstraints();
        cc.setHgrow(Priority.ALWAYS);
        grid.getColumnConstraints().add(cc);
        Button issueButton = new Button("Issue");
        issueButton.setOnAction(event -> {
            try {
                controller.issueCard(usernameInput.getText(), cardIdInput.getText(), expirationDatePicker.getValue());
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Card added successful");
                alert.showAndWait();
                stage.close();
            } catch (IllegalArgumentException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR, ex.getMessage());
                alert.showAndWait();
            }
        });
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER_RIGHT);
        hBox.getChildren().add(issueButton);
        vBox.getChildren().addAll(issueCardLabel, grid, hBox);

        HBox outerBox = new HBox();
        outerBox.setPadding(new Insets(10));
        outerBox.setSpacing(10);
        outerBox.getChildren().addAll(cardTable, vBox);
        HBox.setHgrow(vBox, Priority.ALWAYS);
        HBox.setHgrow(cardTable, Priority.ALWAYS);
        Scene scene = new Scene(outerBox, 640, 200);
        stage.setTitle("Issue Borrower card");
        stage.setScene(scene);
        stage.showAndWait();
    }

    public static class CardInfo {

        final public IntegerProperty id;

        public int getId() {
            return id.get();
        }

        public String getIssuedDate() {
            return issuedDate.get();
        }

        public String getExpiredDate() {
            return expiredDate.get();
        }
        final public StringProperty issuedDate;
        final public StringProperty expiredDate;

        public CardInfo(int id, LocalDate issuedDate, LocalDate expiredDate) {
            this.id = new SimpleIntegerProperty(id);
            this.issuedDate = new SimpleStringProperty(issuedDate.toString());
            this.expiredDate = new SimpleStringProperty(expiredDate.toString());
        }
    }
}
