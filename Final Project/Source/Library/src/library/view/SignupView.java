/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.view;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import library.controller.SignupController;

/**
 *
 * @author Dang Xuan Bach
 */
public class SignupView {

    private final SignupController controller;
    private final Stage stage;

    public Stage getStage() {
        return stage;
    }

    public SignupView(SignupController controller, Stage parent) {
        this.controller = controller;
        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.WINDOW_MODAL);
    }

    public void show() {
        VBox vBox = new VBox();
        vBox.setPadding(new Insets(10));
        vBox.setPrefWidth(320);
        vBox.setMaxWidth(Double.MAX_VALUE);
        vBox.setMaxHeight(Double.MAX_VALUE);

        Label accountTypeLabel = new Label("Account Type");
        ChoiceBox accountTypeInput = new ChoiceBox(FXCollections.observableArrayList(
                "Borrower", "Student", "Librarian"));
        accountTypeInput.setValue("Borrower");
        accountTypeInput.setMaxWidth(Double.MAX_VALUE);
        accountTypeLabel.setLabelFor(accountTypeInput);

        Label usernameLabel = new Label("Username");
        TextField usernameInput = new TextField();
        usernameLabel.setLabelFor(usernameInput);
        usernameLabel.setPadding(new Insets(10, 0, 0, 0));
        usernameInput.setPromptText("Please enter your username");

        Label passwordLabel = new Label("Password");
        TextField passwordInput = new TextField();
        passwordLabel.setLabelFor(passwordInput);
        passwordLabel.setPadding(new Insets(10, 0, 0, 0));
        passwordInput.setPromptText("Please enter your password");

        Label fullNameLabel = new Label("Full name");
        TextField fullNameInput = new TextField();
        fullNameLabel.setLabelFor(fullNameInput);
        fullNameLabel.setPadding(new Insets(10, 0, 0, 0));
        fullNameInput.setPromptText("John Doe");

        Label emailLabel = new Label("Email");
        TextField emailInput = new TextField();
        emailLabel.setLabelFor(emailInput);
        emailLabel.setPadding(new Insets(10, 0, 0, 0));
        emailInput.setPromptText("john@example.com");

        Label genderLabel = new Label("Gender");
        ChoiceBox genderInput = new ChoiceBox(FXCollections.observableArrayList(
                "Male", "Female"));
        genderInput.setValue("Male");
        genderInput.setMaxWidth(Double.MAX_VALUE);
        genderLabel.setLabelFor(genderInput);
        genderLabel.setPadding(new Insets(10, 0, 0, 0));

        Label contactLabel = new Label("Contact");
        TextArea contactInput = new TextArea();
        contactLabel.setLabelFor(contactInput);
        contactLabel.setPadding(new Insets(10, 0, 0, 0));
        contactInput.setPromptText("606-3727 Ullamcorper. Street\n"
                + "Roseville NH");

        Label studentIdLabel = new Label("Student ID");
        TextField studentIdInput = new TextField();
        studentIdLabel.setLabelFor(studentIdInput);
        studentIdLabel.setPadding(new Insets(10, 0, 0, 0));
        studentIdInput.setPromptText("20140296");

        DatePicker periodStartDatePicker = new DatePicker(LocalDate.now());
        periodStartDatePicker.setMaxWidth(Double.MAX_VALUE);
        periodStartDatePicker.setConverter(new StringConverter<LocalDate>() {
            private final DateTimeFormatter defaultFormatter = DateTimeFormatter.ofPattern("dd/MM/uuuu");

            @Override
            public String toString(LocalDate object) {
                if (object != null) {
                    return object.format(defaultFormatter);
                }
                return LocalDate.now().format(defaultFormatter);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = LocalDate.parse(string, defaultFormatter);
                return date;
            }
        });
        DatePicker periodEndDatePicker = new DatePicker(LocalDate.now());
        periodEndDatePicker.setMaxWidth(Double.MAX_VALUE);
        periodEndDatePicker.setConverter(new StringConverter<LocalDate>() {
            private final DateTimeFormatter defaultFormatter = DateTimeFormatter.ofPattern("dd/MM/uuuu");

            @Override
            public String toString(LocalDate object) {
                if (object != null) {
                    return object.format(defaultFormatter);
                }
                return LocalDate.now().format(defaultFormatter);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = LocalDate.parse(string, defaultFormatter);
                if (date.isBefore(periodStartDatePicker.getValue())) {
                    return LocalDate.now();
                }
                return date;
            }
        });
        final Callback<DatePicker, DateCell> periodEndCellFactory;

        periodEndCellFactory = (final DatePicker datePicker) -> new DateCell() {
            @Override
            public void updateItem(LocalDate item, boolean empty) {
                super.updateItem(item, empty);
                if (item.isBefore(periodStartDatePicker.getValue())) {
                    setDisable(true);
                }
            }
        };
        periodEndDatePicker.setDayCellFactory(periodEndCellFactory);

        Label periodStartLabel = new Label("Period Start");
        periodStartLabel.setLabelFor(periodStartDatePicker);
        periodStartLabel.setPadding(new Insets(10, 0, 0, 0));

        Label periodEndLabel = new Label("Period End");
        periodEndLabel.setLabelFor(periodEndDatePicker);
        periodEndLabel.setPadding(new Insets(10, 0, 0, 0));

        Button signupButton = new Button("Sign up");
        signupButton.setMaxWidth(Double.MAX_VALUE);
        Region r = new Region();
        r.setPadding(new Insets(5));
        vBox.getChildren().addAll(accountTypeLabel, accountTypeInput,
                usernameLabel, usernameInput,
                passwordLabel, passwordInput,
                fullNameLabel, fullNameInput,
                emailLabel, emailInput,
                genderLabel, genderInput,
                contactLabel, contactInput,
                studentIdLabel, studentIdInput,
                periodStartLabel, periodStartDatePicker,
                periodEndLabel, periodEndDatePicker,
                r, signupButton
        );
        studentIdInput.managedProperty().bind(studentIdInput.visibleProperty());
        studentIdLabel.managedProperty().bind(studentIdLabel.visibleProperty());
        periodStartDatePicker.managedProperty().bind(periodStartDatePicker.visibleProperty());
        periodEndDatePicker.managedProperty().bind(periodEndDatePicker.visibleProperty());
        periodStartLabel.managedProperty().bind(periodStartLabel.visibleProperty());
        periodEndLabel.managedProperty().bind(periodEndLabel.visibleProperty());
        studentIdInput.visibleProperty().bind(accountTypeInput.valueProperty().asString().isEqualTo("Student"));
        studentIdLabel.visibleProperty().bind(accountTypeInput.valueProperty().asString().isEqualTo("Student"));
        periodStartDatePicker.visibleProperty().bind(accountTypeInput.valueProperty().asString().isEqualTo("Student"));
        periodEndDatePicker.visibleProperty().bind(accountTypeInput.valueProperty().asString().isEqualTo("Student"));
        periodStartLabel.visibleProperty().bind(accountTypeInput.valueProperty().asString().isEqualTo("Student"));
        periodEndLabel.visibleProperty().bind(accountTypeInput.valueProperty().asString().isEqualTo("Student"));
        signupButton.setAlignment(Pos.CENTER);
        vBox.setAlignment(Pos.CENTER_LEFT);
        signupButton.setOnAction(event -> {
            try {
                controller.signup(accountTypeInput.getValue().toString(), new SignupInfo(usernameInput.getText(), passwordInput.getText(), fullNameInput.getText(), emailInput.getText(), genderInput.getValue().toString(), contactInput.getText(), studentIdInput.getText(), periodStartDatePicker.getValue(), periodEndDatePicker.getValue()));
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Register successful");
                alert.showAndWait();
                stage.close();
            } catch (IllegalArgumentException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR, ex.getMessage());
                alert.showAndWait();
            }
        });
        Scene scene = new Scene(vBox);
        stage.setTitle("Sign up");
        stage.setScene(scene);
        stage.show();
    }

    public static class SignupInfo {
        public final String username;
        public final String password;
        public final String fullName;
        public final String email;
        public final String gender;
        public final String contact;
        public final String userId;
        public final LocalDate periodStart;
        public final LocalDate periodEnd;
        
        public SignupInfo(String username, String password, String fullName, String email, String gender, String contact, String userId, LocalDate periodStart, LocalDate periodEnd) {
            this.username = username;
            this.password = password;
            this.fullName = fullName;
            this.email = email;
            this.gender = gender;
            this.contact = contact;
            this.userId = userId;
            this.periodStart = periodStart;
            this.periodEnd = periodEnd;
        }
    }
}
