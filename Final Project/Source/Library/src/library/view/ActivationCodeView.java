/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.view;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import library.controller.ActivationCodeController;

/**
 *
 * @author Dang Xuan Bach
 */
public class ActivationCodeView {

    private final ActivationCodeController controller;
    private final Stage stage;
    private final String code;

    public Stage getStage() {
        return stage;
    }

    public ActivationCodeView(ActivationCodeController controller, Stage parent, String code) {
        this.controller = controller;
        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.WINDOW_MODAL);
        this.code = code;
    }

    public void show() {
        if (code == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Cannot generate activation token");
            alert.showAndWait();
            stage.close();
            return;
        }
        VBox vBox = new VBox();
        vBox.setPrefWidth(320);
        Label issueCardLabel = new Label("Activation Code");
        issueCardLabel.setFont(new Font(32));
        issueCardLabel.setLabelFor(vBox);
        vBox.setPadding(new Insets(10));
        vBox.setSpacing(10);
        Label activationCodeLabel = new Label("Generated activation code");
        TextField activationCodeInput = new TextField(code);
        activationCodeInput.setEditable(false);
        activationCodeInput.setMaxWidth(Double.MAX_VALUE);
        activationCodeLabel.setLabelFor(activationCodeInput);
        vBox.getChildren().addAll(issueCardLabel, activationCodeLabel, activationCodeInput);
        Scene scene = new Scene(vBox);

        stage.setTitle("Activation Code");
        stage.setScene(scene);
        stage.showAndWait();
    }

}
