package library.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import library.controller.AddExistBookController;
import library.entity.Copies;

/**
 *
 * @author tuananh
 */
public class AddExistBookView {

    private final AddExistBookController controller;
    private final Stage stage;

    private String bookNumber;
    private int numberOfCopies;
    private Copies.TypeOfCopies type;
    private double price;

    public Stage getStage() {
        return stage;
    }

    public AddExistBookView(AddExistBookController controller, Stage parent) {
        this.controller = controller;
        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.APPLICATION_MODAL);
    }


    public void show() {

        Text titleWindow = new Text("Add Exist Book Form");

        Text bookNumberLabel = new Text("Book number");

        TextField bookNumberText = new TextField();

        Text numberOfCopiesLabel = new Text("Number of new copies");

        TextField numberOfCopiesText = new TextField();

        Text typeLabel = new Text("Types of copy");

        ToggleGroup groupTypes = new ToggleGroup();
        RadioButton referenceRadio = new RadioButton("Reference");
        referenceRadio.setUserData("Reference");
        referenceRadio.setToggleGroup(groupTypes);
        RadioButton borrowableRadio = new RadioButton("Borrowable");
        borrowableRadio.setUserData("Borrowable");
        borrowableRadio.setToggleGroup(groupTypes);

        Text priceLabel = new Text("Price");

        TextField priceText = new TextField();

        Button buttonRegister = new Button("Register");

        titleWindow.setStyle("-fx-font: normal bold 15px 'serif' ");

        //Creating a Grid Pane
        GridPane gridPane = new GridPane();

        //Setting size for the pane
        gridPane.setMinSize(300, 300);

        //Setting the padding
        gridPane.setPadding(new Insets(10, 10, 10, 10));

        //Setting the vertical and horizontal gaps between the columns
        gridPane.setVgap(5);
        gridPane.setHgap(5);

        //Setting the Grid alignment
        gridPane.setAlignment(Pos.CENTER);
        gridPane.add(titleWindow, 0, 0);
        //Arranging all the nodes in the grid
        gridPane.add(bookNumberLabel, 0, 1);
        gridPane.add(bookNumberText, 1, 1);

        gridPane.add(numberOfCopiesLabel, 0, 2);
        gridPane.add(numberOfCopiesText, 1, 2);

        gridPane.add(typeLabel, 0, 3);
        gridPane.add(referenceRadio, 1, 3);
        gridPane.add(borrowableRadio, 1, 4);
        gridPane.add(priceLabel, 0, 5);
        gridPane.add(priceText, 1, 5);

        gridPane.add(buttonRegister, 0, 6);

        buttonRegister.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!controller.checkValidInput(bookNumberText.getText(), numberOfCopiesText.getText(), groupTypes, priceText.getText())) {
                    displayError("Input fields must not be empty.");
                } else if (!controller.checkPriceValid(priceText.getText())) {
                    displayError("Invalid price input.");
                } else if (!controller.checkNumberValid(numberOfCopiesText.getText())) {
                    displayError("Invalid number of copies input.");
                } else {
                    bookNumber = bookNumberText.getText();
                    numberOfCopies = Integer.parseInt(numberOfCopiesText.getText());
                    if (groupTypes.getSelectedToggle().getUserData().toString().equals("Reference")) {
                        type = Copies.TypeOfCopies.REFERENCE;
                    } else if (groupTypes.getSelectedToggle().getUserData().toString().equals("Borrowable")) {
                        type = Copies.TypeOfCopies.BORROWABLE;
                    }
                    price = Double.parseDouble(priceText.getText());
                    if (!controller.checkBookExist(bookNumber)) {
                        displayError("Book " + bookNumber + " not exist.");
                    } else if (controller.addExistBook(bookNumber, numberOfCopies, type, price)) {
                        showAlert(Alert.AlertType.INFORMATION, stage, "Registration Successful!", "Add copies successfully.");
                    } else {
                        displayError("Registration failed!");
                    }
                }
            }
        });

        Scene scene = new Scene(gridPane);
        stage.setTitle("Add Exist Book");
        stage.setScene(scene);
        stage.show();
    }

    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.initOwner(owner);
        alert.showAndWait();
    }

    /**
     * tuananh
     * @param message
     */
    private void displayError(String message) {
        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
        errorAlert.setHeaderText("Error");
        errorAlert.setContentText(message);
        errorAlert.showAndWait();
    }
}
