/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.view;

import java.time.LocalDate;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import library.controller.ChooseCardToRegisterBookController;

/**
 *
 * @author bach0
 */
public class ChooseCardToRegisterBookView {

    private final ChooseCardToRegisterBookController controller;
    private final Stage stage;
    private final String username;

    public Stage getStage() {
        return stage;
    }

    public ChooseCardToRegisterBookView(ChooseCardToRegisterBookController controller, Stage parent, String username) {
        this.controller = controller;
        stage = new Stage();
        this.username = username;
    }

    public void show() {
        TableView cardTable = new TableView();
        TableColumn idCol = new TableColumn("ID");
        TableColumn issuedCol = new TableColumn("Issued Date");
        TableColumn expiredCol = new TableColumn("Expired Date");
        idCol.setCellValueFactory(new PropertyValueFactory("id"));
        issuedCol.setCellValueFactory(new PropertyValueFactory("issuedDate"));
        expiredCol.setCellValueFactory(new PropertyValueFactory("expiredDate"));
        cardTable.getColumns().addAll(idCol, issuedCol, expiredCol);
        cardTable.setItems(FXCollections.observableArrayList(controller.getAllCards(username)));
        Button confirmButton = new Button("Choose card to register book");
        confirmButton.setDisable(true);
        confirmButton.setMaxWidth(Double.MAX_VALUE);
        cardTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        cardTable.getSelectionModel().selectedItemProperty().addListener(event -> {
            confirmButton.setDisable(cardTable.getSelectionModel().getSelectedItem() == null);
        });
        confirmButton.setOnAction(event -> {
            CardInfo card = (CardInfo) cardTable.getSelectionModel().getSelectedItem();
            controller.registerBookToBorrow(card.getId());
        });
        VBox box = new VBox();
        box.setPadding(new Insets(10));
        box.setSpacing(10);
        box.getChildren().addAll(cardTable, confirmButton);
        Scene scene = new Scene(box, 360, 200);
        stage.setTitle("Choose card to register book");
        stage.setScene(scene);
        stage.showAndWait();
    }

    public static class CardInfo {

        final public IntegerProperty id;

        public int getId() {
            return id.get();
        }

        public String getIssuedDate() {
            return issuedDate.get();
        }

        public String getExpiredDate() {
            return expiredDate.get();
        }
        final public StringProperty issuedDate;
        final public StringProperty expiredDate;

        public CardInfo(int id, LocalDate issuedDate, LocalDate expiredDate) {
            this.id = new SimpleIntegerProperty(id);
            this.issuedDate = new SimpleStringProperty(issuedDate.toString());
            this.expiredDate = new SimpleStringProperty(expiredDate.toString());
        }
    }
}
