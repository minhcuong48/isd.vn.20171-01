/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.view;

import java.time.LocalDate;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import library.controller.BaseController;
import library.controller.ReturnBookController;
import library.entity.BorrowerCard;
import library.entity.BorrowedCopies;
import library.helper.Constant;

public class ReturnBookView extends BaseView {

    VBox mainContainer = new VBox();

    TextField searchField = new TextField();
    Button searchButton = new Button("Search");

    Label searchLabelHint = new Label("Search for Card ID:");
    Label usernameLabelHint = new Label("Username:");
    Label cardIdLabelHint = new Label("Card ID:");
    Label activatedDateLabelHint = new Label("Activated Date:");
    Label expiredDateLabelHint = new Label("Expired Date:");
    Label statusLabelHint = new Label("Card Status:");
    Label borrowedTableLabelHint = new Label("List of Borrowed Copies");

    Label usernameLabel = new Label();
    Label cardIdLabel = new Label();
    Label activatedDateLabel = new Label();
    Label expiredDateLabel = new Label();
    Label statusLabel = new Label();

    HBox searchContainer = new HBox();
    HBox usernameContainer = new HBox();
    HBox cardIdContainer = new HBox();
    HBox activatedDateContainer = new HBox();
    HBox expiredDateContainer = new HBox();
    HBox statusContainer = new HBox();
    HBox buttonsContainer = new HBox();

    Button selectAllButton = new Button("Select/Unselect All");
    Button returnBookButton = new Button("Return book");

    TableView<BorrowedCopies> borrowedTableView = new TableView<>();
    ObservableList<BorrowedCopies> borrowedItems;

    TableColumn<BorrowedCopies, String> bookNumberColumn = new TableColumn<>("Book Number");
    TableColumn<BorrowedCopies, String> sequenceNumberColumn = new TableColumn<>("Sequence Number");
    TableColumn<BorrowedCopies, String> lentDate = new TableColumn<>("Lent Date");
    TableColumn<BorrowedCopies, String> expectedReturnDate = new TableColumn<>("Expected Return Date");

    public ReturnBookView(BaseController controller) {
        super(controller);
    }

    @Override
    public Parent initRoot() {
        setTitle("Return Books");
        setupBorrowedTable();
        setupHBoxContainers();
        setupMainContainer();
        setupSpacings();

        setupEvents();
        return mainContainer;
    }

    public void setUserInfo(BorrowerCard card) {
        if (card == null) {
            return;
        }
        searchField.setText("");

        usernameLabel.setText(card.getUser().getUsername());
        cardIdLabel.setText(String.valueOf(card.getId()));
        activatedDateLabel.setText(String.valueOf(card.getIssuedDate()));
        expiredDateLabel.setText(String.valueOf(card.getExpiredDate()));
        if (card.getExpiredDate().isBefore(LocalDate.now())) {
            statusLabel.setText(Constant.MESSAGE_UNEXPIRED);
        } else {
            statusLabel.setText(Constant.MESSAGE_EXPIRED);
        }
    }

    public void setBorrowedCopies(ObservableList<BorrowedCopies> items) {
        borrowedItems = items;
        borrowedTableView.setItems(items);
    }

    private ReturnBookController getController() {
        return (ReturnBookController) controller;
    }

    private void setupBorrowedTable() {
        borrowedTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        bookNumberColumn.setCellValueFactory(new PropertyValueFactory<>("bookNumber"));
        sequenceNumberColumn.setCellValueFactory(new PropertyValueFactory<>("sequenceNumber"));
        lentDate.setCellValueFactory(new PropertyValueFactory<>("lentDate"));
        expectedReturnDate.setCellValueFactory(new PropertyValueFactory<>("expectedReturnDate"));

        borrowedTableView.getColumns().addAll(bookNumberColumn,
                sequenceNumberColumn,
                lentDate,
                expectedReturnDate);
    }

    private void setupHBoxContainers() {
        searchContainer.getChildren().addAll(searchLabelHint,
                searchField,
                searchButton);

        usernameContainer.getChildren().addAll(usernameLabelHint,
                usernameLabel);
        cardIdContainer.getChildren().addAll(cardIdLabelHint,
                cardIdLabel);
        activatedDateContainer.getChildren().addAll(activatedDateLabelHint,
                activatedDateLabel);
        expiredDateContainer.getChildren().addAll(expiredDateLabelHint,
                expiredDateLabel);
        statusContainer.getChildren().addAll(statusLabelHint,
                statusLabel);
        buttonsContainer.getChildren().addAll(selectAllButton,
                returnBookButton);
    }

    private void setupMainContainer() {
        mainContainer.getChildren().addAll(searchContainer,
                usernameContainer,
                cardIdContainer,
                activatedDateContainer,
                expiredDateContainer,
                statusContainer,
                borrowedTableLabelHint,
                borrowedTableView,
                buttonsContainer);
    }

    private void setupSpacings() {
        mainContainer.setSpacing(8.0);
        mainContainer.setPadding(new Insets(8.0, 8.0, 8.0, 8.0));

        searchContainer.setSpacing(8.0);
        usernameContainer.setSpacing(8.0);
        cardIdContainer.setSpacing(8.0);
        activatedDateContainer.setSpacing(8.0);
        expiredDateContainer.setSpacing(8.0);
        statusContainer.setSpacing(8.0);
        buttonsContainer.setSpacing(8.0);

        HBox.setHgrow(searchField, Priority.ALWAYS);
        searchButton.setMinWidth(150);
        searchContainer.setAlignment(Pos.CENTER);

        usernameLabelHint.setMinWidth(150);
        cardIdLabelHint.setMinWidth(150);
        activatedDateLabelHint.setMinWidth(150);
        expiredDateLabelHint.setMinWidth(150);
        statusLabelHint.setMinWidth(150);

        selectAllButton.setMinSize(100, 50);
        returnBookButton.setMinSize(100, 50);

        // table columns
        bookNumberColumn.setMinWidth(150);
        sequenceNumberColumn.setMinWidth(150);
        lentDate.setMinWidth(200);
        expectedReturnDate.setMinWidth(150);
    }

    // events
    private void setupEvents() {
        searchButton.setOnAction((ActionEvent event) -> {
            searchByCardId();
        });

        selectAllButton.setOnAction((ActionEvent event) -> {
            selectAndUnselectAll();
        });

        returnBookButton.setOnAction((ActionEvent event) -> {
            returnBorrowedBooks();
        });
    }

    private void searchByCardId() {
        String cardIdString = searchField.getText();
        int cardId = -1;
        try {
            cardId = Integer.parseInt(cardIdString);
        } catch (NumberFormatException e) {
            getController().showErrorMessage(Constant.MESSAGE_ERROR_CARD_NOT_FOUND);
            return;
        }

        getController().search(cardId);
    }

    private void selectAndUnselectAll() {
        if (borrowedTableView.getSelectionModel().getSelectedItems().isEmpty()) {
            borrowedTableView.getSelectionModel().selectAll();
        } else {
            borrowedTableView.getSelectionModel().clearSelection();
        }
        borrowedTableView.requestFocus();
    }

    private void returnBorrowedBooks() {
        ObservableList<BorrowedCopies> selectedItems = borrowedTableView.getSelectionModel().getSelectedItems();
        getController().returnBorrowedBooks(selectedItems);
        getController().showSuccessMessage(Constant.MESSAGE_SUCCESS_RETURN_BOOKS);

        BorrowerCard card = getController().getBorrowerCard();
        if (card != null) {
            getController().search(card.getId());
        }
    }
}
