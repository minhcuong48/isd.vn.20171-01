package library.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import library.controller.UpdateBookInfoController;

public class UpdateBookInfoView {

    private final UpdateBookInfoController controller;
    private final Stage stage;

    private String bookNumber;
    private String title;
    private String publisher;
    private String authors;
    private String isbn;

    public Stage getStage() {
        return stage;
    }

    public UpdateBookInfoView(UpdateBookInfoController controller, Stage parent, String bookNumber, String title, String publisher, String authors, String isbn) {
        this.controller = controller;
        this.bookNumber = bookNumber;
        this.title = title;
        this.publisher = publisher;
        this.authors = authors;
        this.isbn = isbn;
        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.APPLICATION_MODAL);
    }

    public void show() {

        Text titleWindow = new Text("Update Book Information");
        Text bookNumberLabel = new Text("Book number");
        Text bookNumberText = new Text(bookNumber);
        Text titleLabel = new Text("Name");
        TextField titleText = new TextField(title);
        Text publisherLabel = new Text("Publisher");
        TextField publisherText = new TextField(publisher);
        Text authorsLabel = new Text("Authors");
        TextField authorsText = new TextField(authors);
        Text isbnLabel = new Text("ISBN");
        TextField isbnText = new TextField(isbn);
        Button buttonUpdate = new Button("Update");
        titleText.setPrefWidth(400.0);
        publisherText.setPrefWidth(400.0);
        authorsText.setPrefWidth(400.0);
        isbnText.setPrefWidth(400.0);
        titleWindow.setStyle("-fx-font: normal bold 15px 'serif' ");

        GridPane gridPane = new GridPane();
        gridPane.setMinSize(300, 300);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);
        gridPane.add(titleWindow, 0, 0);
        gridPane.add(bookNumberLabel, 0, 1);
        gridPane.add(bookNumberText, 1, 1);
        gridPane.add(titleLabel, 0, 2);
        gridPane.add(titleText, 1, 2);
        gridPane.add(publisherLabel, 0, 3);
        gridPane.add(publisherText, 1, 3);
        gridPane.add(authorsLabel, 0, 4);
        gridPane.add(authorsText, 1, 4);
        gridPane.add(isbnLabel, 0, 5);
        gridPane.add(isbnText, 1, 5);
        gridPane.add(buttonUpdate, 0, 6);

        buttonUpdate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                title = titleText.getText();
                publisher = publisherText.getText();
                authors = authorsText.getText();
                isbn = isbnText.getText();

                if (!controller.checkValidInput(title, publisher, authors, isbn)) {
                    displayError("Input fields must not be empty.");
                } else if (controller.updateBook(bookNumber, title, publisher, authors, isbn)) {
                    showAlert(Alert.AlertType.INFORMATION, stage, "Update Successful!", "Book " + bookNumber + " has been updated.");
                } else {
                    displayError("Update failed!");
                }
            }
        });

        Scene scene = new Scene(gridPane);
        stage.setTitle("Update Book Information");
        stage.setScene(scene);
        stage.show();
    }

    private void displayError(String message) {
        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
        errorAlert.setHeaderText("Error");
        errorAlert.setContentText(message);
        errorAlert.showAndWait();
    }

    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.initOwner(owner);
        alert.showAndWait();
    }
}
