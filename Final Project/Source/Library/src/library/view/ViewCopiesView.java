package library.view;

import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import library.controller.ViewCopiesController;
import library.entity.Copies;

public class ViewCopiesView {

    private final ViewCopiesController controller;
    private final Stage stage;

    private String bookNumber;

    public Stage getStage() {
        return stage;
    }

    public ViewCopiesView(ViewCopiesController controller, Stage parent, String bookNumber) {
        this.controller = controller;
        this.bookNumber = bookNumber;
        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.APPLICATION_MODAL);
    }

    private ObservableList<Result> data;
    private List<Copies> copiesList;

    public void show() {

        stage.setTitle("Copies List");

        Text titleWindow = new Text("Copies List");
        titleWindow.setStyle("-fx-font: normal bold 15px 'serif' ");

        TableView table = new TableView();
        table.setEditable(false);

        TableColumn bookNumberCol = new TableColumn("Book number");
        bookNumberCol.setMinWidth(100);
        bookNumberCol.setCellValueFactory(new PropertyValueFactory<>("bookNumber"));

        TableColumn sequenceNumberCol = new TableColumn("Sequence number");
        sequenceNumberCol.setMinWidth(50);
        sequenceNumberCol.setCellValueFactory(new PropertyValueFactory<>("sequenceNumber"));

        TableColumn typeCol = new TableColumn("Type");
        typeCol.setMinWidth(150);
        typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));

        TableColumn priceCol = new TableColumn("Price");
        priceCol.setMinWidth(150);
        priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));

        TableColumn statusCol = new TableColumn("Status");
        statusCol.setMinWidth(150);
        statusCol.setCellValueFactory(new PropertyValueFactory<>("Status"));

        table.getColumns().addAll(bookNumberCol, sequenceNumberCol, typeCol, priceCol, statusCol);
        copiesList = controller.getCopiesList(bookNumber);
        data = FXCollections.observableArrayList();

        for (Copies copies: copiesList) {
            data.add(new Result(copies));
        }

        table.setItems(data);

        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(titleWindow, table);

        Scene scene = new Scene(vbox);
        stage.setTitle("Copies List");
        stage.setScene(scene);
        stage.show();

    }

    public static class Result {

        private final StringProperty bookNumber;
        private final StringProperty sequenceNumber;
        private final StringProperty type;
        private final StringProperty price;
        private final StringProperty status;

        public String getBookNumber() {
            return bookNumber.get();
        }

        public String getSequenceNumber() {
            return sequenceNumber.get();
        }

        public String getType() {
            return type.get();
        }

        public String getPrice() {
            return price.get();
        }

        public String getStatus() {
            return status.get();
        }

        public void setBookNumber(String bookNumber) {
            this.bookNumber.set(bookNumber);
        }

        public void setSequenceNumber(String sequenceNumber) {
            this.sequenceNumber.set(sequenceNumber);
        }

        public void setType(String type) {
            this.type.set(type);
        }

        public void setPrice(String price) {
            this.price.set(price);
        }

        public void setStatus(String status) {
            this.status.set(status);
        }

        private Result(Copies copies) {
            this.bookNumber = new SimpleStringProperty(copies.getBookNumber());
            this.sequenceNumber = new SimpleStringProperty(Integer.toString(copies.getSequenceNumber()));
            this.type = new SimpleStringProperty(copies.getTypeOfCopies().name());
            this.price = new SimpleStringProperty(Double.toString(copies.getPrice()));
            this.status = new SimpleStringProperty(copies.getStatus().name());
        }
    }
}
