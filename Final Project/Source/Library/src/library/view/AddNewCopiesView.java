package library.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import library.controller.AddNewCopiesController;
import library.entity.Copies;

public class AddNewCopiesView {

    private final AddNewCopiesController controller;
    private final Stage stage;

    private String title;
    private String publisher;
    private String authors;
    private String isbn;
    private Copies.TypeOfCopies type;
    private double price;

    public Stage getStage() {
        return stage;
    }

    public AddNewCopiesView(AddNewCopiesController controller, Stage parent, String title, String publisher, String authors, String isbn) {

        this.controller = controller;

        this.title = title;
        this.publisher = publisher;
        this.authors = authors;
        this.isbn = isbn;

        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.APPLICATION_MODAL);

    }

    public void show() {

        Text titleWindow = new Text("Add New Book: Copy Information");
        Text titleLabel = new Text("Title");
        Text titleText = new Text(title);
        Text publisherLabel = new Text("Publisher");
        Text publisherText = new Text(publisher);
        Text authorsLabel = new Text("Authors");
        Text authorsText = new Text(authors);
        Text isbnLabel = new Text("ISBN");
        Text isbnText = new Text(isbn);

        Text typeLabel = new Text("Types of copy");
        ToggleGroup groupTypes = new ToggleGroup();
        RadioButton referenceRadio = new RadioButton("Reference");
        referenceRadio.setUserData("Reference");
        referenceRadio.setToggleGroup(groupTypes);
        RadioButton borrowableRadio = new RadioButton("Borrowable");
        borrowableRadio.setUserData("Borrowable");
        borrowableRadio.setToggleGroup(groupTypes);

        Text priceLabel = new Text("Price");
        TextField priceText = new TextField();
        Button buttonRegister = new Button("Register");
        titleWindow.setStyle("-fx-font: normal bold 15px 'serif' ");
        GridPane gridPane = new GridPane();
        gridPane.setMinSize(400, 400);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);
        gridPane.add(titleWindow, 0, 0);
        gridPane.add(titleLabel, 0, 1);
        gridPane.add(titleText, 1, 1);
        gridPane.add(publisherLabel, 0, 2);
        gridPane.add(publisherText, 1, 2);
        gridPane.add(authorsLabel, 0, 3);
        gridPane.add(authorsText, 1, 3);
        gridPane.add(isbnLabel, 0, 4);
        gridPane.add(isbnText, 1, 4);
        gridPane.add(typeLabel, 0, 5);
        gridPane.add(referenceRadio, 1, 5);
        gridPane.add(borrowableRadio, 1, 6);
        gridPane.add(priceLabel, 0, 7);
        gridPane.add(priceText, 1, 7);
        gridPane.add(buttonRegister, 0, 8);

        buttonRegister.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if (!controller.checkValidInput(groupTypes, priceText.getText())) {
                    displayError("Input fields must not be empty.");
                } else if (!controller.checkPriceValid(priceText.getText())) {
                    displayError("Invalid price input.");
                } else {
                    if (groupTypes.getSelectedToggle().getUserData().toString().equals("Reference")) {
                        type = Copies.TypeOfCopies.REFERENCE;
                    } else if (groupTypes.getSelectedToggle().getUserData().toString().equals("Borrowable")) {
                        type = Copies.TypeOfCopies.BORROWABLE;
                    }
                    price = Double.parseDouble(priceText.getText());

                    if (controller.addNewBook(title, publisher, authors, isbn, type, price)) {
                        showAlert(Alert.AlertType.INFORMATION, stage, "Registration Successful!", "Add new book successfully.");
                        stage.hide();
                    } else {
                        displayError("Registration failed!");
                    }
                }
            }
        });

        Scene scene = new Scene(gridPane);
        stage.setTitle("Add New Book");
        stage.setScene(scene);
        stage.show();
    }

    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.initOwner(owner);
        alert.showAndWait();
    }

    /**
     * tuananh
     * @param message
     */
    private void displayError(String message) {
        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
        errorAlert.setHeaderText("Error");
        errorAlert.setContentText(message);
        errorAlert.showAndWait();
    }
}
