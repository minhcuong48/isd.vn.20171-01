/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.view;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import library.controller.ActivateAccountController;

/**
 *
 * @author Dang Xuan Bach
 */
public class ActivateAccountView {

    private final Stage stage;
    private final ActivateAccountController controller;

    public ActivateAccountView(ActivateAccountController controller, Stage parent) {
        this.controller = controller;
        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.WINDOW_MODAL);
    }

    public void show() {
        VBox vBox = new VBox();
        vBox.setMaxWidth(Double.MAX_VALUE);
        vBox.setMaxHeight(Double.MAX_VALUE);
        vBox.setPadding(new Insets(10));
        vBox.setSpacing(10);
        Label activateAccountLabel = new Label("Activate account");
        activateAccountLabel.setFont(new Font(32));
        activateAccountLabel.setLabelFor(vBox);
        TextField activationCodeInput = new TextField();
        activationCodeInput.setMaxWidth(Double.MAX_VALUE);
        activationCodeInput.setPromptText("Please enter your activation code");
        Button activateButton = new Button("Activate");
        activateButton.setOnAction(event -> {
            try {
                controller.activate(activationCodeInput.getText());
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Your account is activated");
                alert.showAndWait();
                stage.close();
            } catch (IllegalArgumentException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR, ex.getMessage());
                alert.showAndWait();
            }
        });
        HBox innerBox = new HBox();
        HBox.setHgrow(activationCodeInput, Priority.ALWAYS);
        innerBox.getChildren().addAll(activationCodeInput, activateButton);
        vBox.getChildren().addAll(activateAccountLabel, innerBox);
        Scene scene = new Scene(vBox, 480, 120);
        stage.setTitle("Activate Account");
        stage.setScene(scene);
        stage.show();
    }
}
