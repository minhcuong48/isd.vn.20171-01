package library.view;

import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import library.controller.SearchResultController;
import library.entity.Book;

public class SearchResultView {

    private final SearchResultController controller;
    private final Stage stage;

    private String bookNumber;
    private String title;
    private String publisher;
    private String authors;
    private String isbn;

    private ObservableList<Result> data;
    private List<Book> bookList;

    public SearchResultView(SearchResultController controller, Stage parent, String bookNumber, String title, String publisher, String authors, String isbn) {
        this.controller = controller;

        this.bookNumber = bookNumber;
        this.title = title;
        this.publisher = publisher;
        this.authors = authors;
        this.isbn = isbn;

        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.APPLICATION_MODAL);
    }

    public Stage getStage() {
        return stage;
    }

    public void show() {

        Text titleWindow = new Text("Book List Result");
        titleWindow.setStyle("-fx-font: normal bold 15px 'serif' ");

        TableView table = new TableView();
        table.setEditable(false);

        TableColumn bookNumberCol = new TableColumn("Book number");
        bookNumberCol.setMinWidth(100);
        bookNumberCol.setCellValueFactory(new PropertyValueFactory<>("bookNumber"));

        TableColumn titleCol = new TableColumn("Title");
        titleCol.setMinWidth(50);
        titleCol.setCellValueFactory(new PropertyValueFactory<>("title"));

        TableColumn publisherCol = new TableColumn("Publisher");
        publisherCol.setMinWidth(150);
        publisherCol.setCellValueFactory(new PropertyValueFactory<>("publisher"));

        TableColumn authorsCol = new TableColumn("Authors");
        authorsCol.setMinWidth(150);
        authorsCol.setCellValueFactory(new PropertyValueFactory<>("authors"));

        TableColumn isbnCol = new TableColumn("ISBN");
        isbnCol.setMinWidth(150);
        isbnCol.setCellValueFactory(new PropertyValueFactory<>("isbn"));

        table.getColumns().addAll(bookNumberCol, titleCol, publisherCol, authorsCol, isbnCol);
        bookList = controller.getBookList(bookNumber, title, publisher, authors, isbn);
        data = FXCollections.observableArrayList();

        for (Book book: bookList) {
            data.add(new Result(book));
        }

        table.setItems(data);

        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(titleWindow, table);

        Scene scene = new Scene(vbox);
        stage.setTitle("Search Result");
        stage.setScene(scene);
        stage.show();
    }

    public static class Result {

        private final StringProperty bookNumber;
        private final StringProperty title;
        private final StringProperty publisher;
        private final StringProperty authors;
        private final StringProperty isbn;

        public String getBookNumber() {
            return bookNumber.get();
        }

        public String getTitle() {
            return title.get();
        }

        public String getPublisher() {
            return publisher.get();
        }

        public String getAuthors() {
            return authors.get();
        }

        public String getIsbn() {
            return isbn.get();
        }

        public void setBookNumber(String bookNumber) {
            this.bookNumber.set(bookNumber);
        }

        public void setTitle(String title) {
            this.title.set(title);
        }

        public void setPublisher(String type) {
            this.publisher.set(type);
        }

        public void setAuthors(String authors) {
            this.authors.set(authors);
        }

        public void setIsbn(String isbn) {
            this.isbn.set(isbn);
        }

        private Result(Book book) {
            this.bookNumber = new SimpleStringProperty(book.getBookNumber());
            this.title = new SimpleStringProperty(book.getTitle());
            this.publisher = new SimpleStringProperty(book.getPublisher());
            this.authors = new SimpleStringProperty(book.getAuthors());
            this.isbn = new SimpleStringProperty(book.getIsbn());
        }
    }
}
