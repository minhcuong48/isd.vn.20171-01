/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.view;

import java.time.LocalDate;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import library.controller.BaseController;
import library.controller.ConfirmBorrowBooksController;
import library.entity.BorrowerCard;
import library.entity.WantedCopies;
import library.helper.Constant;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class ConfirmBorrowBooksView extends BaseView {

    VBox mainContainer = new VBox();

    TextField searchField = new TextField();
    Button searchButton = new Button("Search");

    Label searchLabelHint = new Label("Search for Card ID:");
    Label usernameLabelHint = new Label("Username:");
    Label cardIdLabelHint = new Label("Card ID:");
    Label activatedDateLabelHint = new Label("Activated Date:");
    Label expiredDateLabelHint = new Label("Expired Date:");
    Label statusLabelHint = new Label("Card Status:");
    Label wantedTableLabelHint = new Label("List of Wanted Copies");

    Label usernameLabel = new Label();
    Label cardIdLabel = new Label();
    Label activatedDateLabel = new Label();
    Label expiredDateLabel = new Label();
    Label statusLabel = new Label();

    HBox searchContainer = new HBox();
    HBox usernameContainer = new HBox();
    HBox cardIdContainer = new HBox();
    HBox activatedDateContainer = new HBox();
    HBox expiredDateContainer = new HBox();
    HBox statusContainer = new HBox();
    HBox buttonsContainer = new HBox();

    Button selectAllButton = new Button("Select/Unselect All");
    Button confirmButton = new Button("Confirm");

    TableView<WantedCopies> wantedTableView = new TableView<>();
    ObservableList<WantedCopies> wantedItems;

    TableColumn<WantedCopies, String> bookNumberColumn = new TableColumn<>("Book Number");
    TableColumn<WantedCopies, String> sequenceNumberColumn = new TableColumn<>("Sequence Number");
    TableColumn<WantedCopies, String> titleColumn = new TableColumn<>("Title");
    TableColumn<WantedCopies, String> borrowedDateColumn = new TableColumn<>("Borrowed Date");
    TableColumn<WantedCopies, Double> priceColumn = new TableColumn<>("Price");
    TableColumn<WantedCopies, String> publisherColumn = new TableColumn<>("Publisher");
    TableColumn<WantedCopies, String> authorsColumn = new TableColumn<>("Authors");
    TableColumn<WantedCopies, String> isbnColumn = new TableColumn<>("ISBN");

    /**
     * Constructor
     * @param controller The controller that control this view
     */
    public ConfirmBorrowBooksView(BaseController controller) {
        super(controller);
    }

    /**
     * Create root of view
     * @return root of view
     */
    @Override
    public Parent initRoot() {
        setTitle("Confirm Borrow Books");
        setupWantedTable();
        setupHBoxContainers();
        setupMainContainer();
        setupSpacings();

        setupEvents();
        return mainContainer;
    }

    /**
     * Set information of specific card in view
     * @param card Borrower card
     */
    public void setUserInfo(BorrowerCard card) {
        if (card == null) {
            return;
        }
        searchField.setText("");

        usernameLabel.setText(card.getUser().getUsername());
        cardIdLabel.setText(String.valueOf(card.getId()));
        activatedDateLabel.setText(String.valueOf(card.getIssuedDate()));
        expiredDateLabel.setText(String.valueOf(card.getExpiredDate()));
        if (card.getExpiredDate().isBefore(LocalDate.now())) {
            statusLabel.setText(Constant.MESSAGE_UNEXPIRED);
        } else {
            statusLabel.setText(Constant.MESSAGE_EXPIRED);
        }
    }

    /**
     * Set borrowed copies to show in table view
     * @param items Borrowed Copies
     */
    public void setBorrowedCopies(ObservableList<WantedCopies> items) {
        wantedItems = items;
        wantedTableView.setItems(items);
    }

    /**
     * Get controller that control this view
     * @return ConfirmBorrowBooksController
     */
    private ConfirmBorrowBooksController getController() {
        return (ConfirmBorrowBooksController) controller;
    }

    /**
     * Setup table to show list of wanted copies
     */
    private void setupWantedTable() {
        wantedTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        bookNumberColumn.setCellValueFactory(new PropertyValueFactory<>("bookNumber"));
        sequenceNumberColumn.setCellValueFactory(new PropertyValueFactory<>("sequenceNumber"));
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        borrowedDateColumn.setCellValueFactory(new PropertyValueFactory<>("borrowedDate"));
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        publisherColumn.setCellValueFactory(new PropertyValueFactory<>("publisher"));
        authorsColumn.setCellValueFactory(new PropertyValueFactory<>("authors"));
        isbnColumn.setCellValueFactory(new PropertyValueFactory<>("isbn"));

        wantedTableView.getColumns().addAll(bookNumberColumn,
                sequenceNumberColumn,
                titleColumn,
                borrowedDateColumn,
                priceColumn,
                publisherColumn,
                authorsColumn,
                isbnColumn);
    }

    /**
     * Setup UI
     */
    private void setupHBoxContainers() {
        searchContainer.getChildren().addAll(searchLabelHint,
                searchField,
                searchButton);

        usernameContainer.getChildren().addAll(usernameLabelHint,
                usernameLabel);
        cardIdContainer.getChildren().addAll(cardIdLabelHint,
                cardIdLabel);
        activatedDateContainer.getChildren().addAll(activatedDateLabelHint,
                activatedDateLabel);
        expiredDateContainer.getChildren().addAll(expiredDateLabelHint,
                expiredDateLabel);
        statusContainer.getChildren().addAll(statusLabelHint,
                statusLabel);
        buttonsContainer.getChildren().addAll(selectAllButton,
                confirmButton);
    }

    /**
     * Setup UI
     */
    private void setupMainContainer() {
        mainContainer.getChildren().addAll(searchContainer,
                usernameContainer,
                cardIdContainer,
                activatedDateContainer,
                expiredDateContainer,
                statusContainer,
                wantedTableLabelHint,
                wantedTableView,
                buttonsContainer);
    }

    /**
     * Setup UI
     */
    private void setupSpacings() {
        mainContainer.setSpacing(8.0);
        mainContainer.setPadding(new Insets(8.0, 8.0, 8.0, 8.0));

        searchContainer.setSpacing(8.0);
        usernameContainer.setSpacing(8.0);
        cardIdContainer.setSpacing(8.0);
        activatedDateContainer.setSpacing(8.0);
        expiredDateContainer.setSpacing(8.0);
        statusContainer.setSpacing(8.0);
        buttonsContainer.setSpacing(8.0);

        HBox.setHgrow(searchField, Priority.ALWAYS);
        searchButton.setMinWidth(150);
        searchContainer.setAlignment(Pos.CENTER);

        usernameLabelHint.setMinWidth(150);
        cardIdLabelHint.setMinWidth(150);
        activatedDateLabelHint.setMinWidth(150);
        expiredDateLabelHint.setMinWidth(150);
        statusLabelHint.setMinWidth(150);

        selectAllButton.setMinSize(100, 50);
        confirmButton.setMinSize(100, 50);

        // table columns
        bookNumberColumn.setMinWidth(150);
        sequenceNumberColumn.setMinWidth(150);
        titleColumn.setMinWidth(200);
        borrowedDateColumn.setMinWidth(150);
        priceColumn.setMinWidth(150);
        publisherColumn.setMinWidth(150);
        authorsColumn.setMinWidth(150);
        isbnColumn.setMinWidth(150);
    }

    // events
    
    /**
     * Setup events
     */
    private void setupEvents() {
        searchButton.setOnAction((ActionEvent event) -> {
            searchByCardId();
        });

        selectAllButton.setOnAction((ActionEvent event) -> {
            selectAndUnselectAll();
        });

        confirmButton.setOnAction((ActionEvent event) -> {
            comfirmBorrowBooks();
        });
    }

    /**
     * Send search message to controller
     */
    private void searchByCardId() {
        String cardIdString = searchField.getText();
        int cardId = -1;
        try {
            cardId = Integer.parseInt(cardIdString);
        } catch (NumberFormatException e) {
            getController().showErrorMessage(Constant.MESSAGE_ERROR_CARD_NOT_FOUND);
            return;
        }

        getController().search(cardId);
    }

    /**
     * Select or Unselect all rows of table
     */
    private void selectAndUnselectAll() {
        if (wantedTableView.getSelectionModel().getSelectedItems().isEmpty()) {
            wantedTableView.getSelectionModel().selectAll();
        } else {
            wantedTableView.getSelectionModel().clearSelection();
        }
        wantedTableView.requestFocus();
    }

    /**
     * Confirm borrow copies
     */
    private void comfirmBorrowBooks() {
        ObservableList<WantedCopies> selectedItems = wantedTableView.getSelectionModel().getSelectedItems();
        getController().confirmBorrowBooks(selectedItems);
        getController().showSuccessMessage(Constant.MESSAGE_SUCCESS_CONFIRMED_BORROW_BOOKS);

        BorrowerCard card = getController().getBorrowerCard();
        if (card != null) {
            getController().search(card.getId());
        }
    }
}
