/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import library.controller.AddNewBookController;

/**
 *
 * @author tuananh
 */
public class AddNewBookView {

    private final AddNewBookController controller;
    private final Stage stage;

    public Stage getStage() {
        return stage;
    }

    public AddNewBookView(AddNewBookController controller, Stage parent) {
        this.controller = controller;
        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.APPLICATION_MODAL);
    }

    public void show() {

        Text titleWindow = new Text("Add New Book Form");
        Text titleLabel = new Text("Name");
        TextField titleText = new TextField();
        Text publisherLabel = new Text("Publisher");
        TextField publisherText = new TextField();
        Text authorsLabel = new Text("Authors");
        TextField authorsText = new TextField();
        Text isbnLabel = new Text("ISBN");
        TextField isbnText = new TextField();
        Button buttonRegister = new Button("Register");
        titleWindow.setStyle("-fx-font: normal bold 15px 'serif' ");
        //Creating a Grid Pane
        GridPane gridPane = new GridPane();
        //Setting size for the pane
        gridPane.setMinSize(300, 300);
        //Setting the padding
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        //Setting the vertical and horizontal gaps between the columns
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        //Setting the Grid alignment
        gridPane.setAlignment(Pos.CENTER);
        gridPane.add(titleWindow, 0, 0);
        //Arranging all the nodes in the grid
        gridPane.add(titleLabel, 0, 1);
        gridPane.add(titleText, 1, 1);
        gridPane.add(publisherLabel, 0, 2);
        gridPane.add(publisherText, 1, 2);
        gridPane.add(authorsLabel, 0, 3);
        gridPane.add(authorsText, 1, 3);
        gridPane.add(isbnLabel, 0, 4);
        gridPane.add(isbnText, 1, 4);
        gridPane.add(buttonRegister, 0, 5);

        buttonRegister.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (controller.checkValidInput(titleText.getText(), publisherText.getText(), authorsText.getText(), isbnText.getText())) {
                    controller.showNewCopiesView(titleText.getText(), publisherText.getText(), authorsText.getText(), isbnText.getText());
                } else {
                    displayError("Input fields must not be empty.");
                }
            }
        });

        Scene scene = new Scene(gridPane);
        stage.setTitle("Add New Book");
        stage.setScene(scene);
        stage.show();
    }

    private void displayError(String message) {
        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
        errorAlert.setHeaderText("Error");
        errorAlert.setContentText(message);
        errorAlert.showAndWait();
    }
}
