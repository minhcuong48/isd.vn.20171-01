package library.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import library.controller.SearchBookController;

public class SearchBookView {

    private final SearchBookController controller;
    private final Stage stage;

    private static final String EMPTY = "";
    private String bookNumber;
    private String title;
    private String publisher;
    private String authors;
    private String isbn;

    public SearchBookView(SearchBookController controller, Stage parent) {
        this.controller = controller;

        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.APPLICATION_MODAL);
    }

    public Stage getStage() {
        return stage;
    }

    public void show() {
        Stage stage = new Stage();
        Text titleWindow = new Text("Search Book Form");
        Text bookNumberLabel = new Text("Book number");
        TextField bookNumberText = new TextField();
        Text titleLabel = new Text("Name");
        TextField titleText = new TextField();
        Text publisherLabel = new Text("Publisher");
        TextField publisherText = new TextField();
        Text authorsLabel = new Text("Authors");
        TextField authorsText = new TextField();
        Text isbnLabel = new Text("ISBN");
        TextField isbnText = new TextField();
        Button buttonSearch = new Button("Search");
        titleWindow.setStyle("-fx-font: normal bold 15px 'serif' ");
        GridPane gridPane = new GridPane();
        gridPane.setMinSize(300, 300);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);
        gridPane.add(titleWindow, 0, 0);
        gridPane.add(bookNumberLabel, 0, 1);
        gridPane.add(bookNumberText, 1, 1);
        gridPane.add(titleLabel, 0, 2);
        gridPane.add(titleText, 1, 2);
        gridPane.add(publisherLabel, 0, 3);
        gridPane.add(publisherText, 1, 3);
        gridPane.add(authorsLabel, 0, 4);
        gridPane.add(authorsText, 1, 4);
        gridPane.add(isbnLabel, 0, 5);
        gridPane.add(isbnText, 1, 5);
        gridPane.add(buttonSearch, 0, 6);

        buttonSearch.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                bookNumber = bookNumberText.getText();
                title = titleText.getText();
                publisher = publisherText.getText();
                authors = authorsText.getText();
                isbn = isbnText.getText();

                if (bookNumber.isEmpty()) bookNumber = EMPTY;
                if (title.isEmpty()) title = EMPTY;
                if (publisher.isEmpty()) publisher = EMPTY;
                if (authors.isEmpty()) authors = EMPTY;
                if (isbn.isEmpty()) isbn = EMPTY;

                controller.showBookResultView(bookNumber, title, publisher, authors, isbn);
            }
        });

        Scene scene = new Scene(gridPane);
        stage.setTitle("Search Book View");
        stage.setScene(scene);
        stage.show();
    }

    private void displayError(String message) {
        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
        errorAlert.setHeaderText("Error");
        errorAlert.setContentText(message);
        errorAlert.showAndWait();
    }
}
