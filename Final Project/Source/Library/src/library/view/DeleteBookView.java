package library.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import library.controller.DeleteBookController;

public class DeleteBookView {

    private final DeleteBookController controller;
    private final Stage stage;

    private String bookNumber;
    private int sequenceNumber;

    public Stage getStage() {
        return stage;
    }

    public DeleteBookView(DeleteBookController controller, Stage parent) {
        this.controller = controller;
        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.APPLICATION_MODAL);
    }

    public void show() {

        Text titleWindow = new Text("Delete Book/Copies Form");
        Text bookNumberLabel = new Text("Book number");
        TextField bookNumberText = new TextField();
        Text sequenceNumberLabel = new Text("Sequence number");
        TextField sequenceNumberText = new TextField();
        Button deleteCopiesButton = new Button("Delete copies");
        Button deleteBookButton = new Button("Delete book (all copies)");
        titleWindow.setStyle("-fx-font: normal bold 15px 'serif' ");
        GridPane gridPane = new GridPane();
        gridPane.setMinSize(300, 300);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);
        gridPane.add(titleWindow, 0, 0);
        gridPane.add(bookNumberLabel, 0, 1);
        gridPane.add(bookNumberText, 1, 1);
        gridPane.add(sequenceNumberLabel, 0, 2);
        gridPane.add(sequenceNumberText, 1, 2);
        gridPane.add(deleteCopiesButton, 0, 3);
        gridPane.add(deleteBookButton, 0, 4);

        deleteCopiesButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if (!controller.checkValidInput(bookNumberText.getText())) {
                    displayError("Book number field must not be empty.");
                } else if (!controller.checkSequenceValid(sequenceNumberText.getText())) {
                    displayError("Sequence number invalid.");
                } else {
                    bookNumber = bookNumberText.getText();
                    sequenceNumber = Integer.parseInt(sequenceNumberText.getText());
                    if (!controller.checkCopiesExist(bookNumber, sequenceNumber)) {
                        displayError("This copies not exist.");
                    } else if (controller.deleteCopies(bookNumber, sequenceNumber)) {
                        showAlert(Alert.AlertType.INFORMATION, stage, "Delete Successful!", "Delete copies successfully.");
                    } else {
                        displayError("Delete copies failed!");
                    }
                }
            }
        });

        deleteBookButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!controller.checkValidInput(bookNumberText.getText())) {
                    displayError("Book number field must not be empty.");
                } else {
                    bookNumber = bookNumberText.getText();
                    if (!controller.checkBookExist(bookNumber)) {
                        displayError("This book not exist.");
                    } else if (controller.deleteBook(bookNumber)) {
                        showAlert(Alert.AlertType.INFORMATION, stage, "Delete Successful!", "Delete book " + bookNumber + " successfully.");
                    } else {
                        displayError("Delete book failed!");
                    }
                }
            }
        });

        Scene scene = new Scene(gridPane);
        stage.setTitle("Delete Book/Copies");
        stage.setScene(scene);
        stage.show();
    }

    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.initOwner(owner);
        alert.showAndWait();
    }

    private void displayError(String message) {
        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
        errorAlert.setHeaderText("Error");
        errorAlert.setContentText(message);
        errorAlert.showAndWait();
    }
}