package library.view;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import library.controller.MenuController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Dang Xuan Bach
 */
public class MenuView {

    private final Stage stage;
    private final MenuController controller;
    private final String type;

    public MenuView(MenuController controller, Stage parent, String type) {
        this.controller = controller;
        this.type = type;
        stage = new Stage();
        stage.initOwner(parent);
        stage.initModality(Modality.WINDOW_MODAL);
    }

    public void show() {
        VBox vBox = new VBox();
        vBox.setMaxWidth(Double.MAX_VALUE);
        vBox.setMaxHeight(Double.MAX_VALUE);
        vBox.setPadding(new Insets(10));
        if (type.equals("LIBRARIAN")) {
            vBox.getChildren().addAll(
                    createButton("Confirm borrow books", () -> controller.confirmBorrow()),
                    createButton("Return books", () -> controller.returnBook()),
                    createButton("Add new book", () -> controller.addNewBook()),
                    createButton("Add exist book", () -> controller.addExistBook()),
                    createButton("View book", () -> controller.viewBook()),
                    createButton("Delete book", () -> controller.deleteBook()),
                    createButton("Search book", () -> controller.searchBook()),
                    createButton("Manage user", () -> controller.manageUser())
            );
        } else {
            vBox.getChildren().addAll(
                    createButton("Register borrow books", () -> controller.registerBorrow()),
                    createButton("Search book", () -> controller.searchBook())
            );
        }
        vBox.getChildren().add(createButton("Logout", () -> {
            controller.logout();
            stage.close();
        }));
        Scene scene = new Scene(vBox, 240, 240);
        stage.setTitle("Menu");
        stage.setScene(scene);
        stage.show();
    }

    private Button createButton(String name, Runnable function) {
        Button button = new Button(name);
        button.setMaxWidth(Double.MAX_VALUE);
        button.setOnAction(e -> {
            function.run();
        });
        return button;
    }

}
