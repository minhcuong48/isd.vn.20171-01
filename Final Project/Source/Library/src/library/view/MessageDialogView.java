/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.view;

import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import library.controller.BaseController;
import library.helper.Constant;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class MessageDialogView extends BaseView {
    private String message = "";
    
    private AnchorPane root;
    private VBox contentBox;
    private Label messageLabel;

    public MessageDialogView(BaseController controller) {
        super(controller);
    }
    
    @Override
    public Parent initRoot() {

        // create root
        AnchorPane root = new AnchorPane();
        
        // create content box
        contentBox = new VBox();
        contentBox.setAlignment(Pos.CENTER);
        
        // setup content box
        contentBox.setSpacing(8.0);
        
        // create message label
        messageLabel = new Label(message);
        contentBox.getChildren().add(messageLabel);
        
        // setup message label
        Font font = new Font(Font.getDefault().getName(), Constant.DIALOG_FONT_SIZE);
        messageLabel.setFont(font);
        
        // set image for message
        if(title.equals(Constant.TITLE_ERROR)) {
            Image image = new Image(getClass().getResourceAsStream("/assets/images/error-64.png"));  
            messageLabel.setGraphic(new ImageView(image));
        }
        else if(title.equals(Constant.TITLE_SUCCESS)) {
            Image image = new Image(getClass().getResourceAsStream("/assets/images/success-64.png"));
             messageLabel.setGraphic(new ImageView(image));
        }
        
        
        
        // create ok button
        Button okButton = new Button("Understood!");
        contentBox.getChildren().add(okButton);
        
        // assign action for ok button
        okButton.setOnAction((ActionEvent event) -> {
            Button okButton1 = (Button)event.getSource();
            Stage stage1 = (Stage) okButton1.getScene().getWindow();
            stage1.close();
        });
        
        // setup constraints
        root.getChildren().add(contentBox);
        
        AnchorPane.setTopAnchor(contentBox, 16.0);
        AnchorPane.setBottomAnchor(contentBox, 16.0);
        AnchorPane.setLeftAnchor(contentBox, 16.0);
        AnchorPane.setRightAnchor(contentBox, 16.0);
        
        return root;
    }

    @Override
    protected StageStyle getStageStyle() {
        return StageStyle.UTILITY;
    }

    @Override
    protected Modality getModality() {
        return Modality.WINDOW_MODAL;
    }

    public void setInfo(String title, String message) {
        this.title = title;
        this.message = message;
    } 
}
