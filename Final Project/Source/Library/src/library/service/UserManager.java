/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.service;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import library.entity.*;
import library.helper.DatabaseHelper;

/**
 * Manage user by storing them in to a database
 *
 * @author Dang Xuan Bach
 */
public class UserManager {

    /**
     * Add an user to the database
     *
     * @param user The user to be added
     * @return true if user added successful, false otherwise
     */
    public boolean addUser(User user) {
        if (user == null) {
            return false;
        }
        // Check if any required fields is null
        if (user.getUsername() == null || user.getFullName() == null || user.getContact() == null || user.getGender() == null || user.getPassword() == null) {
            return false;
        }
        // Check if any required fields is empty
        if (user.getUsername().isEmpty() || user.getFullName().isEmpty() || user.getContact().isEmpty() || user.getGender().isEmpty() || user.getPassword().isEmpty()) {
            return false;
        }

        if (checkUserExists(user.getUsername()) || checkEmailExists(user.getUsername())) {
            return false;
        }

        // If account type is student need to provide student ID and study period
        if (user.getType() == User.Type.STUDENT) {
            Student student = (Student) user;
            if (student.getStudentId().isEmpty() || !student.getStudyPeriodEnd().isAfter(student.getStudyPeriodStart())) {
                return false;
            }
            if (checkStudentIdExists(student.getStudentId())) {
                return false;
            }
        }
        // Persist user in database
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO user(username,password,full_name,email,gender,contact,status,type) VALUES (?,?,?,?,?,?,?,?);");
            stmt.setString(1, user.getUsername());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getFullName());
            stmt.setString(4, user.getEmail());
            stmt.setString(5, user.getGender());
            stmt.setString(6, user.getContact());
            if (user.getType() == User.Type.STUDENT || user.getType() == User.Type.BORROWER) {
                stmt.setString(7, User.Status.NOT_ACTIVATED.name());
            } else {
                stmt.setString(7, User.Status.ACTIVE.name());
            }
            stmt.setString(8, user.getType().name());
            stmt.executeUpdate();
            stmt.close();
            // Persist student info
            if (user.getType() == User.Type.STUDENT) {
                Student student = (Student) user;
                stmt = conn.prepareStatement("INSERT INTO student(username,student_id,period_start,period_end) VALUES (?,?,?,?);");
                stmt.setString(1, student.getUsername());
                stmt.setString(2, student.getStudentId());
                stmt.setDate(3, Date.valueOf(student.getStudyPeriodStart()));
                stmt.setDate(4, Date.valueOf(student.getStudyPeriodEnd()));
                stmt.executeUpdate();
                stmt.close();
            }
            conn.commit();
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    /**
     * Update an existing user in the database with new information
     *
     * @param user contains new information
     * @return true if user updated successful, false otherwise
     */
    public boolean updateUser(User user) {
        if (user == null) {
            return false;
        }
        if (user.getUsername() == null) {
            return false;
        }
        if (!checkUserExists(user.getUsername())) {
            return false;
        }
        Connection conn = DatabaseHelper.getConnection();
        try {
            String sql = "UPDATE %s SET %s=? WHERE username=?";
            if (user.getContact() != null && !user.getContact().isEmpty()) {
                PreparedStatement stmt;
                stmt = conn.prepareStatement(String.format(sql, "user", "contact"));
                stmt.setString(1, user.getContact());
                stmt.setString(2, user.getUsername());
                stmt.executeUpdate();
                stmt.close();
            }
            if (user.getFullName() != null && !user.getFullName().isEmpty()) {
                PreparedStatement stmt = conn.prepareStatement(String.format(sql, "user", "full_name"));
                stmt.setString(1, user.getFullName());
                stmt.setString(2, user.getUsername());
                stmt.executeUpdate();
                stmt.close();
            }
            if (user.getGender() != null && !user.getGender().isEmpty()) {
                PreparedStatement stmt = conn.prepareStatement(String.format(sql, "user", "gender"));
                stmt.setString(1, user.getGender());
                stmt.setString(2, user.getUsername());
                stmt.executeUpdate();
                stmt.close();
            }
            if (user.getPassword() != null && !user.getPassword().isEmpty()) {
                PreparedStatement stmt = conn.prepareStatement(String.format(sql, "user", "password"));
                stmt.setString(1, user.getPassword());
                stmt.setString(2, user.getUsername());
                stmt.executeUpdate();
                stmt.close();
            }
            if (user.getType() != null && user.getType() == User.Type.STUDENT) {
                Student student = (Student) user;
                if (student.getStudyPeriodStart().isAfter(student.getStudyPeriodEnd())) {
                    return false;
                }
                if (student.getStudentId() != null && !student.getStudentId().isEmpty()) {
                    PreparedStatement stmt = conn.prepareStatement(String.format(sql, "student", "student_id"));
                    stmt.setString(1, student.getStudentId());
                    stmt.setString(2, user.getUsername());
                    stmt.executeUpdate();
                    stmt.close();
                }
                if (student.getStudentId() != null && student.getStudyPeriodStart() != LocalDate.MIN && student.getStudyPeriodStart() != LocalDate.MIN && student.getStudyPeriodStart() != null) {
                    PreparedStatement stmt = conn.prepareStatement(String.format(sql, "student", "period_start"));
                    stmt.setDate(1, Date.valueOf(student.getStudyPeriodStart()));
                    stmt.setString(2, user.getUsername());
                    stmt.executeUpdate();
                    stmt.close();
                }
                if (student.getStudentId() != null && student.getStudyPeriodEnd() != LocalDate.MIN && student.getStudyPeriodEnd() != LocalDate.MIN && student.getStudyPeriodEnd() != null) {
                    PreparedStatement stmt = conn.prepareStatement(String.format(sql, "student", "period_end"));
                    stmt.setDate(1, Date.valueOf(student.getStudyPeriodEnd()));
                    stmt.setString(2, user.getUsername());
                    stmt.executeUpdate();
                    stmt.close();
                }
            }

            conn.commit();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     * Check if an user exist in database
     *
     * @param username the username of user
     * @return true if user exist, false otherwise
     */
    private boolean checkUserExists(String username) {
        if (username == null || username.equals("")) {
            return false;
        }
        Connection conn = DatabaseHelper.getConnection();
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement("SELECT username FROM user WHERE username=?;");
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                rs.close();
                stmt.close();
                return true;
            }
            rs.close();
            stmt.close();
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     * Check if an email exist in database
     *
     * @param email the email to be checked
     * @return true if email exist, false otherwise
     */
    private boolean checkEmailExists(String email) {
        Connection conn = DatabaseHelper.getConnection();
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement("SELECT email FROM user WHERE email=?;");
            stmt.setString(1, email);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                rs.close();
                stmt.close();
                return true;
            }
            rs.close();
            stmt.close();
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return true;
        }
    }

    /**
     * Check if student id already exists
     *
     * @param studentId the id to be checked
     * @return true if student id exist false otherwise
     */
    private boolean checkStudentIdExists(String studentId) {
        Connection conn = DatabaseHelper.getConnection();
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement("SELECT student_id FROM student WHERE student_id=?;");
            stmt.setString(1, studentId);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                rs.close();
                stmt.close();
                return true;
            }
            rs.close();
            stmt.close();
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return true;
        }
    }

    /**
     * Generate activate token for user and invalidate old token
     *
     * @param username username of user
     * @return the newly generated activate token
     */
    public String generateActivateToken(String username) {
        if (!checkUserExists(username)) {
            return null;
        }
        CardManager cardManager = new CardManager();
        if (cardManager.getCardByUser(getUserByUsername(username)).isEmpty()) {
            return null;
        }
        String TOKENCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 32) { // length of the random string.
            int index = (int) (rnd.nextFloat() * TOKENCHARS.length());
            salt.append(TOKENCHARS.charAt(index));
        }
        String token = salt.toString();
        Connection conn = DatabaseHelper.getConnection();
        try {
            String sql = "UPDATE user SET activate_token=? WHERE username=?";
            PreparedStatement stmt;
            stmt = conn.prepareStatement(String.format(sql, "contact"));
            stmt.setString(1, token);
            stmt.setString(2, username);
            stmt.executeUpdate();
            stmt.close();
            conn.commit();
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return token;
    }

    /**
     * Activate an user using activate token
     *
     * @param activateToken the activate token
     * @return return the user that was activated, null if activation failed
     */
    public User activateUser(String activateToken) {
        Connection conn = DatabaseHelper.getConnection();
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement("SELECT username,activate_token FROM user WHERE activate_token=?;");
            stmt.setString(1, activateToken);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                String username = rs.getString("username");
                CardManager cardManager = new CardManager();
                if (cardManager.getCardByUser(getUserByUsername(username)).isEmpty()) {
                    return null;
                }
                PreparedStatement updateStmt = conn.prepareStatement("UPDATE user SET status=?,activate_token=? WHERE username=?;");
                updateStmt.setString(1, User.Status.ACTIVE.name());
                updateStmt.setString(2, null);
                updateStmt.setString(3, username);
                updateStmt.executeUpdate();
                conn.commit();
                rs.close();
                stmt.close();
                updateStmt.close();
                return getUserByUsername(username);
            }
            rs.close();
            stmt.close();
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Find an user by his/her username
     *
     * @param username the username of user
     * @return the user with matching username, null if no user found
     */
    public User getUserByUsername(String username) {
        Connection conn = DatabaseHelper.getConnection();
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement("SELECT * FROM user WHERE username=?;");
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                User.Type type = User.Type.valueOf(rs.getString("type"));
                User user = null;
                if (null != type) {
                    switch (type) {
                        case ADMIN:
                            user = new Admin();
                            break;
                        case BORROWER:
                            user = new Borrower();
                            break;
                        case LIBRARIAN:
                            user = new Librarian();
                            break;
                        case STUDENT:
                            user = new Student();
                            break;
                        default:
                            return null;
                    }
                }
                user.setUsername(rs.getString("username"));
                user.setContact(rs.getString("contact"));
                user.setEmail(rs.getString("email"));
                user.setFullName(rs.getString("full_name"));
                user.setGender(rs.getString("gender"));
                user.setPassword(rs.getString("password"));
                user.setStatus(User.Status.valueOf(rs.getString("status")));
                rs.close();
                stmt.close();
                if (type == User.Type.STUDENT) {
                    stmt = conn.prepareStatement("SELECT * FROM student WHERE username=?;");
                    stmt.setString(1, user.getUsername());
                    rs = stmt.executeQuery();
                    rs.next();
                    Student student = (Student) user;
                    student.setStudentId(rs.getString("student_id"));
                    student.setStudyPeriodStart(rs.getDate("period_start").toLocalDate());
                    student.setStudyPeriodEnd(rs.getDate("period_end").toLocalDate());
                }
                return user;
            }
            rs.close();
            stmt.close();
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Get all users in the database
     *
     * @return list of all users
     */
    public List<User> getAllUsers() {
        ArrayList<User> users = new ArrayList();
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt;
            stmt = conn.createStatement();
            try (ResultSet rs = stmt.executeQuery("SELECT username FROM user")) {
                while (rs.next()) {
                    User user = getUserByUsername(rs.getString("username"));
                    users.add(user);
                }
            }
            stmt.close();
            return users;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
