package library.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import library.helper.DatabaseHelper;

public class DeleteCopiesManager {

    /**
     * Xóa 1 bản sao khỏi database.
     * @param bookNumber Mã sách
     * @param sequenceNumber Số hiệu sách
     * @return true nếu xóa thành công, false nếu xóa thất bại
     */
    public boolean deleteCopies(String bookNumber, int sequenceNumber) {
        if (bookNumber.isEmpty() || sequenceNumber < 1)
            return false;
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement("DELETE FROM copies WHERE book_number=? AND sequence_number=?;");
            stmt.setString(1, bookNumber);
            stmt.setInt(2, sequenceNumber);
            stmt.executeUpdate();
            stmt.close();
            conn.commit();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
