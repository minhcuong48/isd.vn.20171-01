/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.service;

import java.sql.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import library.entity.BorrowedCopies;
import library.helper.DatabaseHelper;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class BorrowedCopiesService {
    private static BorrowedCopiesService defaultInstance;
    
    public static BorrowedCopiesService getDefault() {
        if(defaultInstance == null) {
            defaultInstance = new BorrowedCopiesService();
        }
        return defaultInstance;
    }
    
    private BorrowedCopiesService() {}
    
    /**
     * Count number of borrowed copies belongs to specific card id
     * @param cardId Borrower Card ID
     * @return number of borrowed copies
     */
    public int countBorrowedCopies(int cardId) {
        int count = 0;
        String sql = "SELECT count(id) as count from borrowed_copies WHERE id=? GROUP BY id;";
        
        try {
            PreparedStatement stmt = DatabaseHelper.getConnection().prepareStatement(sql);
            stmt.setInt(1, cardId);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                count = rs.getInt("count");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return count;
    }
    
    /**
     * Add items to Borrowed Copies list
     * @param items Borrowed copies
     */
    public void add(ObservableList<BorrowedCopies> items) {
        String sql = "INSERT INTO borrowed_copies(id, book_number, sequence_number, lent_date, expected_return_date) VALUES(?,?,?,?,?);";
        Connection conn = DatabaseHelper.getConnection();
        for (int i=0; i<items.size(); i++) {
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                BorrowedCopies item = items.get(i);
                
                stmt.setInt(1, item.getCardId());
                stmt.setString(2, item.getBookNumber());
                stmt.setInt(3, item.getSequenceNumber());
                stmt.setDate(4, Date.valueOf(item.getLentDate()));
                stmt.setDate(5, Date.valueOf(item.getExpectedReturnDate()));
                
                stmt.execute();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }  
        }
        try {
            conn.commit();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Get all of borrowed copies
     * @param cardId Borrower Card ID
     * @return List of borrowed copies
     */
    public ObservableList<BorrowedCopies> all(int cardId) {
        ObservableList<BorrowedCopies> list = FXCollections.observableArrayList();
        String sql = "SELECT * from borrowed_copies "
                + "INNER JOIN copies ON borrowed_copies.book_number = copies.book_number "
                + "AND borrowed_copies.sequence_number = copies.sequence_number "
                + "INNER JOIN book ON copies.book_number = book.book_number "
                + "INNER JOIN borrower_card ON borrowed_copies.id = borrower_card.id "
                + "INNER JOIN user ON borrower_card.username = user.username "
                + "WHERE borrower_card.id=?;";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, cardId);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                BorrowedCopies copy = new BorrowedCopies();

                copy.setCardId(rs.getInt("id"));
                copy.setBookNumber(rs.getString("book_number"));
                copy.setSequenceNumber(rs.getInt("sequence_number"));
                copy.setLentDate(rs.getDate("lent_date").toLocalDate());
                copy.setExpectedReturnDate(rs.getDate("expected_return_date").toLocalDate());

                list.add(copy);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    /**
     * Delete items from borrowed copies
     * @param items deleting items
     */
    public void delete(ObservableList<BorrowedCopies> items) {
        Connection conn = DatabaseHelper.getConnection();
        for (int i=0; i<items.size(); i++) {
            BorrowedCopies item = items.get(i);
            String sql = "DELETE FROM borrowed_copies WHERE id=? AND book_number=? AND sequence_number=?;";
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, item.getCardId());
                stmt.setString(2, item.getBookNumber());
                stmt.setInt(3, item.getSequenceNumber());
                stmt.execute();
                conn.commit();
                stmt.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
