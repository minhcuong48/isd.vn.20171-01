package library.service;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import library.entity.BorrowerCard;
import library.entity.Student;
import library.entity.User;
import library.helper.DatabaseHelper;

/**
 * Manage card by storing them in to a database
 *
 * @author Dang Xuan Bach
 */
public class CardManager {

    /**
     * Store a card to database, the card must not be issued and issue date and
     * expire date must be valid
     *
     * @param card the card to be insert
     * @return true if the card inserted successfully, false otherwise
     */
    public boolean addCard(BorrowerCard card) {
        if (card == null || card.getUser() == null) {
            return false;
        }
        if (checkCardExists(card.getId())) {
            return false;
        }
        Connection conn = DatabaseHelper.getConnection();
        try {
            if (card.getUser().getType() == User.Type.STUDENT) {
                Student student = (Student) card.getUser();
                if (!getActiveCards(student, card.getIssuedDate(), card.getExpiredDate()).isEmpty()) {
                    return false;
                }
                if (card.getExpiredDate().isAfter(student.getStudyPeriodEnd())) {
                    return false;
                }
                if (card.getIssuedDate().isBefore(student.getStudyPeriodStart())) {
                    return false;
                }
            }
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO borrower_card(id, issued_date, expired_date, username) VALUES(?,?,?,?);");
            stmt.setInt(1, card.getId());
            stmt.setDate(2, Date.valueOf(card.getIssuedDate()));
            stmt.setDate(3, Date.valueOf(card.getExpiredDate()));
            stmt.setString(4, card.getUser().getUsername());
            stmt.executeUpdate();
            stmt.close();
            conn.commit();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(CardManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     * Update a card with new information, the card must be exist and expire
     * date must be valid
     *
     * @param card
     * @return true if information updated successfully, false otherwise
     */
    public boolean updateCard(BorrowerCard card) {
        if (card == null || card.getUser() == null) {
            return false;
        }
        if (!checkCardExists(card.getId())) {
            return false;
        }
        Connection conn = DatabaseHelper.getConnection();
        try {
            if (card.getUser().getType() == User.Type.STUDENT) {
                Student student = (Student) card.getUser();
                List<BorrowerCard> cards = getActiveCards(student, card.getIssuedDate(), card.getExpiredDate());
                cards.removeIf(c -> {
                    return c.getId() == card.getId();
                });
                if (!cards.isEmpty()) {
                    return false;
                }
                if (card.getExpiredDate().isAfter(student.getStudyPeriodEnd())) {
                    return false;
                }
                if (card.getIssuedDate().isBefore(student.getStudyPeriodStart())) {
                    return false;
                }
            }
            PreparedStatement stmt = conn.prepareStatement("UPDATE borrower_card SET expired_date=? WHERE id=?");
            stmt.setDate(1, Date.valueOf(card.getExpiredDate()));
            stmt.setInt(2, card.getId());
            stmt.executeUpdate();
            stmt.close();
            conn.commit();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(CardManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     * Check if card with id exist
     *
     * @param id the id of the card
     * @return true if card exist, false otherwise
     */
    private boolean checkCardExists(int id) {
        Connection conn = DatabaseHelper.getConnection();
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement("SELECT id FROM borrower_card WHERE id=?;");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                rs.close();
                stmt.close();
                return true;
            }
            rs.close();
            stmt.close();
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(CardManager.class.getName()).log(Level.SEVERE, null, ex);
            return true;
        }
    }

    /**
     * Get all cards owned by a user
     *
     * @param user the user owning the cards
     * @return list of card owned by user
     */
    public List<BorrowerCard> getCardByUser(User user) {
        ArrayList<BorrowerCard> cards = new ArrayList();
        if (user == null || user.getUsername() == null || user.getUsername().isEmpty()) {
            return cards;
        }
        Connection conn = DatabaseHelper.getConnection();
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement("SELECT id FROM borrower_card WHERE username=?;");
            stmt.setString(1, user.getUsername());
            System.out.println(stmt);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                BorrowerCard card = getCardById(rs.getInt("id"));
                cards.add(card);
            }
            rs.close();
            stmt.close();
            return cards;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Get card information using its id
     *
     * @param id the id of the card
     * @return the information of the card with id
     */
    public BorrowerCard getCardById(int id) {
        Connection conn = DatabaseHelper.getConnection();
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement("SELECT * FROM borrower_card WHERE id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                BorrowerCard card = new BorrowerCard();
                card.setId(rs.getInt("id"));
                card.setExpiredDate(rs.getDate("expired_date").toLocalDate());
                card.setIssuedDate(rs.getDate("issued_date").toLocalDate());
                UserManager um = new UserManager();
                card.setUser(um.getUserByUsername(rs.getString("username")));
                rs.close();
                stmt.close();
                return card;
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(CardManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Get all cards in database
     *
     * @return list of cards in database
     */
    public List<BorrowerCard> getAllCards() {
        ArrayList<BorrowerCard> cards = new ArrayList();
        Connection conn = DatabaseHelper.getConnection();
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement("SELECT id FROM borrower_card;");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                BorrowerCard card = getCardById(rs.getInt("id"));
                cards.add(card);
            }
            rs.close();
            stmt.close();
            return cards;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Get currently active cards of student
     *
     * @param student the student owning cards
     * @return list of active card
     */
    public List<BorrowerCard> getActiveCards(Student student) {
        return getActiveCards(student, student.getStudyPeriodStart(), student.getStudyPeriodEnd());
    }

    /**
     * Get currently active cards of user at a point in time
     *
     * @param user the user owning the cards
     * @param date the date to be checked
     * @return list of active cards, empty if there is no active card
     */
    public List<BorrowerCard> getActiveCards(User user, LocalDate date) {
        return getActiveCards(user, date, date, true, true);
    }

    /**
     * Get currently active cards of user in a range of time, the start and end
     * time is inclusive
     *
     * @param user the user owning the cards
     * @param start the start date to be checked
     * @param end the end date to be checked
     * @return list of active cards, empty if there is no active card
     */
    public List<BorrowerCard> getActiveCards(User user, LocalDate start, LocalDate end) {
        return getActiveCards(user, start, end, true, true);
    }

    /**
     * Get currently active cards of user in a range of time
     *
     * @param user the user owning the cards
     * @param start the start date to be checked
     * @param end the end date to be checked
     * @param start_inclusive true if start time is inclusive
     * @param end_inclusive true if end time is inclusive
     * @return list of active cards, empty if there is no active card
     */
    public List<BorrowerCard> getActiveCards(User user, LocalDate start, LocalDate end, boolean start_inclusive, boolean end_inclusive) {
        if (user == null || start == null || end == null) {
            return new ArrayList<BorrowerCard>();
        }
        List<BorrowerCard> cards = getAllCards();
        cards.removeIf(c -> {
            if (!c.getUser().getUsername().equals(user.getUsername())) {
                return true;
            }
            if (start_inclusive && c.getIssuedDate().isEqual(start)) {
                return false;
            }
            if (end_inclusive && c.getExpiredDate().isEqual(end)) {
                return false;
            }
            return !(c.getIssuedDate().isBefore(end) && c.getExpiredDate().isAfter(start));
        });
        return cards;
    }
}
