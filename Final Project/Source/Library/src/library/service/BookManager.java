package library.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import library.entity.Book;
import library.helper.DatabaseHelper;

public class BookManager {

    /**
     * Lấy ra danh sách các quyển sách thỏa mãn điều kiện tìm kiếm.
     * @param bookNumber Chuỗi kí tự tìm kiếm mã sách
     * @param title Chuỗi kí tự tìm kiếm tiêu đề sách
     * @param publisher Chuỗi kí tự tìm kiếm nhà xuất bản
     * @param authors Chuỗi kí tự tìm kiếm tác giả
     * @param isbn Chuỗi kí tự tìm kiếm mã isbn
     * @return Danh sách quyển sách thỏa mãn điều kiện
     */
    public List<Book> getBookList(String bookNumber, String title, String publisher, String authors, String isbn) {
        ArrayList<Book> bookList = new ArrayList<>();
        Connection conn = DatabaseHelper.getConnection();
        StringBuilder builderSql = new StringBuilder("SELECT * FROM book WHERE 1=1");
        if (!bookNumber.isEmpty()) builderSql.append(" AND book_number LIKE \'%").append(bookNumber).append("%\'");
        if (!title.isEmpty()) builderSql.append(" AND title LIKE \'%").append(title).append("%\'");
        if (!publisher.isEmpty()) builderSql.append(" AND publisher LIKE \'%").append(publisher).append("%\'");
        if (!authors.isEmpty()) builderSql.append(" AND authors LIKE \'%").append(authors).append("%\'");
        if (!isbn.isEmpty()) builderSql.append(" AND isbn LIKE \'%").append(isbn).append("%\'");
        builderSql.append(";");
        String sql = builderSql.toString();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Book book = new Book();
                book.setBookNumber(rs.getString("book_number"));
                book.setTitle(rs.getString("title"));
                book.setPublisher(rs.getString("publisher"));
                book.setAuthors(rs.getString("authors"));
                book.setIsbn(rs.getString("isbn"));
                bookList.add(book);
            }
            rs.close();
            stmt.close();
            return bookList;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    /**
     * Kiểm tra xem sách đã tồn tại chưa
     * @param bookNumber Mã sách
     * @return true nếu sách đã tồn tại, false nếu không
     */
    public boolean checkBookExist(String bookNumber) {
        int result;
        String sql = "select 1 FROM book WHERE book_number=?;";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, bookNumber);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            result = rs.getInt(1);
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
//            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            result = 0;
        }
        return (result > 0);
    }

    /**
     * Cho biết thông tin của một quyển sách
     * @param bookNumber mã sách
     * @return book
     */
    public Book viewBook(String bookNumber) {
        String sql = "SELECT book_number,title,publisher,authors,isbn FROM book WHERE book_number=?";
        Connection conn = DatabaseHelper.getConnection();
        PreparedStatement stmt;
        Book book = new Book();
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, bookNumber);
            ResultSet rs = stmt.executeQuery();
            String title = rs.getString(2);
            String publisher = rs.getString(3);
            String authors = rs.getString(4);
            String isbn = rs.getString(5);
            book.setBookNumber(bookNumber);
            book.setTitle(title);
            book.setPublisher(publisher);
            book.setAuthors(authors);
            book.setIsbn(isbn);
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return book;
    }
}
