package library.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import library.entity.Copies;
import library.helper.DatabaseHelper;

public class AddNewBookManager {

    /**
     * Thêm sách mới và bản sao đầu tiên vào database.
     * @param title Tên sách
     * @param publisher Nhà xuất bản
     * @param authors Tác giả
     * @param isbn Mã ISBN
     * @param type Loại bản sao
     * @param price Giá bản sao
     * @return true nếu thêm thành công, false nếu thêm thất bại
     */
    public boolean addNewBook(String title, String publisher, String authors, String isbn, Copies.TypeOfCopies type, double price) {

        String newBookNumber = generateNewBookNumber();
        int sequenceNumber = generateNewSequenceNumber();
        Copies.Status status = Copies.Status.AVAILABLE;
        if (newBookNumber == null || newBookNumber.isEmpty()) {
            return false;
        }
        if (title.isEmpty() || publisher.isEmpty() || authors.isEmpty() || isbn.isEmpty()) {
            return false;
        }
        if (price <= 0.0) {
            return false;
        }
        if (generateNewBook(newBookNumber, title, publisher, authors, isbn)) {
            return (generateNewCopies(newBookNumber, sequenceNumber, type, price, status));
        } else {
            return false;
        }
    }

    /**
     * Thêm sách mới vào database
     * @param bookNumber Mã sách
     * @param title Tên sách
     * @param publisher Nhà xuất bản
     * @param authors Tác giả
     * @param isbn Mã ISBN
     * @return true nếu thêm thành công, false nếu thêm thất bại
     */
    private boolean generateNewBook(String bookNumber, String title, String publisher, String authors, String isbn) {
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO book(book_number,title,publisher,authors,isbn) VALUES (?,?,?,?,?);");
            stmt.setString(1, bookNumber);
            stmt.setString(2, title);
            stmt.setString(3, publisher);
            stmt.setString(4, authors);
            stmt.setString(5, isbn);
            stmt.executeUpdate();
            stmt.close();
            conn.commit();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     * Thêm bản sao mới vào database
     * @param bookNumber Mã sách
     * @param sequenceNumber Số hiệu bản sao
     * @param type Loại bản sao
     * @param price Giá bản sao
     * @param status Trạng thái của bản sao
     * @return true nếu thêm thành công, false nếu thêm thất bại
     */
    private boolean generateNewCopies(String bookNumber, int sequenceNumber, Copies.TypeOfCopies type, double price, Copies.Status status) {
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO copies(book_number,sequence_number,type_of_copies,price,status) VALUES (?,?,?,?,?);");
            stmt.setString(1, bookNumber);
            stmt.setInt(2, sequenceNumber);
            stmt.setString(3, type.name());
            stmt.setDouble(4, price);
            stmt.setString(5, status.name());
            stmt.executeUpdate();
            stmt.close();
            conn.commit();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     * Tạo ra giá trị book number mới.
     * @return Một chuỗi string gồm 6 kí tự
     */
    private String generateNewBookNumber() {
        String result;
        String sql = "SELECT MAX(book_number) AS max_book FROM book;";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            String str = rs.getString("max_book");
            if (str == null || str.isEmpty()) return "000000";
            int maxBook = Integer.parseInt(str);
            result = String.format("%06d", ++maxBook);
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return result;
    }

    /**
     * Tạo ra số hiệu bản sao mới. Luôn luôn trả về một
     * @return 1
     */
    private int generateNewSequenceNumber() {
        return 1;
    }
}
