package library.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import library.helper.DatabaseHelper;

public class DeleteBookManager {

    /**
     * Xóa sách khỏi database.
     * @param bookNumber Mã sách
     * @return true nếu xóa thành công, false nếu xóa thất bại
     */
    public boolean deleteBook(String bookNumber) {
        if (bookNumber.isEmpty())
            return false;
        if (deleteAllCopies(bookNumber)) {
            Connection conn = DatabaseHelper.getConnection();
            try {
                PreparedStatement stmt = conn.prepareStatement("DELETE FROM book WHERE book_number=?;");
                stmt.setString(1, bookNumber);
                stmt.executeUpdate();
                stmt.close();
                conn.commit();
                return true;
            } catch (SQLException ex) {
                Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Xóa toàn bộ bản sao (trước khi xóa sách) khỏi database)
     * @param bookNumber Mã sách
     * @return true nếu xóa thành công, false nếu xóa thất bại
     */
    private boolean deleteAllCopies(String bookNumber) {
        if (bookNumber.isEmpty())
            return false;
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement("DELETE FROM copies WHERE book_number=?;");
            stmt.setString(1, bookNumber);
            stmt.executeUpdate();
            stmt.close();
            conn.commit();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
