package library.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import library.helper.DatabaseHelper;

public class UpdateBookManager {

    /**
     * Cập nhật thông tin sách.
     * @param bookNumber Mã sách cần cập nhật
     * @param title Tiêu đề sách mới
     * @param publisher Nhà xuất bản mới
     * @param authors Tác giả mới
     * @param isbn Mã ISBN mới
     * @return true nếu cập nhật thành công, false nếu cập nhật thất bại
     */
    public boolean updateBook(String bookNumber, String title, String publisher, String authors, String isbn) {
        if (title.isEmpty()) {
            return false;
        }
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement("UPDATE book SET title=?,publisher=?,authors=?,isbn=? WHERE book_number=?;");
            stmt.setString(1, title);
            stmt.setString(2, publisher);
            stmt.setString(3, authors);
            stmt.setString(4, isbn);
            stmt.setString(5, bookNumber);
            stmt.executeUpdate();
            stmt.close();
            conn.commit();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

}
