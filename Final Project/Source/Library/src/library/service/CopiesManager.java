package library.service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import library.entity.Copies;
import library.helper.DatabaseHelper;

public class CopiesManager {

    /**
     * Lấy ra danh sách bản sao ứng với một quyển sách.
     * @param bookNumber Mã sách
     * @return danh sách bản sao của sách
     */
    public List<Copies> getCopiesList(String bookNumber) {
        ArrayList<Copies> copiesList = new ArrayList<>();
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT book_number,sequence_number,type_of_copies,price,status FROM copies WHERE book_number=?");
            stmt.setString(1, bookNumber);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Copies copies = new Copies();
                copies.setBookNumber(bookNumber);
                copies.setSequenceNumber(rs.getInt("sequence_number"));
                copies.setTypeOfCopies(Copies.TypeOfCopies.valueOf(rs.getString("type_of_copies")));
                copies.setPrice(rs.getDouble("price"));
                copies.setStatus(Copies.Status.valueOf(rs.getString("status")));
                copiesList.add(copies);
            }
            rs.close();
            stmt.close();
            return copiesList;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Kiểm tra xem bản sao đã tồn tại chưa
     * @param bookNumber Mã sách
     * @param sequenceNumber Số hiệu bản sao
     * @return true nếu đã tồn tại bản sao, false nếu không
     */
    public boolean checkCopiesExist(String bookNumber, int sequenceNumber) {
        int result;
        String sql = "select 1 FROM copies WHERE book_number=? AND sequence_number=?;";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, bookNumber);
            stmt.setInt(2, sequenceNumber);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            result = rs.getInt(1);
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            result = 0;
        }
        return (result > 0);
    }
}
