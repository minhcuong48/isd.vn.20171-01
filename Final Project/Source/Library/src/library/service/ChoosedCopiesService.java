/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import library.entity.ChoosedCopies;
import library.entity.Copies;
import library.helper.DatabaseHelper;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class ChoosedCopiesService {
    
    private static ChoosedCopiesService defaultInstance;
    
    private ChoosedCopiesService() {}
    
    public static ChoosedCopiesService getDefault() {
        if(defaultInstance == null) {
            defaultInstance = new ChoosedCopiesService();
        }
        return defaultInstance;
    }
    
    /**
     * Get all of choosed copies
     * @return choosed copies
     */
    public ObservableList<ChoosedCopies> all() {
        ObservableList<ChoosedCopies> list = FXCollections.observableArrayList();
        
        Connection conn = DatabaseHelper.getConnection();
        String sql = "SELECT * FROM copies INNER JOIN book ON copies.book_number = book.book_number;";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                ChoosedCopies copy = new ChoosedCopies();
                
                copy.setBookNumber(rs.getString("book_number"));
                copy.setSequenceNumber(rs.getInt("sequence_number"));
                copy.setTitle(rs.getString("title"));
                copy.setPrice(rs.getDouble("price"));
                copy.setPublisher(rs.getString("publisher"));
                copy.setAuthors(rs.getString("authors"));
                copy.setTypeOfCopies(rs.getString("type_of_copies"));
                copy.setStatus(rs.getString("status"));
                copy.setIsbn(rs.getString("isbn"));
                
                list.add(copy);
            }
            conn.commit();
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
    
    /**
     * Get all of status-specific choosed copies
     * @param status status of copies
     * @return list of choosed copies
     */
    public ObservableList<ChoosedCopies> all(Copies.Status status) {
        ObservableList<ChoosedCopies> list = FXCollections.observableArrayList();
        
        Connection conn = DatabaseHelper.getConnection();
        String sql = "SELECT * FROM copies INNER JOIN book ON copies.book_number = book.book_number WHERE status=?;";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, status.name());
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                ChoosedCopies copy = new ChoosedCopies();
                
                copy.setBookNumber(rs.getString("book_number"));
                copy.setSequenceNumber(rs.getInt("sequence_number"));
                copy.setTitle(rs.getString("title"));
                copy.setPrice(rs.getDouble("price"));
                copy.setPublisher(rs.getString("publisher"));
                copy.setAuthors(rs.getString("authors"));
                copy.setTypeOfCopies(rs.getString("type_of_copies"));
                copy.setStatus(rs.getString("status"));
                copy.setIsbn(rs.getString("isbn"));
                
                list.add(copy);
            }
            conn.commit();
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return list;
    }
            
    
}
