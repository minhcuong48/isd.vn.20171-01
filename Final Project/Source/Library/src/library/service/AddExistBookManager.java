package library.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import library.entity.Copies;
import library.helper.DatabaseHelper;

public class AddExistBookManager {

    /**
     * Thêm các bản sao vào database
     * @param bookNumber số hiệu sách
     * @param numberOfCopies số bản sao mới được thêm vào
     * @param type loại bản sao
     * @param price giá bản sao
     * @return true nếu tất cả bản sao mới được thêm thành công
     */
    public boolean addExistBook(String bookNumber, int numberOfCopies, Copies.TypeOfCopies type, double price) {
        if (bookNumber.isEmpty() || numberOfCopies < 1) {
            return false;
        }
        int sequenceNumber = generateNewCopiesNumber(bookNumber);
        if (price <= 0.0)
            return false;
        if (sequenceNumber < 1) {
            return false;
        }
        Copies.Status status = Copies.Status.AVAILABLE;
        boolean success = false;
        for (int i = 0; i < numberOfCopies; i++) {
            success = generateNewCopies(bookNumber, ++sequenceNumber, type, price, status);
            // nếu gặp lỗi giữa chừng thì thoát luôn
            if (!success) break;
        }
        return success;
    }

    /**
     * Tìm số thứ tự bản sao lớn nhất
     * @param bookNumber mã sách
     * @return number of copies
     */
    private int generateNewCopiesNumber(String bookNumber) {
        int result;
        String sql = "SELECT MAX(sequence_number) AS max_copies FROM copies WHERE book_number=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, bookNumber);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            result = rs.getInt(1);
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
        return result;
    }

    /**
     * Tạo ra một (1) bản sao mới trong database.
     * @param bookNumber mã sách
     * @param sequenceNumber số hiệu bản sao
     * @param type loại bản sao
     * @param price giá bản sao
     * @param status trạng thái bản sao
     * @return true nếu thành công, false nếu thất bại
     */
    private boolean generateNewCopies(String bookNumber, int sequenceNumber, Copies.TypeOfCopies type, double price, Copies.Status status) {
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO copies(book_number,sequence_number,type_of_copies,price,status) VALUES (?,?,?,?,?);");
            stmt.setString(1, bookNumber);
            stmt.setInt(2, sequenceNumber);
            stmt.setString(3, type.name());
            stmt.setDouble(4, price);
            stmt.setString(5, status.name());
            stmt.executeUpdate();
            stmt.close();
            conn.commit();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
