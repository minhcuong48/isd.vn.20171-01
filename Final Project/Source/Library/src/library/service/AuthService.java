/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import library.entity.User;
import library.helper.DatabaseHelper;

/**
 * Authenticate user in this current session
 *
 * @author Dang Xuan Bach
 */
public class AuthService {

    static private AuthService singleton = new AuthService();

    private String authenticatedUsername;

    /**
     * Get the singleton object of this class
     *
     * @return the singleton object
     */
    public static AuthService getInstance() {
        return singleton;
    }

    /**
     * Authenticate registered user
     *
     * @param username username of user
     * @param password password of user
     * @return the authenticated user, null if no user found or login
     * information is invalid
     */
    public User authenticate(String username, String password) {
        Connection conn = DatabaseHelper.getConnection();
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement("SELECT username, password, status FROM user WHERE username=?");
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                String userPassword = rs.getString("password");
                if (userPassword.equals(password) && User.Status.valueOf(rs.getString("status")) == User.Status.ACTIVE) {
                    authenticatedUsername = username;
                    rs.close();
                    stmt.close();
                    UserManager um = new UserManager();
                    return um.getUserByUsername(authenticatedUsername);
                } else {
                    authenticatedUsername = null;
                    rs.close();
                    stmt.close();
                    return null;
                }
            }
            authenticatedUsername = null;
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(AuthService.class.getName()).log(Level.SEVERE, null, ex);
            authenticatedUsername = null;
            return null;
        }
    }

    /**
     * Get authenticated user
     *
     * @return authenticated user, null if there is no authenticated users
     */
    public User getIdentity() {
        if (authenticatedUsername == null || authenticatedUsername.isEmpty()) {
            return null;
        }
        UserManager um = new UserManager();
        return um.getUserByUsername(authenticatedUsername);
    }

    /**
     * Clear current authenticated user
     */
    public void clearIdentity() {
        authenticatedUsername = null;
    }
}
