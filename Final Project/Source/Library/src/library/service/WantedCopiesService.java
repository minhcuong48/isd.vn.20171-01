/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.service;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import library.entity.WantedCopies;
import library.helper.DatabaseHelper;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class WantedCopiesService {
    
    private static WantedCopiesService defaultInstance;
    
    private WantedCopiesService() {}
    
    public static WantedCopiesService getDefault() {
        if(defaultInstance == null) {
            defaultInstance = new WantedCopiesService();
        }
        return defaultInstance;
    }
    
    /**
     * Add items to Wanted Copies list
     * @param list items
     * @return success or fail
     */
    public boolean add(ObservableList<WantedCopies> list) {
        Connection conn = DatabaseHelper.getConnection();
        String sql = "INSERT INTO wanted_copies(id, book_number, sequence_number, borrowed_date) VALUES (?, ?, ?, ?);";
        for(int i=0; i<list.size(); i++) {
            WantedCopies item = list.get(i);
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, item.getCardId());
                stmt.setString(2, item.getBookNumber());
                stmt.setInt(3, item.getSequenceNumber());
                stmt.setDate(4, Date.valueOf(item.getBorrowedDate()));
                stmt.execute();
                stmt.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return false;
            }
        }
        try {
            conn.commit();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
        return true;
    }
    
    /**
     * Count number of borrowed copies
     * @param cardId Borrower Card id
     * @return number of borrowed copies
     */
    public int countBorrowedCopiesOfUser(int cardId) {
        ObservableList<WantedCopies> list = FXCollections.observableArrayList();
        
        String sql = "SELECT count(*) as count, id FROM wanted_copies WHERE id=? GROUP BY id;";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, cardId);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs.getInt("count");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return 0;
    }
    
    /**
     * Get all of wanted copies belongs to a card
     * @param cardId
     * @return wanted copies
     */
    public ObservableList<WantedCopies> all(int cardId) {
        ObservableList<WantedCopies> list = FXCollections.observableArrayList();
        String sql = "SELECT * from wanted_copies "
                + "INNER JOIN copies ON wanted_copies.book_number = copies.book_number "
                + "AND wanted_copies.sequence_number = copies.sequence_number "
                + "INNER JOIN book ON copies.book_number = book.book_number "
                + "INNER JOIN borrower_card ON wanted_copies.id = borrower_card.id "
                + "INNER JOIN user ON borrower_card.username = user.username "
                + "WHERE borrower_card.id = " + cardId + ";";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                WantedCopies copy = new WantedCopies();
                
                copy.setCardId(rs.getInt("id"));
                copy.setUsername(rs.getString("username"));
                copy.setBookNumber(rs.getString("book_number"));
                copy.setSequenceNumber(rs.getInt("sequence_number"));
                copy.setBorrowedDate(rs.getDate("borrowed_date").toLocalDate());
                copy.setTitle(rs.getString("title"));
                copy.setPrice(rs.getDouble("price"));
                copy.setPublisher(rs.getString("publisher"));
                copy.setAuthors(rs.getString("authors"));
                copy.setIsbn(rs.getString("isbn"));
                
                list.add(copy);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    /**
     * Delete items from wanted copies list
     * @param items deleting item
     */
    public void delete(ObservableList<WantedCopies> items) {
        Connection conn = DatabaseHelper.getConnection();
        for (int i=0; i<items.size(); i++) {
            WantedCopies item = items.get(i);
            String sql = "DELETE FROM wanted_copies WHERE id=? AND book_number=? AND sequence_number=?;";
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, item.getCardId());
                stmt.setString(2, item.getBookNumber());
                stmt.setInt(3, item.getSequenceNumber());
                stmt.execute();
                conn.commit();
                stmt.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            } 
        }
    }
}
