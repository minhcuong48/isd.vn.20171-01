/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.service;

import java.sql.*;

import javafx.collections.ObservableList;
import library.entity.Copies;
import library.helper.DatabaseHelper;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class CopiesService {
    
    private static CopiesService defaultInstance;
    
    public static CopiesService getDefault() {
        if (defaultInstance == null) {
            defaultInstance = new CopiesService();
        }
        
        return defaultInstance;
    }
    
    private CopiesService() {}
 
    /**
     * Get a copy from Copies list
     * @param bookNumber book number
     * @param sequenceNumber sequence number
     * @return a copy
     */
    public Copies find(String bookNumber, int sequenceNumber) {
        Copies copy = null;
        Connection conn = DatabaseHelper.getConnection();
        String sql = "SELECT * from copies WHERE book_number=? AND sequence_number=?;";

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, bookNumber);
            stmt.setInt(2, sequenceNumber);
            ResultSet rs = stmt.executeQuery();
            conn.commit();
            if(rs.next()) {
               copy = new Copies();
               copy.setBookNumber(rs.getString("book_number"));
               copy.setSequenceNumber(rs.getInt("sequence_number"));
               copy.setPrice(rs.getDouble("price"));
               copy.setTypeOfCopies(Copies.TypeOfCopies.valueOf(rs.getString("type_of_copies")));
               copy.setStatus(Copies.Status.valueOf(rs.getString("status")));
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return copy;
    }
    
    /**
     * Update status for copies
     * @param copies list of copies
     * @param status status of copies
     */
    public void updateStatus(ObservableList<Copies> copies, Copies.Status status) {
        for(int i=0; i<copies.size(); i++) {
            updateStatus(copies.get(i), status);
        }
    }
    
    /**
     * Update status for a copy
     * @param copy A copy
     * @param status status of copy
     */
    public void updateStatus(Copies copy, Copies.Status status) {
        String sql = "UPDATE copies SET status=? WHERE book_number=? AND sequence_number=?;";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, status.name());
            stmt.setString(2, copy.getBookNumber());
            stmt.setInt(3, copy.getSequenceNumber());
            stmt.execute();
            conn.commit();
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    } 
}
