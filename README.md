# ISD.VN.20171-01

ISD.VN.20171-01 is a repository for submitting homework done by *team 1* in
**ITSS Software Development** subject.

## Members

|Full name      |Student IDs|Class          |
|---------------|-----------|---------------|
|Nguyễn Tuấn Anh|20140179   |Việt Nhật A K59|
|Đặng Xuân Bách |20140296   |Việt Nhật A K59|
|Giáp Minh Cương|20140539   |Việt Nhật B K59|

## License

ISD.VN.20171-01 is licensed under the [WTFPL License](http://www.wtfpl.net/about/), meaning you can do what the fuck you want to do with it.
